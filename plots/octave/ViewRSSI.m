#! /usr/bin/octave
% almost like contiki/examples/rssi-scanner/ViewRSSI.java
close all; % close all figures
clear all; % clear all workspac variables
clc; % clear the command line
clf; % clear figures

% constants
CHANNELS = 86;	%each 1MHz wide
RSSI_MAX_VALUE = 100; %Maximum value obtainable from RSSI readings of CC2420.

% initialize plot
figure(1);

% process input stream
while(1)
	inputLine = fgetl(stdin());
%	disp(inputLine);
	if strncmp("RSSI:", inputLine, 5)
		%save new rssi measures
		[rssi,n] = sscanf(inputLine(6:end),'%d');
		if n == CHANNELS
			plot(rssi);
			axis ([0, 86, 0, 100]);
			xlabel ("Channel");
			ylabel ("RSSI");
			drawnow();
		end
%		pause(0.05);		
	end
end
