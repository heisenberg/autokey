Project: AutoKey, SecureFog, APP-E-PHY

**Guideline to Cross Compile custom Module for OpenWrt**


Assuming the autokey repository is fully cloned (including all branches and submodules) on a Linux machine (Debian variant) and is located at:

/home/user/Documents/git/autokey/

1. go to ~/git/autokey/submodules/openwrt
2. check if feeds.conf exists. If not, create an empty file with touch feeds.conf
3. feed.conf should contain:

src-link custom_packages

↪ /home/user/Documents/git/autokey/submodules/openwrt/custom_packages

notice that full directory path has to be given.
Also, go to ~/openwrt/custom_packages and update the  SOURCE_DIR in the MAKEFILE

4. inside openwrt folder type:

$./scripts/feeds update -a 
$./scripts/feeds install -a 
make menuconfig

A new window will open the OpenWrt build setting Menu. This should contain extra menu ”AutoKey”

5. go to AutoKey>csi_probing
6. press space bar on keyboard to toggle and set it to ’M’ (M is for module, * is for integrating with firmware)
7. Exit (multiple time Exit is needed to completely exit the window)

Before we can compile a module we need to compile the whole OpenWrt firmware at least once, so that all dependent binaries can be linked!
    Options to select from "menuconfig" for the routers are:
    Target Systems (Atheros AR7XXX/AR9XXX)
    Subtarget (Generic)
    Target Profile (TP-LINK Archer C7 v1)


8. run $make -j<number of your processor cores> to compile the full firmware
9. now compile the csi_probing module: $make package/csi_probing/{clean,compile}

the compiled binary is located at: ~/openwrt/bin/packages/mips_24kc/custom_packages/

10. transfer the compiled OpenWrt custom module(csi_probing.ipk) to both routers via scp
11. remove existing package with opkg remove <package name>
12. install with opkg install <package name>

binaries will be installed at /usr/bin/ directory of the routers

HOW TO RUN THE "COMPASS" PROTOCOL

1. connect PC to 2GHz open AP: (ow_2GHz)
-> SSH to Bob(192.168.3.1)
-> SSH to Alice(192.168.3.2)
[if connection to Alice fails, ping Alice from Bob to establish a route then do SSH to Alice again from the PC]

if devices were powered off long time, their local time has been reset. Its better to a  sync them even to facilitae easy synchronization.
-> from PC browser visit Bob at 192.168.3.1 and Alice at 192.168.3.2
-> System Settings > Time > Sync with browser

2. Run: ~/autokey/code/plotting/gui$./main.py (its python 3)

The GUI should establish TCP socket connection with the routers and wait for response. In some cases this connection might fails if your PC is already using those ports. In that case close all existing browser tabs and give it another try. If everything fails you can restart the PC to have those ports free. (TO DO: randomized port assignment)

3. Click start probing button on the GUI
4. start $random_probing_client on Alice
5. start $random_probing_server on Bob

after the key generation,  background process should stop on the routers. 
for next run its a good idea to check if that old process is still running.

if found, kill it before runing the procedure again!
