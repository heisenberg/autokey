import os
import numpy as np
from pathlib import Path

class CsiLogConverter():
    """
    Used to load and store csi data including csi header, phase and amplitude.
    """
    def __init__(self):
        self.src_path = None
        self.dst_path = None
        self.csi = []
        self.headers = []
        self.amp = []
        self.phase = []

    def load(self):
        if self.src_path:
            with self.src_path.open('rb') as fd:
                file_size = os.fstat(fd.fileno()).st_size
                while fd.tell() < file_size:
                    self.headers.append(np.load(fd))
                    self.csi.append(np.load(fd))

                self.calc_phase_and_amp()

        else:
            print("Path is not set.")

    def calc_phase_and_amp(self):
        # Don't compute dbm value (10*np.log10)
        for i in range(len(self.csi)):
            self.amp.append(np.absolute(self.csi[i][:][:][:]['real'] + 1.j*self.csi[i][:][:][:]['imag']))
            self.phase.append(np.unwrap(np.angle(self.csi[i][:][:][:]['real'] + 1.j*self.csi[i][:][:][:]['imag'])))

    def clear(self):
        self.src_path = None
        self.dst_path = None
        self.csi.clear()
        self.headers.clear()
        self.amp.clear()
        self.phase.clear()

    def set_src_path(self, path):
        csi_file = Path(path)
        if csi_file.is_file():
            self.src_path = csi_file
        else:
            raise Exception("No file to load has been selected.")

    def set_dst_path(self, path):
        self.dst_path = Path(path)

    def write_to_drive(self):
        csi_hdr = self.headers[0]

        with self.dst_path.open('w') as csi_phase_fd:
            csi_phase_fd.write('double phase[' + str(len(self.phase)) + '][' + str(csi_hdr[0][8]) + '][' + str(csi_hdr[0][9]) + '][' + str(csi_hdr[0][7]) + ']' + ' = {\n')
            for i in range(len(self.phase)):
                csi_phase_fd.write('{')
                for rx in range(csi_hdr[0][8]):
                    csi_phase_fd.write('{\n')
                    for tx in range(csi_hdr[0][9]):
                        csi_phase_fd.write('{')

                        for subcarrier in range(csi_hdr[0][7] - 1):
                            string = str(self.phase[i][rx][tx][subcarrier])
                            string = string + ', '
                            csi_phase_fd.write('{:12}'.format(string))
                            if (subcarrier % 10 == 0) and (subcarrier != 0):
                                csi_phase_fd.write('\n')
                        # No comma after last entry
                        string = str(self.phase[i][rx][tx][csi_hdr[0][7] - 1])
                        csi_phase_fd.write('{:12}'.format(string))
                        if tx < csi_hdr[0][9] - 1:
                            csi_phase_fd.write('},\n')
                        else:
                            if rx < csi_hdr[0][8] - 1:
                                csi_phase_fd.write('}\n},\n\n')
                            else:
                                if i < len(self.phase) - 1:
                                    csi_phase_fd.write('}\n}},\n\n')
                                else:
                                    csi_phase_fd.write('}\n}\n}\n};\n')

converter = CsiLogConverter()
converter.set_src_path("/home/reaz/Documents/gitlab/autokey/code/plotting/gui/2020-03-02_15-20-26_bob_csi_log.npy")
converter.set_dst_path("/home/reaz/Documents/gitlab/autokey/code/plotting/gui/2020-03-02_15-20-26_bob_csi_log.c")
converter.load()
converter.write_to_drive()
