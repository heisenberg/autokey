#!/bin/bash
ssh -A robat@erich "\
	cd ~/atheros_csi/wishful/agent_modules/spectral_scan_ath9k/wishful_module_spectral_scan_ath9k/fu-wishful/auto-wish && \
	sudo ./erich_ap_setup.sh && \
	exit" &
wait

ssh -A robat@mirinda "\
	cd ~/atheros_csi/wishful/agent_modules/spectral_scan_ath9k/wishful_module_spectral_scan_ath9k/fu-wishful/auto-wish && \
	sudo ./mirinda_link_setup.sh && \
	exit" &
wait
	
#ssh -A robat@mirinda "\
#	ping -I wlan9 -i 0.2 -c 350 -q 192.168.5.1 & \
#	exit" &
#
#ssh -A robat@erich "\
#	ping -I wlan905 -i 0.2 -c 350 -q 192.168.5.2 & \
#	exit" &
#
#ssh -A robat@mirinda "\
#	cd ~/atheros_csi/wishful/agent_modules/spectral_scan_ath9k/wishful_module_spectral_scan_ath9k/fu-wishful/auto-wish && \
#	sudo ./test_csi_receiver.py >> mirinda-csi-$(date -d "today" +"%Y%m%d%H%M").log & \
#	exit" &
#
#ssh -A robat@erich "\
#	cd ~/atheros_csi/wishful/agent_modules/spectral_scan_ath9k/wishful_module_spectral_scan_ath9k/fu-wishful/auto-wish && \
#	sudo ./test_csi_receiver.py >> erich-csi-$(date -d "today" +"%Y%m%d%H%M").log & \
#	exit" &

exit
