#!/bin/bash
pkill hostapd
ifconfig wlan905 | grep "inet addr:192.168.5.1"
if [ $? = 1 ]
then
	echo "Setting wlan905 inet addr to 192.168.5.1"
	ifconfig wlan905 192.168.5.1
fi
ifconfig wlan905 down
phyn=$(iw dev wlan905 info | grep "wiphy [0-9]" | grep -o "[0-9]")
sudo iw phy${phyn} set antenna 7 7
ifconfig wlan905 up
iw dev wlan905 set bitrates mcs-5 23
cd ~/atheros_csi/
./start_hostapd.sh
sleep 6
