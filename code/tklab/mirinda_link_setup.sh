#!/bin/bash
ifconfig wlan9 | grep "inet addr:192.168.5.2"
if [ $? = 1 ]
then
	echo "Setting wlan9 inet addr to 192.168.5.2"
	ifconfig wlan9 192.168.5.2
fi
ifconfig wlan9 down
phyn=$(iw dev wlan9 info | grep "wiphy [0-9]" | grep -o "[0-9]")
sudo iw phy${phyn} set antenna 7 7
ifconfig wlan9 up
iw dev wlan9 set bitrates mcs-5 23
cd ~/atheros_csi/
./connect_athcsi.sh
