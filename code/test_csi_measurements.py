# The script takes two .npy csi logs, estimates parameters, quantizes them and compares the result.
# Afterwards a the distribution of the fist parameter is plotted.
# TODO: Seems to produce slightly lower missmatch rates than C++ Code. Why?
# TODO: Generated sequences seem to contain patterns, beyond effect of quantization method. Why?

import numpy as np
import matplotlib.pyplot as plt
import plotting.gui.gui_data as ct
from plotting.gui.parameter_estimator import ParameterEstimator

def quantize(window_size, params):
    message = []
  	
    for i in range(int(len(params) / window_size)):
        moving_average = 0;
        for j in range(window_size):
            moving_average += params[i * window_size + j];
        moving_average /= window_size;
        # ~ print("Average for window " + str(i) + ": " + str(moving_average));
        
        for j in range(window_size):
            # XXX: This will make a sequence of length window_size consisting only of 1 impossible.
            # XXX: Reduces randomness!
            message.append(1 if params[i * window_size + j] > moving_average else 0)
            
    return message
            
# Only filtered for 3x3 and phyerror == 0
csi_a = './plotting/gui/csi_offline_bin_data/2020-03-02_15-20-26_alice_csi_log.npy'
csi_b = './plotting/gui/csi_offline_bin_data/2020-03-02_15-20-26_bob_csi_log.npy'
# ~ csi_a = './plotting/gui/csi_offline_bin_data/2020-02-28_13-02-14_alice_csi_log.npy'
# ~ csi_b = './plotting/gui/csi_offline_bin_data/2020-02-28_13-02-14_bob_csi_log.npy'

# Filtered for 3x3, phyerror == 0, correlation and timediff
# ~ csi_a = './plotting/csi_offline_bin_data/2020-02-19_14-05-25_alice_csi_log.npy'
# ~ csi_b = './plotting/csi_offline_bin_data/2020-02-19_14-05-25_bob_csi_log.npy'

alice_csi = ct.CsiRecord()
alice_csi.clear()
alice_csi.set_path(csi_a)
alice_csi.load()

bob_csi = ct.CsiRecord()
bob_csi.clear()
bob_csi.set_path(csi_b)
bob_csi.load()


parameterEstimator = ParameterEstimator()
parameterEstimator.load_csi(alice_csi,bob_csi)
params_a, params_b = parameterEstimator.extract(0,0)
# ~ parameterEstimator.export(0,0)

# ~ param_data_a = list(filter(lambda x: x != 0, params_a[:,0]))
# ~ param_data_b = list(filter(lambda x: x != 0, params_b[:,0]))
# ~ print("A Couldn't find parameter for " + str(len(param_data_a) - len(params_a)) + " packets.")
# ~ print("B Couldn't find parameter for " + str(len(param_data_b) - len(params_b)) + " packets.")

param_data_a = params_a[:,0]
param_data_b = params_b[:,0]


for i in range(int(min(len(param_data_a),len(param_data_b)) / 128)):
    msg_a = quantize(2, param_data_a[i * 128:(i+1) * 128])
    msg_b = quantize(2, param_data_b[i * 128:(i+1) * 128])
    missmatches = 0
    for j in range(min(len(msg_a),len(msg_b))):
        if msg_a[j] != msg_b[j]:
            missmatches += 1
    print("Message A" + str(i) + ": " + str(msg_a))
    print("Message B" + str(i) + ": " + str(msg_b))
    print("Number of missmatches: " + str(missmatches))

plt.hist(param_data_a, bins=len(param_data_a))
plt.hist(param_data_b, bins=len(param_data_b))
plt.title("Distribution for the fist parameter.")
plt.show()


