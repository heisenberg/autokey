#!/usr/bin/env python3

#import os
#from pathlib import Path
import numpy as np
#import scipy as sp
import scipy.io as sio
from scipy.optimize import curve_fit
from pathlib import Path

class ParameterEstimator():
    def __init__(self):
        # function to fit
        self.param_names = ['eA', 'zeta', 'eP', 'l', 'beta']
        self.params_a = []
        self.params_b = []
        self.params_i = None
        self.params_j = None
        # define x values
        self.x2 = (np.arange(57).astype(float)-28.)
        self.mask = np.ones((57,)).astype(bool)
        self.mask[28] = False
        self.x2 = self.x2[self.mask]
        self.x = 0.3125*self.x2
        self.csi_a = None
        self.csi_b = None
    
    def load_csi(self, csi_a, csi_b):
        self.csi_a = csi_a
        self.csi_b = csi_b
    
    def extract(self, params_i, params_j):
        """Extract parameters from csi phase data.

        Keyword arguments:
        params_i -- antenna number
        params_j -- antenna number
        """
        self.params_a = []
        self.params_b = []
        self.params_i = params_i
        self.params_j = params_j
        for k in range(min(len(self.csi_a.phase), len(self.csi_b.phase))):
            y_a = self.csi_a.phase[k][self.params_i,self.params_j]
            y_b = self.csi_b.phase[k][self.params_i,self.params_j]
            y_a = y_a-np.mean(y_a)
            y_b = y_b-np.mean(y_b)
            try:
                popt_a, pcov_a = curve_fit(self._f, self.x2, y_a, p0=[0.512,-0.02812,-0.006355,-0.02762,0.1326], method='lm')
                popt_b, pcov_b = curve_fit(self._f, self.x2, y_b, p0=[0.512,-0.02812,-0.006355,-0.02762,0.1326], method='lm')
                f_a = self._f(self.x2, *popt_a)
                f_b = self._f(self.x2, *popt_b)
                self.params_a.append(popt_a.reshape((1,-1)))
                self.params_b.append(popt_b.reshape((1,-1)))
            except Exception as error:
                # TODO: Handle exception
                print(error)
                continue

        self.params_a = np.concatenate(self.params_a, axis=0)
        self.params_b = np.concatenate(self.params_b, axis=0)
        
        return self.params_a, self.params_b 
    
    def export(self, params_i, params_j):
        # store csi for matlab
        #~ sio.savemat('csi_a.mat', {'csi_a': self.csi_a.csi})
        #~ sio.savemat('csi_b.mat', {'csi_b': self.csi_b.csi})
        # store parameters for matlab
        self.params_i = params_i
        self.params_j = params_j
        
        file_path = Path('./csi_parameters')
        file_path.mkdir(exist_ok = True)
        file_a = Path('./csi_parameters/params_a' + str(self.params_i) + str(self.params_j) + '.mat')
        file_b = Path('./csi_parameters/params_b' + str(self.params_i) + str(self.params_j) + '.mat')
        sio.savemat(file_a, {'params_a': self.params_a})
        sio.savemat(file_b, {'params_b': self.params_b})

    def _f(self, x, eA, zeta, eP, l, beta):
        fs = 0.3125 #MHz
        return np.arctan(eA*np.sin(2*np.pi*zeta*fs*x + eP)/np.cos(2*np.pi*zeta*fs*x))-2*np.pi*l*fs*x+beta*np.ones(56)

    def _jac(self, x, eA, zeta, eP, l, beta):
        fs = 0.3125 #MHz
        
        d1 = -4*np.sin(np.pi/4-np.pi*zeta*fs*x)*np.sin(np.pi*zeta*fs*x+np.pi/4)*np.sin(
            2*np.pi*zeta*fs*x+eP)/(eA**2*np.cos(
            4*np.pi*zeta*fs*x+2*eP)-eA**2-np.cos(4*np.pi*zeta*fs*x)-1)
        
        d2 = -4*np.pi*eA*fs*x*np.cos(eP)/(eA**2*(np.cos(4*np.pi*zeta**fs*x+2*eP)-1)-np.cos(4*np.pi*eA*fs*x)-1)
        
        d3 = eA*np.cos(2*np.pi*zeta*fs*x)*np.cos(2*np.pi*zeta*fs*x+eP)/(eA**2*np.sin(2*np.pi*zeta*fs*x+eP)**2+
                                                                     np.cos(2*np.pi*zeta*fs*x)**2)
        
        d4 = -2*np.pi*fs*x
        
        d5 = np.ones(x.shape)
        return np.concatenate([d1.reshape((-1,1)), 
                               d2.reshape((-1,1)), 
                               d3.reshape((-1,1)), 
                               d4.reshape((-1,1)), 
                               d5.reshape((-1,1))], axis=1)
