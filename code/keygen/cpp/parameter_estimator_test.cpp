#include <cmath>
#include <cstdlib>
#include <cstdarg>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>

#include "least-squares-cpp/include/lsqcpp.h"
#include "parameter_estimator.h"
#include "csi_log.cpp"
#include "../../utils/print.h"

double sinus_fit(double observed_x, const Eigen::VectorXd& parameters)
{
    if(parameters.size() == 3) {
        return parameters(0) * std::sin(parameters(1) * observed_x + parameters(2));
    }
    else {
        std::cerr << "sinus_fit expects 3 parameters but number of parameters is " << parameters.size() << "!" << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

/**
 * This function is for testing purposes.
 * It generates artificial observations of a sinus function with variable amplitude, frequency and phase.
 */
void create_sinus_observation(ErrorFunction::Vector& observed_x,
                          ErrorFunction::Vector& observed_y,
                          double amplitude,
                          double frequency,
                          double phase)
{
    int n_observations = std::min(observed_x.size(), observed_y.size());
    double x_step = 2 * M_PI / frequency / n_observations;
    
    if(n_observations > 0) {
        observed_x(0) = 0;
        for(int i = 0; i < n_observations; ++i) {
            observed_y(i) = amplitude * std::sin(frequency * observed_x(i) + phase);
            if(i < n_observations - 1) {
                observed_x(i + 1) += observed_x(i) + x_step;
            }
        }
    }
}


void sinus_fit_test()
{
    // Create artificial observations for an oscillation
    double amplitude = 1.123456789;
    double frequency = 1.89;
    double phase = M_PI / 2.5;
    int n_observations = 56;
    ErrorFunction::Vector errors(n_observations);
    ErrorFunction::Vector observed_x(n_observations);
    ErrorFunction::Vector observed_y(n_observations);
    
    create_sinus_observation(observed_x, observed_y, amplitude, frequency, phase);
    std::cout << "Generated " << n_observations 
        << " observations with Amplitude: " << amplitude 
        << ", Frequency: " << frequency 
        << ", Phase: " << phase << std::endl;
    
    // Use least-squares-cpp library to fit function to observations.
    ErrorFunction fitting_error(observed_x, observed_y, &sinus_fit);
    
    lsq::GaussNewton<double, ErrorFunction, lsq::ArmijoBacktracking<double>> optimizer;    
    optimizer.setErrorFunction(fitting_error);
    optimizer.setMaxIterations(200);
    //~ optimizer.setMinGradientLength(1e-6);
    //~ optimizer.setMinStepLength(1e-6);
    //~ optimizer.setMinError(0);
    optimizer.setStepSize(lsq::ArmijoBacktracking<double>(0.8, 0.1, 1e-10, 1.0, 0));
    optimizer.setVerbosity(0);

    // Initial guess for the unknown parameters of the fitting function
    Eigen::VectorXd initialGuess(3);
    initialGuess << 1.0, 1.5, 1.3;

    auto result = optimizer.minimize(initialGuess);
    std::cout << "Fitted Parameters: " << result.xval.transpose() << std::endl;
}

void csi_fit_test(void)
{
    double parameters[5];
    
    for(int measurement = 0; measurement < 8; ++measurement) {
        for(int rx_antenna = 0; rx_antenna < 3; ++ rx_antenna) {
            for(int tx_antenna = 0; tx_antenna < 3; ++tx_antenna) {
                // XXX: format-security...
                std::string str = "Measurement: " + std::to_string(measurement) + ", RX: " + std::to_string(rx_antenna) + ", TX: " + std::to_string(tx_antenna) + "\n";
                PRINT(LOG_INFO, "%s", str.c_str());
                estimate_csi_parameters(csi_log_a[measurement][rx_antenna][tx_antenna], parameters);
                estimate_csi_parameters(csi_log_b[measurement][rx_antenna][tx_antenna], parameters);
                
                /* to_string has fixed default precision of 6 digits, if higher precision is needed ostringstring can be used. */
                std::ostringstream gain_mismatch_sstream;
                gain_mismatch_sstream << std::fixed;
                gain_mismatch_sstream << std::setprecision(8);
                gain_mismatch_sstream << parameters[0];
    
                // XXX: format-security...
                std::string parameter_string = "Parameters: " + gain_mismatch_sstream.str() + ","
                                                 + std::to_string(parameters[1]) + ","
                                                 + std::to_string(parameters[2]) + ","
                                                 + std::to_string(parameters[3]) + ","
                                                 + std::to_string(parameters[4]) + "\n";
                PRINT(LOG_INFO, "%s", parameter_string.c_str());
            }
        }
    }
}

int main(void)
{
    std::cout << "Test 1: Try to fit a sinus curve." << std::endl;
    sinus_fit_test();
    std::cout << std::endl;
    
    std::cout << "Test 2: Try to fit curve to csi measurements from log file." << std::endl;
    csi_fit_test();
    
    exit(EXIT_SUCCESS);
}
