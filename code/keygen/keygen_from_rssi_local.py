#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 17:19:19 2016

@author: olbrich
"""
import os
import time
from csi import receiver as csi_receiver
import numpy as np
# uses https://pypi.python.org/pypi/pycrypto
from Crypto.Cipher import AES

#constants
MAX_PINGS = 128

def gen_rnd_num(rssis):
    key = np.zeros((AES.block_size, 1), dtype=np.uint8)
    cipher = AES.new(key, AES.MODE_CFB, key)
    for i in range(0, int(len(rssis)/AES.block_size)):
        for j in range(0, AES.block_size):
            key[j] ^= rssis[(i * AES.block_size) + j]
        key = cipher.encrypt(key)
    print('AUTH', key)

# define receiver params
csi_dev = '/dev/CSI_dev'
runt = 60 #total time in seconds for the measurements
ival = 1.0 # interval in seconds
debug = False

# check CSI device
if not os.path.exists(csi_dev):
    raise ValueError('Could not find CSI device: %s.' % csi_dev)

# set timer
start_time = time.time()
end_time = start_time + runt

print("Start receiving CSI for %d sec in %d sec intervals..." %(runt, ival))
rssis = np.zeros((MAX_PINGS, 1), dtype=np.uint8)
rx_antenna_nb = 3
rx_amp = np.zeros((rx_antenna_nb, 1))
#while time.time() < end_time:
for idx in range(0, MAX_PINGS):
    csi = csi_receiver.scan(debug=debug)
    #print("csi," +str(csi))
    time.sleep(ival)
    csi_0 = csi[0].view(np.recarray)
    csi_matrix = csi_0.csi_matrix
    for r in range(0, rx_antenna_nb):
        rx_amp[r] = np.mean(csi_matrix[r])
    print(time.time(), rx_amp[0], rx_amp[1], rx_amp[2])

#compute random key
#gen_rnd_num(rssis)
