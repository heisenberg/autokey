#!/usr/bin/env python3

from fuzzy_extractor import FuzzyExtractor
import os
import numpy as np
from pathlib import Path

RX = 3
TX = 3
NUM_TONES = 56
MSR_NR = 2

server_path = Path('../plotting/csi_offline_bin_data/server_csi.npy')
client_path = Path('../plotting/csi_offline_bin_data/client_csi.npy')

with server_path.open('rb') as server_fd:
    file_size = os.fstat(server_fd.fileno()).st_size
    server_csi = []
    while server_fd.tell() < file_size:
        server_csi.append(np.load(server_fd))
        
with client_path.open('rb') as client_fd:
    file_size = os.fstat(client_fd.fileno()).st_size
    client_csi = []
    while client_fd.tell() < file_size:
        client_csi.append(np.load(client_fd))

s_csi = np.empty([RX, TX, NUM_TONES])
c_csi = np.empty([RX, TX, NUM_TONES])
s_csi = server_csi[MSR_NR][:][:][:]['real'] + 1.j*server_csi[MSR_NR][:][:][:]['imag']
c_csi = client_csi[MSR_NR][:][:][:]['real'] + 1.j*client_csi[MSR_NR][:][:][:]['imag']

quantized_csi_s = np.empty([RX, TX, NUM_TONES])
quantized_csi_c = np.empty([RX, TX, NUM_TONES])
for r in range(RX):
    for t in range(TX):
        # get centeroid of csi data
        s_csi_sum = np.sum(s_csi[r][t][:])
        s_csi_center = s_csi_sum / NUM_TONES
        c_csi_sum = np.sum(c_csi[r][t][:])
        c_csi_center = c_csi_sum / NUM_TONES
        print(s_csi_center)
        print(c_csi_center)
        # quantize
        for i in range(NUM_TONES):
            if s_csi[r][t][i].real >= s_csi_center.real and s_csi[r][t][i].imag >= s_csi_center.imag:
                quantized_csi_s[r,t,i] = int(0)
            elif s_csi[r][t][i].real >= s_csi_center.real and s_csi[r][t][i].imag < s_csi_center.imag:
                quantized_csi_s[r,t,i] = int(1)
            elif s_csi[r][t][i].real < s_csi_center.real and s_csi[r][t][i].imag < s_csi_center.imag:
                quantized_csi_s[r,t,i] = int(2)
            else:
                quantized_csi_s[r,t,i] = int(3)
            
            if c_csi[r][t][i].real >= c_csi_center.real and c_csi[r][t][i].imag >= c_csi_center.imag:
                quantized_csi_c[r,t,i] = int(0)
            elif c_csi[r][t][i].real >= c_csi_center.real and c_csi[r][t][i].imag < c_csi_center.imag:
                quantized_csi_c[r,t,i] = int(1)
            elif c_csi[r][t][i].real < c_csi_center.real and c_csi[r][t][i].imag < c_csi_center.imag:
                quantized_csi_c[r,t,i] = int(2)
            else:
                quantized_csi_c[r,t,i] = int(3)

keygen_str_s = ""
keygen_str_c = ""
key_list_s = []
key_list_c = []
for r in range(RX):
    for t in range(TX):
        keygen_str_s_dbg = ""
        keygen_str_c_dbg = ""
        for b in range(NUM_TONES // 8):
            new_byte_s = int(0)
            new_byte_c = int(0)
            for i in range(8):
                new_byte_s = (int(new_byte_s) << 2) + quantized_csi_s[r, t, b*8 + i]
                if r == t:
                    new_byte_c = (int(new_byte_c) << 2) + quantized_csi_c[r, t, b*8 + i]
                else:
                    new_byte_c = (int(new_byte_c) << 2) + quantized_csi_c[t, r, b*8 + i]
                
                keygen_str_s_dbg += str(int(quantized_csi_s[r, t, b*8 + i]))
                if r == t:
                    keygen_str_c_dbg += str(int(quantized_csi_c[r, t, b*8 + i]))
                else:
                    keygen_str_c_dbg += str(int(quantized_csi_c[t, r, b*8 + i]))
            keygen_str_s += chr(int(new_byte_s))
            keygen_str_c += chr(int(new_byte_c))
        key_list_s.append(keygen_str_s_dbg)
        key_list_c.append(keygen_str_c_dbg)
        print("RX: " + str(r+1) + " TX: " + str(t+1))
        print(keygen_str_s_dbg)
        print(keygen_str_c_dbg + "\n")

# fuzzy extractor code taken from https://pypi.org/project/fuzzy-extractor/
#                                 https://github.com/carter-yagemann/python-fuzzy-extractor/tree/master/docs
#
# accept 16 byte (128-bit) input values and guarantees that inputs within 8 bits of each other will produce the same key with over 0.9999 probability
#~ extractor = FuzzyExtractor(16, 8)

#~ key, helper = extractor.generate(keygen_str_s)
#~ r_key_1 = extractor.reproduce(keygen_str_c, helper)  # r_key should equal key

#~ print('Key   : ' + str(key))
#~ print('R Key1: ' + str(r_key_1))
