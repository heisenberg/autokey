#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <sys/types.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "bch_codec.h"
#include "secresketch.h"
#include "./cpp/parameter_estimator.h"
//~ #include "test_data/alice_bob_phase.c"
// #include "test_data/2019-07-10_16-03-24_csi_log.c"
#include "test_data/2019-07-10_15-12-45_csi_log.c"
//~ #include "test_data/2020-02-19_14-05-25_csi_log.c"

/**
 * Helper function to print the various uint8_t arrays.
 * 
 * \param array The array of uint8_t to print.
 * \param length The length of the array to print.
 */
inline static void print_array(uint8_t array[], int length)
{
    for (int i = 0; i < length; ++i) {
        printf("%" PRIu8, array[i]);
    }
}

/**
 * 
 */
static double calculate_metric_entropy(uint8_t message[], unsigned int length)
{
    /* H(X) = [-[(freq_0)log2(freq_0))+((freq_1)log2(freq_1))]] / number of bits */
	double freq_0 = 0.0;
	double freq_1 = 0.0;
	double c = 0.0;

    /* Calculate frequency of each alphabet */
	for (unsigned int i = 0; i < length; ++i) {
		if (message[i] == 0)
			++c;
	}
	freq_0 = c / length;
	printf("freq_0: %f\n", freq_0);

	c = 0.0;
	for (unsigned int i = 0; i < length; ++i) {
		if (message[i] == 1)
			++c;
	}
	freq_1 = c / length;
	printf("freq_1: %f\n", freq_1);

	return (- ((freq_0 * log2(freq_0)) + (freq_1 * log2(freq_1)))) / length;
}

int main(void)
{	/* Common */
	time_t begin = time(NULL);  // to calculate total execution time;    
    struct key_recovery_data *key_r_data;

    printf("\n**** Quantization (Moving Window) ****\n");
	uint8_t *message_a = malloc(N_PKT * sizeof(uint8_t)); // quantized bits
	uint8_t *message_b = malloc(N_PKT * sizeof(uint8_t)); // Bob: for quantized bits
    memset(message_a, 0, N_PKT);
    memset(message_b, 0, N_PKT);
    parameter_quantization(message_a, 3, 3, N_PKT, alice_phase);
    parameter_quantization(message_b, 3, 3, N_PKT, bob_phase);
    //~ csi_quantization(message_a, 3, 3, N_PKT, alice_phase);
    //~ csi_quantization(message_b, 3, 3, N_PKT, bob_phase);

	#if (DEBUG_KEYGEN != 0)
    {
        printf("\nmessage_a = ");
        print_array(message_a, N_PKT);
        printf("\nmessage_b = ");
        print_array(message_b, N_PKT);

        /* Check bit mismatch between A,B after quantization (DEBUG)*/
        printf("\nBit mismatch message_a, message_b: ");
        int k = 0;
        for (unsigned int i = 0; i < N_PKT; ++i) {
            if (message_a[i] != message_b[i]) {
                printf("%d ", i);
                k++;
            }
        }
        printf("\nNr. of errors		:%d", k);
        printf("\n");
    }
	#endif

	printf("\n**** Node A encodes  ****\n");
    key_r_data = encode(message_a, N_PKT);
    
    printf("\n\n/* Node A sends sketch_a_s, rand_a_x and ecc_a to Node B */\n\n");
    
	printf("\n**** Node B decodes  ****\n");
    decode(message_b, N_PKT, key_r_data);
    
	/* Check if correctly reconstructed. */
    #if (DEBUG_KEYGEN != 0)
	{
        int k = 0;
        printf("\n");
        printf("\nBit mismatch between message_a and message_b after reconciliating all blocks : \n");
        for (unsigned int i = 0; i < N_PKT; ++i) {
            if (message_a[i] != message_b[i]) {
                printf("%d ", i);
                k++;
            }
        }
        if (k != 0 ){
            printf("\n0, Successfully reconliced and reconstructed at Node B\n");
        }
    }
    #endif
    
    /* Calculate Metric Entropy */
    {
        double entropy;
        printf("\n\n**** Metric entropy of quantized bits ****\n");
        entropy = calculate_metric_entropy(message_a, N_PKT);
        printf("Metric entropy of quantized bits: %f\n\n", entropy);
    }
    
    free(message_a);
    free(message_b);

	time_t end = time(NULL);
	printf("\nTotal time elapsed: %ld sec.", (end - begin));
	printf("\n");

	return 0;
}
