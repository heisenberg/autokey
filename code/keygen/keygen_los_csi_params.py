import hashlib # cyrpto module
#import os
import numpy as np
import csi_types as ct
import komm # digital communication @https://komm.readthedocs.io/en/latest/
from parameter_estimator import ParameterEstimator

#os.chdir('home/reaz/Documents/fu-git/autokey/code/plotting/')


def secure_sketch_generate(w,k):# to be run on Alice to reconcile. Fuzzy Extractor
    n = len(w)
    assert(np.log2(n+1) % 1 == 0)
    x = [bool(np.random.randint(0,2)) for i in range(n)]
    R = np.logical_xor(x,w)
    k = 50 # default 50
    RNG1 = [np.random.randint(0,2) for i in range(k)]
    # BCH encode
    code = komm.BCHCode(int(np.log2(n+1)),13) # Here 13 is the maximum number of error that is possible to correct based on BCH 
    r = code.encode(RNG1)
    s = np.logical_xor(w,r)
    return x, s, R

def secure_sketch_reproduce(w,s,x,k):# to be run on Bob to reconcile. Fuzzy Extractor
    n = len(w)
    #sm, sn = s.shape
    #xm, xn = x.shape

    assert(np.log2(n+1) % 1 == 0)
    #assert(sm == 1)
    #assert(xm == 1)
    #assert(sn == n)
    #assert(xn == n)

    # r' = XOR(w',s)
    r1 = np.logical_xor(w,s)

    code = komm.BCHCode(int(np.log2(n+1)),13) # Here 13 is the maximum number of error that is possible to correct based on BCH 
    # randomness = BCH decode r'
    r2 = code.decode(r1*1)
    # r = BCH encode randomness
    r3 = code.encode(r2)
    # w = XOR(s,r)
    Wclean = np.logical_xor(s,r3)
    # R = XOR(w,x)
    R = np.logical_xor(Wclean,x)
    return R

csi_a = '/home/aljoscha/wimi/autokey/code/plotting/gui/csi_offline_bin_data/alice_csi.npy'
csi_b = '/home/aljoscha/wimi/autokey/code/plotting/gui/csi_offline_bin_data/bob_csi.npy'

alice_csi = ct.CsiRecord()
alice_csi.clear()
alice_csi.set_path(csi_a)
alice_csi.load()

bob_csi = ct.CsiRecord()
bob_csi.clear()
bob_csi.set_path(csi_b)
bob_csi.load()

parameterEstimator = ParameterEstimator()
parameterEstimator.load_csi(alice_csi,bob_csi)
params_a, params_b = parameterEstimator.extract(0,0)

eAGainAlice = []
eAGainBob = []
for i in range(3):
    for j in range(3):
        a,b = parameterEstimator.extract(i,j)
        eAGainAlice.append(a[:,0])
        eAGainBob.append(b[:,0])

# eAGainAlice[0,1,2,3,4,5,6,7,8] equals [Rx0Tx0 Rx0Tx1 Rx0Tx2 Rx1Tx0 Rx1Tx1 Rx1Tx2 Rx2Tx0 Rx2Tx1 Rx2Tx2]

keyLength = 127 # 1 bit less as fuzzy extractor supports
nrSubCarrier = 56 # put 56(if BW= 20) or 114 (if Bw = 40) for Atheros based NIC
nrOmmitSubcarr = 7 # number of ignored subcarrier from both sides, so its 7+7=14, i.e effective subcarriers = 42;
lastIdx = []
countKey = 0

idxSizeServer = len(eAGainAlice[0])
idxSizeClient = len(eAGainBob[0])

if idxSizeServer > idxSizeClient:
    idxSizeDif = idxSizeServer - idxSizeClient
    lastIdx = idxSizeServer - idxSizeDif

else:
    idxSizeDif = idxSizeClient - idxSizeServer
    lastIdx = idxSizeClient - idxSizeDif

# Quantize eA: Alice
#
MeAGainAlice = []
SeAGainAlice = []
QeAGainAlice = []
for i in range(9):
    MeAGainAlice.append(np.mean(eAGainAlice[i]))
    SeAGainAlice.append(np.std(eAGainAlice[i]))
    
    lastIdx = len(eAGainAlice[i])
    QeAGainAlice.append([None]*lastIdx)
    for j in range(lastIdx):
        if eAGainAlice[i][j] > MeAGainAlice[i]:
            QeAGainAlice[i][j] = 1 
        else:
            QeAGainAlice[i][j] = 0

# Quantize eA: Bob
#
MeAGainBob = []
SeAGainBob = []
QeAGainBob = []
for i in range(9):
    MeAGainBob.append(np.mean(eAGainBob[i]))
    SeAGainBob.append(np.std(eAGainBob[i]))

    lastIdx = len(eAGainBob[i])
    QeAGainBob.append([None]*lastIdx)
    for j in range(lastIdx):
        if eAGainBob[i][j] > MeAGainBob[i]:
            QeAGainBob[i][j] = 1
        else:
            QeAGainBob[i][j] = 0

# Plot

# Pre entropy
#quantAlicePre = QeAGainAlice[0]
#quantBobPre = QeAGainBob[0]

#entropAlicePre = entropy(quantAlicePre)
#entropBobPre = entropy (quantBobPre)

# Plot

# Reconciliation for eA
# 

nrBlock = lastIdx % keyLength
nrKeyBlock = [None]*nrBlock
idxStartPkt = 0
# additional randomness size for the Fuzzy extractor
kTruePhase = 50
idxStartPkt = 0
clean = [[]]*9
noisy = [[]]*9
compareBits = [[]]*9
misBits = [[]]*9
sTruePhase = [[]]*9
xTruePhase = [[]]*9
RgenTruePhase = [[]]*9
RrepTruePhase = [[]]*9
bitsAlice = [[]]*9
bitsBob = [[]]*9
AliceKey = [[]]*9
BobKey = [[]]*9
countKey = 0
reconc = 0
for i in range(9):
    if len(QeAGainAlice[i]) >= keyLength:
        clean[i] = (QeAGainAlice[i][idxStartPkt:(idxStartPkt+keyLength)])
        noisy[i] = (QeAGainBob[i][idxStartPkt:(idxStartPkt+keyLength)])
        compareBits[i] = (np.bitwise_xor(clean[i], noisy[i]))
        misBits[i] = (len([e for e in compareBits[i] if e == 1]))

        sTruePhase[i], xTruePhase[i], RgenTruePhase[i] = secure_sketch_generate(clean[i],kTruePhase)
        RrepTruePhase[i] = secure_sketch_reproduce(noisy[i],sTruePhase[i],xTruePhase[i],kTruePhase)
        for j, elm in enumerate(sTruePhase[i]):
            if RrepTruePhase[i][j] == RgenTruePhase[i][j]:
                reconc = 1
            else:
                reconc = 0
        if reconc == 1:
            print(str(i)+': Reconciled  Successfully')
            bitsAlice[i] = RgenTruePhase[i]*1 # to convert from logical to double use double(), for integers int32()
            bitsBob[i] = RrepTruePhase[i]*1
            AliceKey[i] = hashlib.sha256("".join(map(str,bitsAlice[i])).encode('utf-8')).hexdigest()
            BobKey[i] = hashlib.sha256("".join(map(str,bitsAlice[i])).encode('utf-8')).hexdigest()
            if AliceKey[i] == BobKey[i]:
                print('AliceKey and BobKey are same: ' )
                key = AliceKey[i] # could be also BobKey as they are the same
                file = open('Key'+str(i)+'.txt','w') 
                file.write(AliceKey[i])
                countKey = countKey+1
                nrZeros = (len([e for e in clean[i] if e == 1]))
            else:
                print('AliceKey and BobKey are not same')        
        else:
            print(str(i)+': Reconciliation  failed' )
