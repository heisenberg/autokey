#ifndef _SECRESKETCH_H_
#define _SECRESKETCH_H_

#include <inttypes.h>
#include "bch_codec.h"


#define MAX_ERRORS   2048
#define WINDOW_SIZE     4   /* XXX: Check floor rounding */
#define DEBUG_KEYGEN    1
#define	POLY_DEGREE     7   /* BCH polynomial degree */
#define ERROR_CORR_CAP  9   /* erroc correction capability */
#define N_PKT         128
#define GENERATOR       0

/**
 * XXX: Data is an array containing 2 * sketch_len + ecc_len bytes.
 * It can be considered a struct with minimal alignment of the following form:
 *
 * struct data {
 *  uint8_t sketch[sketch_len];
 *  uint8_t rand_x[rand_x_len];
 *  uint8_t ecc[ecc_len];
 * }__attribute__((packed));
 *
 * TODO: Use bits instead of bytes.
 */
struct key_recovery_data {
    uint16_t sketch_len;
    uint16_t rand_x_len;
    uint16_t ecc_len;
    uint8_t data[];
};

void parameter_quantization(uint8_t message[], int rx, int tx, int nr_packets, double csi_phase_data[][rx][tx][56]);
void parameter_quantization_2(uint8_t message[], int rx, int tx, int nr_packets, double csi_phase_data[][rx][tx][56]);
void csi_quantization(uint8_t message[], int rx, int tx, int nr_packets, double csi_phase_data[][rx][tx][56]);

struct key_recovery_data* encode(uint8_t message[], unsigned int msg_len);

void decode(uint8_t message[], unsigned int msg_len, struct key_recovery_data *key_r_data);
// struct uint8_t* decode(uint8_t message[], unsigned int msg_len, struct key_recovery_data *key_r_data);

inline unsigned int calculate_nr_block_bits(struct bch_control *bch) {
    return (bch->n + 1) / (2 * ERROR_CORR_CAP - 2);
}

inline unsigned int calculate_nr_blocks(struct bch_control *bch) {
    return (bch->n + 1) / calculate_nr_block_bits(bch);
}

#endif
