import logging
import datetime
import datetime
import numpy as np

from uniflex.core import modules
from uniflex.core import events

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import hashlib
from Crypto.Cipher import AES

alpha = 1

# csi_num defines number of CSI pairs (Erich, Miranda) to plot. csi_index SHOULD NOT be changed from 0
csi_num = 10
csi_index = 0

# Rows are rx indices, columns are tx indices.
# A value of False in the matrix will say that that specific rx and tx antenna pair is not shown in the plot.
# The number of rows and columns should match the number of received and transmission antennas,
#respectively.
erich_indices = [[True, False, False],
                [False, False, False],
                [False, False, False]]
miranda_indices = [[True, False, False],
                [False, False, False],
                [False, False, False]]
erich_indices = np.array(erich_indices)
miranda_indices = np.array(miranda_indices)

#hex2bin converter
binary = lambda x: " ".join(reversed( [i+j for i,j in zip( *[ ["{0:04b}".format(int(c,16)) for c in reversed("0"+x)][n::2] for n in [1,0] ] ) ] ))

def find_first_pair(indices):
    for r in range(0, indices.shape[0]):
        for t in range(0, indices.shape[1]):
            if indices[r,t]:
                return r, t
    return -1, -1

e_rx_idx, e_tx_idx = find_first_pair(erich_indices)
m_rx_idx, m_tx_idx = find_first_pair(miranda_indices)

def getbytes(bits):
    done = False
    while not done:
        byte = 0
        for _ in range(0, 8):
            try:
                bit = next(bits)
            except StopIteration:
                bit = 0
                done = True
            byte = (byte << 1) | int(bit)
        yield byte

'''
	Global controller performs channel sounding in 802.11 network using Atheros WiFi chipsets supporting
	CSI.
'''

class ChannelSounderWiFiController(modules.ControlApplication):
    def __init__(self, num_nodes):
        super(ChannelSounderWiFiController, self).__init__()
        self.log = logging.getLogger('ChannelSounderWiFiController')
        self.log.info("ChannelSounderWiFiController")
        self.nodes = {}  # APs UUID -> node
        self.num_nodes = num_nodes
        #setup plot
        self.first_erich = True
        self.first_miranda = True
        self.ydata_erich = None
        self.ydata_miranda = None
        self.colors = ['#FF0000', '#0000FF', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000',
            '#00FFFF', '#008080',  '#000080', '#FF00FF', '#800080', '#000000']
        self.show_legend = True
        self.print_unmatched = True
        print('Finished initialisation')

    @modules.on_start()
    def my_start_function(self):
        self.log.info("start control app")

        self.ifaceName = 'wlan0'
        self.start = None
        #self.hopping_interval = 3

        # CSI stuff
        self.results = []
        self.samples = 1


    @modules.on_exit()
    def my_stop_function(self):
        print("stop control app")

    @modules.on_event(events.NewNodeEvent)
    def add_node(self, event):
        node = event.node

        self.log.info("Added new node: {}, Local: {}"
                      .format(node.uuid, node.local))
        self.nodes[node.uuid] = node

        devs = node.get_devices()
        for dev in devs:
            self.log.info("Dev: %s" % str(dev.name))
            ifaces = dev.get_interfaces()
            self.log.info('Ifaces %s' % ifaces)


        if len(self.nodes) == self.num_nodes:
            self.schedule_fetch_csi()


    @modules.on_event(events.NodeExitEvent)
    @modules.on_event(events.NodeLostEvent)
    def remove_node(self, event):
        self.log.info("Node lost".format())
        node = event.node
        reason = event.reason
        if node in self.nodes:
            del self.nodes[node.uuid]
            self.log.info("Node: {}, Local: {} removed reason: {}"
                          .format(node.uuid, node.local, reason))


    def schedule_fetch_csi(self):
        try:
            self.log.info('First schedule_fetch_csi')

            for node in self.nodes.values():
                device = node.get_device(0)
                device.callback(self.channel_csi_cb).get_csi(self.samples, False)

        except Exception as e:
            self.log.error("{} !!!Exception!!!: {}".format(
                datetime.datetime.now(), e))

    def get_power(self, m):
        s = m.shape
        #print('CSI shape', s)
        rx_pwr = np.zeros((s[0], s[1], s[2]))
        rx_phase = np.zeros((s[0], s[1], s[2]))
        for r in range(0, s[0]):
            for t in range(0, s[1]):
                rx_sig = m[r, t, :]
                rx_pwr[r,t] = 10*np.log10(np.absolute(rx_sig))
                rx_phase[r,t] = np.angle(rx_sig)
        return rx_pwr, rx_phase

    def channel_csi_cb(self, data):
        """
        Callback function called when CSI results are available
        """
        global erich_indices
        global miranda_indices
        global csi_num, csi_index
        global e_rx_idx, e_tx_idx, m_rx_idx, m_tx_idx
        global binary

        node = data.node
        devName = None
        if data.device:
            devName = data.device.name
        csi = data.msg

        #print("Default Callback: "
        #      "Node: {}, Dev: {}, Data: {}"
        #      .format(node.hostname, devName, csi.shape))
        host = node.hostname.lower()

        csi_0 = csi[0].view(np.recarray)

        #print(csi_0.header)
        #print(csi_0.csi_matrix)

        #self.results.append(csi_0)

        #plot data (new data will replace the old one)
        y, p = self.get_power(csi_0.csi_matrix)

        if 'erich' == host:
            current_y = y[e_rx_idx, e_tx_idx, :]
            mean = np.mean(current_y)
            std = np.std(current_y)
            upper_lim = mean+alpha*std
            lower_lim = mean-alpha*std
            for i in range(0, len(current_y)):
                if current_y[i] >= lower_lim and current_y[i] <= upper_lim:
                    current_y[i] = np.nan
                elif current_y[i] < lower_lim:
                    current_y[i] = 0
                else:
                    current_y[i] = 1
            if self.first_erich:
                self.ydata_erich = current_y
                self.first_erich = False
            else:
                self.ydata_erich = np.concatenate((self.ydata_erich, current_y))
        elif 'miranda' == host:
            current_y = y[m_rx_idx, m_tx_idx, :]
            mean = np.mean(current_y)
            std = np.std(current_y)
            upper_lim = mean+alpha*std
            lower_lim = mean-alpha*std
            for i in range(0, len(current_y)):
                if current_y[i] > lower_lim and current_y[i] < upper_lim:
                    current_y[i] = np.nan
                elif current_y[i] < lower_lim:
                    current_y[i] = 0
                else:
                    current_y[i] = 1
            if self.first_miranda:
                self.ydata_miranda = current_y
                self.first_miranda = False
            else:
                self.ydata_miranda = np.concatenate((self.ydata_miranda, current_y))
            #keep only values from common positions on both
            common_erich = []
            common_mirinda = []
            for i in range(0, min(len(self.ydata_erich), len(self.ydata_miranda))):
                if not np.isnan(self.ydata_erich[i]) and not np.isnan(self.ydata_miranda[i]):
                    common_erich.append(self.ydata_erich[i])
                    common_mirinda.append(self.ydata_miranda[i])

            print('CSI index', csi_index)
            print('Erich bits', len(common_erich))
            print(common_erich)
            print('Miranda bits', len(common_mirinda))
            print(common_mirinda)

            #compare bits
            matched = True
            for i in range(0, len(common_erich)):
                if common_erich[i] != common_mirinda[i]:
                    matched = False
                    break
            if matched:
                print('Matched')
                self.print_unmatched = True
            elif self.print_unmatched:
                self.print_unmatched = False
                print('Unmatched')

            #convert to string of bits
            common_erich_str = ''.join(str(e) for e in common_erich)
            common_erich_str = common_erich_str.encode('utf-8')
            common_mirinda_str = ''.join(str(e) for e in common_mirinda)
            common_mirinda_str = common_mirinda_str.encode('utf-8')
            #compute hash
            print('Hash Erich')
            print(hashlib.sha224(common_erich_str).hexdigest())
            print('Hash Mirinda')
            print(hashlib.sha224(common_mirinda_str).hexdigest())

            #compute AES key
            if matched:
                key = np.zeros((AES.block_size, 1), dtype=np.uint8)
                i = 0
                for b in getbytes(iter(common_erich)):
                    if i < AES.block_size:
                        key[i] = b
                        i += 1
                    else:
                        break
                cipher = AES.new(key, AES.MODE_CFB, key)
                key = cipher.encrypt(key)
                #print numpy array in bin
                #s = binary(key.hex())
                s = key.hex()
                print('AES key', s)

            #plot data
            #plt.figure(0)
            #plt.subplot(2, 1, 1)
            #plt.plot(common_erich)
            #plt.title('Erich')
            #plt.subplot(2, 1, 2)
            #plt.plot(common_mirinda)
            #plt.title('Miranda')
            #plt.savefig('e_rx_idx'+str(e_rx_idx)+'_tx_idx'+str(e_tx_idx)+'_m_rx_idx'+str(m_rx_idx)+'_tx_idx'+str(m_tx_idx)+'.eps')
            #plt.clf()
            csi_index += 1

        # schedule callback for next CSI value
        if csi_index < csi_num:
            data.device.callback(self.channel_csi_cb).get_csi(self.samples, False)
