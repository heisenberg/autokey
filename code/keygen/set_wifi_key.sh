#!/bin/bash
/etc/config/wireless > ./etc/config/wireless.bkp
ifconfig wlan down
sed -e "/^  *.*'key' */s/'.*'/'key'        '$1'/" /etc/config/wireless > /etc/config/wireless.tmp
/etc/config/wireless.tmp > /etc/config/wireless
rm -f /etc/config/wireless.tmp
ifconfig wlan up
etc/init.d/network reload wireless
