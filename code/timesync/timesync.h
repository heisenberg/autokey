#ifndef _TCP_TIMESYNC_
#define _TCP_TIMESYNC_

/**
 * This is a small collection of functions for calculations with c timespec
 * as defined in time.h.
 * 
 * man 7 time: "UNIX systems represent time in seconds since the Epoch, 1970-01-01 00:00:00 +0000 (UTC)."
 * As a consequence when getting the system time, a timespec has a non negative value for
 * its tv_sec and tv_nsec members.
 * To represent a time difference that can be either positive or negative the struct timediff is defined.
 * It consists of a struct timespec and an int "sign" which is -1 for a negative offset and 1 for a positive offset. 
 * */

#include <time.h>
#include <stdint.h>

// Normally there should be no reason to change these defines
#define FORK_ERROR					"error: forking process\n"
#define CLOCK_GETTIME_ERROR			"error: clock_gettime\n"
#define TIMESYNC_BUF_SIZE			      256
#define HALF_A_SECOND				500000000
#define DEBUG_TIMESYNC                      0

static uint32_t const TIME_SYNC_PORT = 50000;

void sync_service();
void sync();
void sync_close(int socket);

#endif
