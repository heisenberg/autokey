#include "timesync.h"
#include "../utils/print.h"
#include "../utils/time_calc.h"
#include "../utils/transceive.h"
#include "../measuring/connections.h"

#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define SYNC_TIME_MSG               0x00
#define SYNC_CLOSE_MSG              0xFF

#define SYNC_TIMEOUT_US             10000
#define SYNC_TIMEOUT_CNT_LIMIT      10

#define SYNC_SERVICE_PORT           50000
#define SYNC_MAX_CLIENTS            10

#define SYNC_MSG_BUFFER_SIZE        256
#define SYNC_MSG_SIZE               256    

#define SYNC_SRV_ERROR              0
#define SYNC_SRV_CLOSE_SOCKET       1
#define SYNC_SRV_TIME_SERVED        2

struct sync_msg {
    uint8_t msg_type;
    struct timespec timestamp;
} __attribute__((packed));

struct sync_info {
    struct timespec start_time;
    struct timespec server_time;
    struct timespec stop_time;
    struct timediff offset;
};

static uint8_t sync_srv(int client_socket);
static int8_t sync_validate_request();
static int sync_init(struct sockaddr_in *sync_addr);
static int sync_new_connection(int listen_socket);
static void sync_set_min_offset(struct timediff *td_offset);

inline static void sync_get_info(int client_socket, struct sync_info *const p_info);
inline static int8_t sync_check_info(struct sync_info const *const p_info);
inline static void sync_set_time(struct sync_info const *const p_info);
inline static void sync_get_time(struct timespec *time);
inline static int8_t sync_deserialize_msg(struct sync_msg *dest, uint8_t *src, uint32_t src_size);
inline static uint32_t sync_serialize_msg(uint8_t *dest, struct sync_msg *src, uint32_t dst_size);
inline static int8_t sync_validate_msg(uint8_t *sync_msg_buffer, uint32_t msg_size);

// Server code
void sync_service() {
    int enable = 1;
    int listen_socket;
    int client_socket;
    static struct sockaddr_in sync_service_addr;
    fd_set active_fd_set;
    fd_set read_fd_set;
    
    memset(&sync_service_addr, 0, sizeof(struct sockaddr_in));
	inet_aton(SERVER_IP, &sync_service_addr.sin_addr);
    sync_service_addr.sin_family = AF_INET;
    sync_service_addr.sin_port = htons(TIME_SYNC_PORT);
    listen_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(listen_socket == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }    
    if(setsockopt(listen_socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) == -1) {
        PRINT(LOG_ERROR, "setsockopt: %s.\n", strerror(errno));
    }
    if(bind(listen_socket, (struct sockaddr*) &sync_service_addr, sizeof(struct sockaddr_in)) == -1) {
        PRINT(LOG_ERROR, "Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(listen(listen_socket, SYNC_MAX_CLIENTS)) {
        PRINT(LOG_ERROR, "Socket listening failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    FD_ZERO(&active_fd_set);
    FD_SET(listen_socket, &active_fd_set);
    
    PRINT(LOG_ERROR, "Entering service loop.\n");
    while(1) {
        read_fd_set = active_fd_set;
        
        // TODO: nfds = max fd + 1
        
        if(select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) {
            PRINT(LOG_ERROR, "Select error: %s.\n", strerror(errno));
        }
        
        for(int fd_i = 0; fd_i < FD_SETSIZE; fd_i++) {
            if(FD_ISSET(fd_i, &read_fd_set)) {
                if(fd_i == listen_socket) {
                    client_socket = sync_new_connection(fd_i);
                    if(client_socket != -1) {
                        FD_SET(client_socket, &active_fd_set);
                        PRINT(LOG_ERROR, "New connection established.\n");
                    }
                    else {
                        // TODO: handle accept error
                        exit(EXIT_FAILURE);
                    }
                }
                else {
                    PRINT(LOG_ERROR, "New service request: ");
                    switch(sync_srv(fd_i)) {
                        case SYNC_SRV_ERROR:
                            exit(EXIT_FAILURE);
                            break;
                        case SYNC_SRV_CLOSE_SOCKET:
                            PRINT(LOG_ERROR, "Close connection.\n");
                            FD_CLR(fd_i, &active_fd_set);
                            break;
                        case SYNC_SRV_TIME_SERVED:
                            PRINT(LOG_ERROR, "Send timestamp.\n");
                            break;
                        default:
                            PRINT(LOG_ERROR, "Unkown request.\n");
                            break;
                    }
                }
            }
        }
    }
}

static uint8_t sync_srv(int client_socket) {
    uint8_t sync_msg_buffer[SYNC_MSG_SIZE];
    uint8_t send_buffer[SYNC_MSG_SIZE];
    uint32_t msg_size;
    int32_t result = SYNC_SRV_ERROR;
    struct sync_msg client_request;
    struct sync_msg server_response;

    memset((void *) send_buffer, 0, sizeof(send_buffer));
    memset((void *) sync_msg_buffer, 0, sizeof(sync_msg_buffer));
    
    msg_size = msg_recv(client_socket, (void *) sync_msg_buffer, sizeof(struct sync_msg));    
    if(sync_deserialize_msg(&client_request, sync_msg_buffer, msg_size) == 1) {
        switch(client_request.msg_type) {
            case SYNC_TIME_MSG:
                server_response.msg_type = SYNC_TIME_MSG;
                sync_get_time(&server_response.timestamp);
                result = SYNC_SRV_TIME_SERVED;
                msg_size = sync_serialize_msg(send_buffer, &server_response, sizeof(struct sync_msg));
                msg_send(client_socket, send_buffer, msg_size);
                break;
            case SYNC_CLOSE_MSG:
                result = SYNC_SRV_CLOSE_SOCKET;
                close(client_socket);
                break;
            default:
                PRINT(LOG_ERROR, "Invalid message type!\n");
                exit(EXIT_FAILURE);
                break;
        }
    }
    else {
        // TODO: handle broken/invalid message
        PRINT(LOG_ERROR, "Invalid message type!\n");
        exit(EXIT_FAILURE);
    }
    
    return result;
}

// Client code
void sync() {
    int client_socket;
    struct sync_info info;
    struct sockaddr_in sync_addr;        
    client_socket = sync_init(&sync_addr);
    
    PRINT(LOG_ERROR, "Successfully connected to timesync server.\n");
    sync_get_info(client_socket, &info);
    if(sync_check_info((struct sync_info const *const) &info))  {
        sync_set_time(&info);
    }
    
    sync_close(client_socket);        
    //~ /* update minimum offset if necessary */
    //~ if(td_current_offset.time.tv_sec < td_min_offset.time.tv_sec) {
        //~ td_min_offset.time.tv_sec = td_current_offset.time.tv_sec;
        //~ td_min_offset.time.tv_nsec = td_current_offset.time.tv_nsec;
        //~ td_min_offset.sign = td_current_offset.sign;
    //~ }
    //~ else if(td_current_offset.time.tv_sec == td_min_offset.time.tv_sec) {
        //~ if(td_current_offset.time.tv_nsec < td_min_offset.time.tv_nsec) {
            //~ td_min_offset.time.tv_nsec = td_current_offset.time.tv_nsec;
            //~ td_min_offset.sign = td_current_offset.sign;
        //~ }
    //~ }
}

void sync_close(int socket) {
    uint8_t send_buffer[SYNC_MSG_SIZE];
    uint32_t msg_size;
    struct sync_msg close_msg;
    
    close_msg.msg_type = SYNC_CLOSE_MSG;
    sync_get_time(&close_msg.timestamp);
    msg_size = sync_serialize_msg(send_buffer, &close_msg, sizeof(struct sync_msg));    
    msg_send(socket, send_buffer, msg_size);
    
    // TODO: ensure msg was send before closing
    close(socket);
}

static void sync_set_min_offset(struct timediff *td_offset) {
    /* check if time_t is defined as signed and set current minimum offset to supported maximum values */
    td_offset->time.tv_sec = 1;
    if(td_offset->time.tv_sec >= 0 && ~td_offset->time.tv_sec >= 0) {
        td_offset->time.tv_sec = 0 - 1;
        if(DEBUG_TIMESYNC) {
            //printf("time_t is defined as unsigned, this is rather unusual\n");
            //printf("max value for tv_sec: %lu\n\n", td_offset->time.tv_sec);
        }
    }
    else {
        td_offset->time.tv_sec = 1;
        for(size_t k = 1; k < (sizeof(time_t) * 8)-1; k++) {
            td_offset->time.tv_sec += (1 << k);    
        }
        if(DEBUG_TIMESYNC) {
            //printf("time_t is defined as signed\n");
            //printf("max positive value for tv_sec: %ld\n\n", td_offset->time.tv_sec);
        }
    }
	td_offset->time.tv_nsec = MAX_NANOSLEEP_NSEC;
}

// TODO: prevent DoS
static int8_t sync_validate_request() {
    return 1;
}

static int sync_init(struct sockaddr_in *sync_addr) {
    int connect_socket;
    int priority = 6;
    int no_delay = 1;
    
	memset(sync_addr, 0, sizeof(struct sockaddr_in));	
	inet_aton(SERVER_IP, &sync_addr->sin_addr);
	sync_addr->sin_family = AF_INET;
    sync_addr->sin_port = htons(TIME_SYNC_PORT);
    
    connect_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(connect_socket == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    if(setsockopt(connect_socket, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        PRINT(LOG_ERROR, "setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(connect_socket, IPPROTO_TCP, TCP_NODELAY, (const void *) &no_delay, sizeof(no_delay)) == -1) {
        PRINT(LOG_ERROR, "setsockopt: %s.\n", strerror(errno));
    }

	while(connect(connect_socket, (struct sockaddr*) sync_addr, sizeof(struct sockaddr_in)) == -1) {
		PRINT(LOG_ERROR, "Connecting to server failed: %s.\n", strerror(errno));
	}
    
    return connect_socket;
}

static int sync_new_connection(int listen_socket) {
    int const enable = 1;
    int const priority = 6;
    int accept_socket;
    
    if(sync_validate_request()) {
        accept_socket = accept(listen_socket, 0, 0);
        if(accept_socket == -1) {
            PRINT(LOG_ERROR, "Accepting connection failed: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        /* TCP_NODELAY : Turn off any Nagle-like algorithm.
         * SO_PRIORITY : Low priotity 0 to high priority 6 (7 with CAP_NET_ADMIN capability).
         *               Only works if network interface qdisc (queuing discipline) supports priorities. */
        if(setsockopt(accept_socket, SOL_SOCKET, SO_PRIORITY, (void const *) &priority, sizeof(priority)) == -1) {
            PRINT(LOG_ERROR, "setsockopt: %s.\n", strerror(errno));
        }
        if(setsockopt(accept_socket, IPPROTO_TCP, TCP_NODELAY, (void const *) &enable, sizeof(enable)) == -1) {
            PRINT(LOG_ERROR, "setsockopt: %s.\n", strerror(errno));
        }
    }
    
    return accept_socket;
}

inline static void sync_get_time(struct timespec *time) {
    if(clock_gettime(CLOCK_REALTIME, time) == -1) {
        PRINT(LOG_ERROR, CLOCK_GETTIME_ERROR);
        exit(EXIT_FAILURE);
    }
}

// XXX: Implement de/serialization (googles lib)
inline static int8_t sync_deserialize_msg(struct sync_msg *dest, uint8_t *src, uint32_t src_size) {
    int8_t result = 0;
    
    if(sync_validate_msg(src, src_size) == 1) {
        dest->msg_type = src[0];
        dest->timestamp.tv_sec = ((struct sync_msg *) src)->timestamp.tv_sec;
        dest->timestamp.tv_nsec = ((struct sync_msg *) src)->timestamp.tv_nsec;
        result = 1;
    }
    else {
        //printf("Deserialization failed.\n");
        dest = NULL;
    }
    
    return result;
}

// XXX: Implement de/serialization (googles lib)
inline static uint32_t sync_serialize_msg(uint8_t *dest, struct sync_msg *src, uint32_t dst_size) {
    uint32_t msg_size = 0;
    
    if(dest != NULL && src != NULL && dst_size > 0) {
        memcpy(dest, &src->msg_type, sizeof(src->msg_type));
        memcpy(&dest[sizeof(src->msg_type)], &src->timestamp.tv_sec, sizeof(src->timestamp.tv_sec));
        memcpy(&dest[sizeof(src->msg_type) + sizeof(src->timestamp.tv_sec)], &src->timestamp.tv_nsec, sizeof(src->timestamp.tv_nsec));
        msg_size = sizeof(src->msg_type) + sizeof(src->timestamp.tv_sec) + sizeof(src->timestamp.tv_nsec);
    }
    
    return msg_size;
}

inline static int8_t sync_validate_msg(uint8_t *sync_msg_buffer, uint32_t msg_size) {
    int8_t result = 0;
    
    if(msg_size == sizeof(struct sync_msg) && sync_msg_buffer != NULL) {
        switch(sync_msg_buffer[0]) {
            case SYNC_CLOSE_MSG:
            case SYNC_TIME_MSG:
                result = 1;
                break;
            default:
                break;
        }
    }
    
    return result;
}

inline static void sync_get_info(int client_socket, struct sync_info *const p_info) {
    uint8_t send_buffer[SYNC_MSG_SIZE];
    uint8_t sync_msg_buffer[SYNC_MSG_SIZE];
    uint32_t msg_size;
    struct sync_msg client_request;
    struct sync_msg server_response;
    struct timediff td_1;
	struct timediff td_2;

    memset((void *) send_buffer, 0, sizeof(send_buffer));
    memset((void *) sync_msg_buffer, 0, sizeof(sync_msg_buffer));
    client_request.msg_type = SYNC_TIME_MSG;
    sync_set_min_offset(&p_info->offset);
    
    sync_get_time(&p_info->start_time);
    /* Request timestamp from server. */
    client_request.timestamp.tv_sec = p_info->start_time.tv_sec;
    client_request.timestamp.tv_nsec = p_info->start_time.tv_nsec;
    msg_size = sync_serialize_msg(send_buffer, &client_request, sizeof(send_buffer));
    msg_send(client_socket, send_buffer, msg_size);
    
    /* Receive server timestamp. */
    msg_size = msg_recv(client_socket, (void *) sync_msg_buffer, sizeof(struct sync_msg));
    sync_get_time(&p_info->stop_time);
    sync_deserialize_msg(&server_response, sync_msg_buffer, msg_size);
    p_info->server_time = server_response.timestamp;
        
    time_sub(&td_1, &p_info->server_time, &p_info->start_time);
    time_sub(&td_2, &p_info->stop_time,   &p_info->server_time);
    timediff_substract(&p_info->offset, &td_1, &td_2);
        
    /* divide by two and correct integer floor rounding */
    p_info->offset.time.tv_sec /= 2;
    if(p_info->offset.time.tv_sec % 2 == 1) {
        p_info->offset.time.tv_nsec /= 2;
        p_info->offset.time.tv_nsec += HALF_A_SECOND;
    }
    else {
        p_info->offset.time.tv_nsec /= 2;
    }
}

inline static int8_t sync_check_info(struct sync_info const *const p_info) {
    int8_t result = 0;
    
    // TODO: return (p_info->stop_time - p_info->start_time >= SYNC_TIMEOUT) ? 0 : 1;
    if(p_info != NULL) {
        result = 1;
    }
     
    return result;
}

inline static void sync_set_time(struct sync_info const *const p_info) {
	struct timespec cur_time;
    struct timespec new_time;
	struct timediff td;
	
    /* Set system time to server time by adding calculated offset to current system time. */
    // TODO: This code block MUST not be interrupted or there will be a significiant synchronization error!
    sync_get_time(&cur_time);
    if(p_info->offset.sign == 1) {
        time_add(&new_time, &cur_time, &p_info->offset.time);
    }
    else {
        time_sub(&td, &cur_time, &p_info->offset.time);
        if(td.sign == -1) {
            PRINT(LOG_ERROR, "error: adjusted time can't be negative!\n");
            exit(EXIT_FAILURE);
        }
        new_time.tv_sec = td.time.tv_sec;
        new_time.tv_nsec = td.time.tv_nsec;
    }
    
    /* Beware that this can have an effect on timers. */
    if(clock_settime(CLOCK_REALTIME, (struct timespec const *) &new_time)) {
        PRINT(LOG_ERROR, "Time synchronization failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    else {
        //printf("Time synchronization succeeded.\n");
        //printf("Time before adjustment: %ld.%09ld\n",   cur_time.tv_sec, cur_time.tv_nsec);
        //printf("Offset                : %ld.%09ld\n",   p_info->offset.time.tv_sec, p_info->offset.time.tv_nsec);
        //printf("Time after adjustment : %ld.%09ld\n\n", new_time.tv_sec, new_time.tv_nsec);
        fflush(stdout);
    }
}
