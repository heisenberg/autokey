#include "csi.h"

// ONLY INCLUDED FOR DEBUGGIG / PROFILING
#include "../utils/time_calc.h"
#include "../utils/print.h"
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <inttypes.h>
#include <syslog.h>

static int csi_dev_fd;                  	            // file descriptor for csi device
static u_int8_t csi_data_buf[CSI_DATA_BUFFER_SIZE];     // user space buffer where csi data from kernel is copied to.

/* Prototypes for static functions. */
static void get_header(struct csi *const csi_pkt);
static void get_matrix(struct csi *const csi_pkt);
static void get_payload(struct csi *const csi_pkt);
static int recover_signbit(int data, int maxbit);

/**
 * Prints the csi header.
 * */
void print_csi_header(struct csi_hdr const *const hdr) {
    PRINT(LOG_INFO, "CSI header: time: %" PRIu64
    ", csilen: %" PRIu16
    ", channel: %" PRIu16
    ", phyerr: %" PRIu8
    ", noise: %" PRIu8
    ", rate: %" PRIu8
    ", chbw: %" PRIu8
    ", tones: %" PRIu8
    ", rx: %" PRIu8
    ", tx: %" PRIu8
    ", rssi: %" PRIu8
    ", rssi0: %" PRIu8
    ", rssi1: %" PRIu8
    ", rssi2: %" PRIu8
    ", pldlen: %" PRIu16 "\n\n",
        hdr->tstamp,
        hdr->csi_len,
        hdr->channel,
        hdr->phyerr,
        hdr->noise,
        hdr->rate,
        hdr->chanbw,
        hdr->num_tones,
        hdr->nr,
        hdr->nc,
        hdr->rssi,
        hdr->rssi_0,
        hdr->rssi_1,
        hdr->rssi_2,
        hdr->pld_len);
}

/**
 * Prints the csi matrix.
 * */
void print_csi_matrix(struct COMPLEX const *const matrix, uint8_t nr, uint8_t nc, uint8_t num_tones) {
    for(int i = 0; i < nr*nc*num_tones; i += 4) {
        PRINT(LOG_INFO, "%5" PRId16 "%5" PRId16 "i,  ", matrix[i].real, matrix[i].imag);
        PRINT(LOG_INFO, "%5" PRId16 "%5" PRId16 "i,  ", matrix[i+1].real, matrix[i+1].imag);
        PRINT(LOG_INFO, "%5" PRId16 "%5" PRId16 "i,  ", matrix[i+2].real, matrix[i+2].imag);
        PRINT(LOG_INFO, "%5" PRId16 "%5" PRId16 "i,\n", matrix[i+3].real, matrix[i+3].imag);
    }    
    PRINT(LOG_INFO, "\n\n");
}

/**
 * Prints the payload that came with the csi packet.
 * The payload contains the whole packet including all the layers of
 * protocol header that were used for transmission. It is not just some
 * upper layer user data that was send.
 * */
void print_csi_payload(uint8_t const *const payload, uint16_t len) {
    PRINT(LOG_INFO, "Message payload(%d bytes):\n", len);
    for(int i = 0; i < len && i < CSI_PAYLOAD_SIZE; i++) {
        PRINT(LOG_INFO, "%2x  ", payload[i]);
    }
    PRINT(LOG_INFO, "\n\n"); 
}

/**
 * Open the CSI device for reading.
 * It is also possible to open the device for writing and to configure
 * the atheros card (see csi_read function in ar9003_csi.c).
 * */
int open_csi_dev(int flags) {
	/* TODO: Check flags! */
	csi_dev_fd = open(CSI_DEV_PATH, flags);
	return csi_dev_fd;
}

/**
 * Close the CSI device.
 * */
int close_csi_dev() {
    return close(csi_dev_fd);
}

/**
 * It takes csi_pkt, a pointer to a user struct csi and fills in the data
 * for the header, csi matrix and payload.
 * When making a read system call, the device driver for the CSI device
 * is trying to copy all available data in one chunk.
 * */
int32_t get_csi(struct csi *const csi_pkt) {
    int32_t bytes_read;
    
    /* Get CSI header, matrix and payload from device driver
     * TODO: Better to write directly to user buffer for better performance? */
    bytes_read = read(csi_dev_fd, csi_data_buf, CSI_DATA_BUFFER_SIZE);
    if(bytes_read > CSI_DATA_BUFFER_SIZE) {
        PRINT(LOG_CRIT, "Buffer overflow while reading CSI data.\n");
        exit(EXIT_FAILURE);
    }
    
    if(bytes_read >= (int32_t) sizeof(struct csi_hdr)) {
		get_header(csi_pkt);
		get_matrix(csi_pkt);
		get_payload(csi_pkt);
	    
		// Change csi_len to uncompressed size to match length of csi_pkt->matrix
		csi_pkt->hdr.csi_len = csi_pkt->hdr.nr * csi_pkt->hdr.nc * csi_pkt->hdr.num_tones * sizeof(struct COMPLEX);
	} else if(bytes_read > 0) {
		printf("Csi device data is corrupted. Only read %d bytes.\n", bytes_read);
	}
    
    return bytes_read;
}

/**
 * Convert integer type matrix to C99 double complex matrix.
 * */
void convert_matrix_type(struct csi const *const input_pkt, double complex (*converted_matrix)[NUM_TX_ANTENNAS][NUM_SUBCARRIERS]) {
	struct COMPLEX const(*const input_matrix)[NUM_TX_ANTENNAS][NUM_SUBCARRIERS] = input_pkt->matrix;
	
	/* loop for each rx antenna */
    for(int nr_idx = 0; nr_idx < input_pkt->hdr.nr; ++nr_idx) {
        /* loop for each tx antenna */
        for(int nc_idx = 0; nc_idx < input_pkt->hdr.nc; ++nc_idx) {
            /* subcarrier loop */
            for(int k = 0; k < input_pkt->hdr.num_tones; ++k) {
				converted_matrix[nr_idx][nc_idx][k] = (double complex) input_matrix[nr_idx][nc_idx][k].real + (double complex) input_matrix[nr_idx][nc_idx][k].imag * I;
			}
        }
    }
}

/*
 * Copy the csi header to user structure.
 * */
static void get_header(struct csi *const csi_pkt) {    
    memcpy(&csi_pkt->hdr, csi_data_buf, sizeof(struct csi_hdr));
}

/*
 * The complex numbers of the CSI matrix are saved in memory as an array
 * of 10 Bit values. This saves memory but is complicated to access so
 * here the 10 Bit array is converted.
 * */
static void get_matrix(struct csi *const csi_pkt) {
    u_int8_t k;
    u_int8_t bits_left;
    u_int8_t nr_idx;
    u_int8_t nc_idx;
    u_int32_t bitmask;
    u_int32_t idx;
    u_int32_t current_data;
    u_int32_t h_data;
    int32_t real;
    int32_t imag;
    u_int8_t *csi_values;
    struct COMPLEX(* matrix)[NUM_TX_ANTENNAS][NUM_SUBCARRIERS];
    
    csi_values = &csi_data_buf[sizeof(struct csi_hdr)];
    matrix = csi_pkt->matrix;
    
    /* init bits_left
     * we process 16 bits at a time*/
    bits_left = 16; 

    /* according to the h/w, we have 10 bit resolution 
     * for each real and imag value of the csi matrix H 
     */
    bitmask = (1 << 10) - 1;
    idx = 0;
    
    /* get 16 bits for processing */
    h_data = csi_values[idx++];
    h_data += (csi_values[idx++] << 8);
    current_data = h_data & ((1 << 16) -1);
    
    // TODO: Increment rightmost index first for better performance
    /* loop for every subcarrier */
    for(k = 0; k < csi_pkt->hdr.num_tones; k++) {
        /* loop for each tx antenna */
        for(nc_idx = 0; nc_idx < csi_pkt->hdr.nc; nc_idx++) {
            /* loop for each rx antenna */
            for(nr_idx = 0; nr_idx < csi_pkt->hdr.nr; nr_idx++) {
                /* bits number less than 10, get next 16 bits */
                if((bits_left - 10) < 0) {
                    h_data = csi_values[idx++];
                    h_data += (csi_values[idx++] << 8);
                    current_data += h_data << bits_left;
                    bits_left += 16;
                }
                
                imag = current_data & bitmask;
                imag = recover_signbit(imag, CSI_RESOLUTION);
                matrix[nr_idx][nc_idx][k].imag = imag;

                bits_left -= 10;
                current_data = current_data >> 10;
                
                /* bits number less than 10, get next 16 bits */
                if((bits_left - 10) < 0) {
                    h_data = csi_values[idx++];
                    h_data += (csi_values[idx++] << 8);
                    current_data += h_data << bits_left;
                    bits_left += 16;
                }

                real = current_data & bitmask;
                real = recover_signbit(real, CSI_RESOLUTION);
                matrix[nr_idx][nc_idx][k].real = real;

                bits_left -= 10;
                current_data = current_data >> 10;
            }
        }
    }
}

/*
 * Here payload means the whole packet, including all data that is added
 * by protocol layers, like MAC frame, etc. not just the plain user payload!
 * */
static void get_payload(struct csi *const csi_pkt) {
    u_int8_t *pld;
    
    if(csi_pkt->hdr.pld_len <= CSI_PAYLOAD_SIZE) {
        pld = &csi_data_buf[sizeof(struct csi_hdr) + csi_pkt->hdr.csi_len];
        memcpy(csi_pkt->payload, pld, csi_pkt->hdr.pld_len);
    }
    else {
        PRINT(LOG_ERR, "Copying %d bytes payload to user buffer would result in buffer overflow.\n", csi_pkt->hdr.pld_len);
        exit(EXIT_FAILURE);
    }
}

/*
 * The msb of the 10 Bit value is its sign. When moved to a 16 Bit variable
 * the format needs to be in proper twos complement.
 * */
static int recover_signbit(int data, int maxbit) {
    if ( data & (1 << (maxbit - 1))) {
        /* negative */;
        data -= (1 << maxbit);
    }
    return data;
}
