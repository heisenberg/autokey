#include "connections.h"
#include "../timesync/timesync.h"
#include "../utils/print.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>

int connect_tcp_server(struct sockaddr_in *server_addr) {
    int listen_socket;
    int accept_socket;
    int priority = 6;
    int no_delay = 1;
    int reuse_addr = 1;

    memset(server_addr, 0, sizeof(struct sockaddr_in));

	inet_aton(SERVER_IP, &server_addr->sin_addr);
    server_addr->sin_family = AF_INET;
    server_addr->sin_port = htons(TIME_SYNC_PORT);

    listen_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(listen_socket == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(listen_socket, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr)) == -1) {
        PRINT(LOG_ERROR, "setsockopt: %s.\n", strerror(errno));
    }

    if(bind(listen_socket, (struct sockaddr*) server_addr, sizeof(struct sockaddr_in)) == -1) {
        PRINT(LOG_ERROR, "Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(listen(listen_socket, 1)) {
        PRINT(LOG_ERROR, "Socket listening failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

	do {
		accept_socket = accept(listen_socket, 0, 0);
		if(accept_socket == -1) {
			PRINT(LOG_WARNING, "Accepting connection failed: %s.\n", strerror(errno));
		}
	} while(accept_socket == -1);

    /* Set socket options.
     * TCP_NODELAY : Turn off any Nagle-like algorithm.
     * SO_PRIORITY : Set the protocol-defined priority for all packets to be sent on this socket.
     *               Low priotity 0 to high priority 6 (7 with CAP_NET_ADMIN capability).
     *               Only works if network interface qdisc (queuing discipline) supports prioritys.
     */
    if(setsockopt(accept_socket, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(accept_socket, IPPROTO_TCP, TCP_NODELAY, (const void *) &no_delay, sizeof(no_delay)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    close(listen_socket);

    return accept_socket;
}

int connect_tcp_client(struct sockaddr_in *server_addr) {
    int sock;
    int priority = 6;
    int no_delay = 1;

	*server_addr = (const struct sockaddr_in) {0};
	inet_aton(SERVER_IP, &server_addr->sin_addr);
	server_addr->sin_family = AF_INET;
    server_addr->sin_port = htons(TIME_SYNC_PORT);

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sock == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(sock, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    if(setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const void *) &no_delay, sizeof(no_delay)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }

	while(connect(sock, (struct sockaddr*) server_addr, sizeof(struct sockaddr_in)) == -1) {
		PRINT(LOG_WARNING, "Connecting to server failed: %s.\n", strerror(errno));
	}

    return sock;
}

int measure_connect(struct sockaddr_in *address) {
    int server_msr_socket;
    int priority = 6;
    //~ struct timeval timeout;

    //~ timeout.tv_sec = SND_RCV_TIMEOUT_SEC;
    //~ timeout.tv_usec = SND_RCV_TIMEOUT_USEC;

    server_msr_socket = socket(PF_INET, SOCK_DGRAM | SOCK_NONBLOCK, IPPROTO_UDP);
    if(server_msr_socket == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(server_msr_socket, SOL_SOCKET, SO_PRIORITY, (const void *) &priority, sizeof(priority)) == -1) {
        PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
    }
    //~ if(setsockopt(server_msr_socket, SOL_SOCKET, SO_RCVTIMEO, (const void *) &timeout, sizeof(timeout)) == -1) {
        //~ //printf("setsockopt: %s.\n", strerror(errno));
    //~ }
    //~ if(setsockopt(server_msr_socket, SOL_SOCKET, SO_SNDTIMEO, (const void *) &timeout, sizeof(timeout)) == -1) {
        //~ //printf("setsockopt: %s.\n", strerror(errno));
    //~ }

    if(bind(server_msr_socket, (struct sockaddr*) address, sizeof(struct sockaddr_in)) == -1) {
        PRINT(LOG_ERROR, "Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    return server_msr_socket;
}

int connect_to_pc(struct sockaddr_in *remote_addr, uint32_t port) {
    int res;
    int remote_plot_socket;

	inet_aton(REMOTE_PLOT_IP, &remote_addr->sin_addr);
	remote_addr->sin_family = AF_INET;
    remote_addr->sin_port = htons(port);

    remote_plot_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(remote_plot_socket == -1) {
        PRINT(LOG_ERROR, "Remote plot socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    do {
        res = connect(remote_plot_socket, (struct sockaddr*) remote_addr, sizeof(struct sockaddr_in));
        if(res == -1) {
            PRINT(LOG_WARNING, "Connecting to remote plot host failed: %s.\n", strerror(errno));
            usleep(250000);
        }
    } while(res == -1);

    return remote_plot_socket;
}
