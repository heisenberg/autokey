/*
* This program is for Station(STA) to synchronise, CSI aquisition, parameter extraction,
* reconciliation, key verification.
*/

#include "csi.h"
#include "process_payload.h"
#include "connections.h"
#include "csi_measure.h"
#include "../utils/daemon.h"
#include "../utils/time_calc.h"
#include "../utils/print.h"
#include "../utils/transceive.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>

#include "../keygen/secresketch.h"

#define WAIT_FOR_SERVER_START       2           /* Time to wait in seconds before the program execution is started. */

static int control_socket;
struct csi csi_pkt[MEASURES_TO_TAKE];           /* User structure where csi data is saved */

static void generate_key(uint8_t **key)
{
    PRINT(LOG_INFO, "Quantized Bits:\n");

    struct key_recovery_data *key_r_data;
    *key = malloc(N_PKT * sizeof(uint8_t));
    double csi_phase_matrix[N_PKT][3][3][56];

    for(uint32_t i = 0; i < N_PKT; ++i) {
        calculate_phase(&csi_pkt[i], &csi_phase_matrix[i][0]);
    }
    parameter_quantization(*key, 3, 3, N_PKT, csi_phase_matrix);

    for (int i = 0; i < N_PKT; ++i) {
        printf("%" PRIu8, (*key)[i]);
    }
    printf("\n");


    /* Send sketch to AP. */
    key_r_data = encode(*key, N_PKT);
    uint32_t len = 2 * key_r_data->sketch_len + key_r_data->ecc_len + sizeof(struct key_recovery_data);
    sleep(WAIT_FOR_SERVER_START); // to wait for the AP to finish fro the case when AP is slightly slower.
    msg_send(control_socket, (uint8_t *) key_r_data, len);
    PRINT(LOG_INFO, "Sent Sketch\n");
    free(key_r_data);

}




static uint8_t validate_key(uint8_t *key)
{
    PRINT(LOG_NOTICE,"Compare key\n");
    int missmatch = 0;
    uint8_t buffer[N_PKT];

    // msg_send(control_socket, key, N_PKT);
    uint32_t bytes_received = msg_recv(control_socket, buffer, N_PKT);
    if(bytes_received == N_PKT) {
        PRINT(LOG_INFO,"AP's key received.\n");
    } else {
        DEBUG_PRINT("Could not receive AP's key.\n");
    }

    for(int i = 0; i < N_PKT; ++i) {
        if(key[i] != buffer[i])
            missmatch++;
    }
    printf("Missmatch: %d bits.\n", missmatch);
    free(key);

    // return (missmatch < 44) ? 1 : 0;
}


static void run()
{
    char rcv_buffer[CONTROL_BUF_SIZE];			/* Used to receive csi and timesync messages */
    struct timespec job_time;					/* Time when the next measuring periode starts */
	struct timespec cur_time;
	struct timediff td;
    int32_t bytes_received;
    uint8_t *key;

    /* CONNECT PC FOR REMOTE PLOTTING */
    int plot_socket;						/* Socket for plotting csi packets on PC aplication */
    struct sockaddr_in remote_plot_addr;		/* PC address and port */
    if(CONNECT_CLIENT_TO_PC) {
        plot_socket = connect_to_pc(&remote_plot_addr, REMOTE_PLOT_PORT_CLIENT);
    }

    struct sockaddr_in server_msr_addr;
    memset(&server_msr_addr, 0, sizeof(struct sockaddr_in));
    inet_aton(SERVER_IP, &server_msr_addr.sin_addr);
    server_msr_addr.sin_family = AF_INET;
    server_msr_addr.sin_port = htons(MEASURE_PORT);

    struct sockaddr_in client_msr_addr;
    memset(&client_msr_addr, 0, sizeof(struct sockaddr_in));
    inet_aton(CLIENT_IP, &client_msr_addr.sin_addr);
    client_msr_addr.sin_family = AF_INET;
    client_msr_addr.sin_port = htons(MEASURE_PORT);

    /* CONNECT TO SERVER */
    struct sockaddr_in server_sync_addr;
	control_socket = connect_tcp_client(&server_sync_addr);
    int msr_socket = measure_connect(&client_msr_addr);

    uint32_t i_pkt = 0;
    enum state status = SYNCHRONIZE;
	while(status != TERMINATE) {
		switch(status) {
			case SYNCHRONIZE:
                PRINT(LOG_NOTICE,"SYNCHRONIZE\n");
				/* Signal server to send time to start measuring. */
				memset(rcv_buffer, 0, CONTROL_BUF_SIZE);
				send(control_socket, MEASURE_STATE, STATE_SIZE, 0);
				bytes_received = recv(control_socket, (void*) rcv_buffer, CONTROL_BUF_SIZE-1, 0);
				if(bytes_received == -1) {
                    PRINT(LOG_ERR, "Could not receive time to start probing.");
                    PRINT(LOG_ERR, "%s.\n", strerror(errno));
					close(control_socket);
					exit(EXIT_FAILURE);
				}
				if(rcv_buffer[0] == 'T' && rcv_buffer[1] == 'I' && rcv_buffer[2] == 'M' && rcv_buffer[3] == 0) {
					memcpy(&job_time, &rcv_buffer[STATE_SIZE], sizeof(job_time));
                    //~ job_time.tv_sec = *((time_t*) &rcv_buffer[STATE_SIZE]);
					//~ job_time.tv_nsec = *((long*) &rcv_buffer[STATE_SIZE + sizeof(time_t)]);
					status = MEASURE;
				} else {
                    PRINT(LOG_ERR, "Wrong message received (%d bytes), synchronization failed.\n", bytes_received);
					exit(EXIT_FAILURE);
				}
                DEBUG_PRINT("\n\n");
				break;

			case MEASURE:
                PRINT(LOG_NOTICE,"MEASURE\n");
				if(clock_gettime(CLOCK_REALTIME, &cur_time) == 0) {
                    DEBUG_PRINT("job_time: %ld.%09ld\n", job_time.tv_sec, job_time.tv_nsec);
                    DEBUG_PRINT("cur_time: %ld.%09ld\n", cur_time.tv_sec, cur_time.tv_nsec);
					time_sub(&td, &job_time, &cur_time); // calculate delay until it is time to start the jobs
					if(td.sign == -1) {
                        PRINT(LOG_WARNING, "The point in time to start the jobs has already passed.\n");
                        PRINT(LOG_WARNING, "Difference between job execution time and current time: -%ld.%09ld.\n", td.time.tv_sec, td.time.tv_nsec);
					} else {
                        /* TODO: job_t - cur_t > JOB_T ==> sync error, start measuring now. */
                        DEBUG_PRINT("Start measuring in %ld.%09ld\n", td.time.tv_sec, td.time.tv_nsec);
                        nanosleep(&td.time, NULL);
                    }
                    measure(&csi_pkt[i_pkt], 1, &client_msr_addr, &server_msr_addr, msr_socket, plot_socket);
				} else {
					PRINT(LOG_WARNING, "Error when calling clock_gettime.");
				}

				status = CHECK_MEASUREMENT;
                DEBUG_PRINT("\n\n");
				break;

            case CHECK_MEASUREMENT:
                // PRINT(LOG_NOTICE,"CHECK MEASUREMENT\n");
                status = check_measurement(&csi_pkt[i_pkt]);
                DEBUG_PRINT("\n\n");
                break;

            case SIGNAL_REPEAT:
                DEBUG_PRINT("SIGNAL REPEAT\n");
                /* Signal to repeat and if necessary discard measurement i_pkt. */
                signal_repeat(control_socket);
                // status = SYNCHRONIZE;
                DEBUG_PRINT("\n\n");
                break;

            case SIGNAL_OK:
                // PRINT(LOG_NOTICE,"SIGNAL OK\n");
                /* TODO: Write function for that. */
                if(signal_ok(control_socket, i_pkt)) {
                    i_pkt++;
                    if(i_pkt < MEASURES_TO_TAKE) {
                        status = SYNCHRONIZE;
                    } else {
                        status = GENERATE_KEY;
                    }
                } else {
                    /* Repeat measurement: don't increase i_pkt. */
                    status = SYNCHRONIZE;
                }
                DEBUG_PRINT("\n\n");
                break;

            case GENERATE_KEY:
                PRINT(LOG_NOTICE,"GENERATE KEY\n");
                #if (SEND_COLLECTED_CSI_TO_PC == 1 && CONNECT_CLIENT_TO_PC == 1)
                // for(int i = 0; i < N_PKT; ++i) {
                //     send(plot_socket, &csi_pkt[i].hdr, sizeof(csi_pkt[i].hdr), 0);
                //     send(plot_socket, csi_pkt[i].matrix, csi_pkt[i].hdr.csi_len, 0);
                // }
                #endif
                generate_key(&key);
                status = TERMINATE;
                // DEBUG_PRINT("\n\n");
                // PRINT(LOG_NOTICE, "END of GENERATE KEY\n");
                break;

            case VALIDATE_KEY:

                PRINT(LOG_NOTICE,"VALIDATE KEY\n");
                /* Calculate hash from key and salt and compare it with client hash. */
                #if 1
                if(validate_key(key)) {
                    status = TERMINATE;
                    PRINT(LOG_NOTICE,"KEY GENERATION SUCCESSFUL!\n");
                } else {
                    /* Try again. */
                    i_pkt = 0;
                    // status = SYNCHRONIZE;
                    status = TERMINATE;
                    PRINT(LOG_NOTICE,"KEY DOES NOT MATCH!\n");
                }
                #endif
                PRINT(LOG_NOTICE, "END of VALIDATE KEY\n");
                break;

			default:
				/* should never end up here, otherwise something went utterly wrong. */
				PRINT(LOG_ERR, "Unkown server state.\n");
				exit(EXIT_FAILURE);
		}
	}

	close(control_socket);
    close(plot_socket);

}

int main(void)
{
    #if (RUN_AS_DAEMON)
    create_daemon();
    #endif
    PRINT(LOG_NOTICE, "Random probing at STA started.\n");
    sleep(WAIT_FOR_SERVER_START);               // Wait a bit to make sure that server is already running
    run();
    exit(EXIT_SUCCESS);
}
