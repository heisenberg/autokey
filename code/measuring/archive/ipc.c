#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include "ipc.h"

static int client_usocket;
static struct sockaddr_un server_addr;

int connect_to_ipc_file() {
    int ret;
    int server_path_len;
    
    #ifdef UNIX_PATH_MAX
    server_path_len = strnlen(IPC_SERVER_PATH_NAME, UNIX_PATH_MAX + 1);
    if(server_path_len > UNIX_PATH_MAX) {
        printf("Path name for Unix socket can't be longer than %d bytes.\n", UNIX_PATH_MAX);
        exit(EXIT_FAILURE);
    }
    #else
    server_path_len = strnlen(IPC_SERVER_PATH_NAME, 93);
    if(server_path_len > 92) {
        printf("Path name for Unix socket can't be longer than 92 bytes.\n");
        exit(EXIT_FAILURE);
    }
    #endif
    
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sun_family = AF_UNIX;
    strncpy(server_addr.sun_path, IPC_SERVER_PATH_NAME, server_path_len);
    
    client_usocket = socket(AF_UNIX, SOCK_DGRAM, 0);
    
    ret = connect(client_usocket, (struct sockaddr*) &server_addr, sizeof(server_addr));
    if(ret) {
        printf("Can't connect to host.\n");
    }
    
    return ret;
}

int send_unix_datagram(const char* buffer, int size) {
    return send(client_usocket, buffer, size, 0);
}

int close_ipc_file() {
    return close(client_usocket);
}
