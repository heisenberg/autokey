#include "../utils/time_calc.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define TIMING_CORRECTION   85914

int main(void) {
    struct timespec t_start;
    struct timespec t_stop;
    struct timespec t_seed;
    struct timespec req;
    struct timespec rem;
    struct timediff ts_diff;
    int ret;
    const char *path_sleep = "./nanosleep.dat";
    const char *path_clock = "./clock_gettime.dat";
    FILE *file_sleep;
    FILE *file_clock;
    
    req.tv_sec = 0;
    req.tv_nsec = 1;// - TIMING_CORRECTION;
    rem.tv_sec = 0;
    rem.tv_nsec = 0;
    
    file_sleep = fopen(path_sleep, "wb");
    file_clock = fopen(path_clock, "wb");
    
    if(clock_gettime(CLOCK_REALTIME, &t_seed) == -1) {
        printf("clock_gettime: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    printf("Use seed: %d\n", (unsigned int) t_seed.tv_nsec);
    srandom((unsigned int) t_seed.tv_nsec);

    printf("Size of struct timespec: %lu\n", sizeof(struct timespec));
    printf("Record time between two consecutive clock_gettime calls.\n");
    for(uint32_t i = 0; i < 100000; i++) {
        if(clock_gettime(CLOCK_REALTIME, &t_start) == -1) {
            printf("clock_gettime: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        if(clock_gettime(CLOCK_REALTIME, &t_stop) == -1) {
            printf("clock_gettime: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        time_sub(&ts_diff, &t_stop, &t_start);
        fwrite(&ts_diff.time, 1, sizeof(ts_diff.time), file_clock);
    }
    
    printf("Check nanosleep time.\n");
    for(uint32_t turns = 0; turns < 100; turns++) {
        for(uint32_t i = 0; i < 1000; i++) {
            if(clock_gettime(CLOCK_REALTIME, &t_start) == -1) {
                printf("clock_gettime: %s.\n", strerror(errno));
                exit(EXIT_FAILURE);
            }
            ret = nanosleep(&req, &rem);
            //~ ret = usleep(1);
            if(clock_gettime(CLOCK_REALTIME, &t_stop) == -1) {
                printf("clock_gettime: %s.\n", strerror(errno));
                exit(EXIT_FAILURE);
            }
            if(ret == 0) {
                time_sub(&ts_diff, &t_stop, &t_start);
                fwrite(&ts_diff.time, 1, sizeof(ts_diff.time), file_sleep);
            }
            else {
                printf("nanosleep was interrupted.\n");
            }
        }
        usleep(random() % 1000000);
    }
    
    fclose(file_sleep);
    fclose(file_clock);
}
