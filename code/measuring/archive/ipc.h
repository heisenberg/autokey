#ifndef _IPC_H
#define _IPC_H

#define IPC_SERVER_PATH_NAME "/tmp/ipc-server"
#define SERVER_BUFFER_SIZE 1500

int connect_to_ipc_file();
int send_unix_datagram();
int close_ipc_file();

#endif
