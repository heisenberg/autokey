#ifndef _CSI_H_
#define _CSI_H_

#include <stdint.h>
#include <complex.h>

/* 
 * The maximum size of the CSI matrix is 3x3x114x20Bit = 2565 Byte.
 * The header size is 25 Bytes.
 * When making a read system call, the device driver for the CSI device is trying to copy all available data in one chunk.
 * See csi_read function in ar9003_csi.h
 * */
#define NUM_RX_ANTENNAS			3
#define NUM_TX_ANTENNAS			3
#define NUM_SUBCARRIERS			56
#define CSI_DATA_BUFFER_SIZE    4096

/* 
 * XXX:
 * Max size for 802.11 frame is 2304, however Ethernet V2 format with 1500 (MPDU of 1540) is commonly used.
 * MTU + HEADERS + QOS = 2360 ?
 * */
#define CSI_PAYLOAD_SIZE        2360				

/* CSI resolution in bits, 10 Bit real, 10 Bit imaginary */
#define CSI_RESOLUTION          10
#define CSI_DEV_PATH            "/dev/CSI_dev"

/* 
 * This struct contains additional information about the captured csi packet.
 * The members are listing in there order of apperance in memory.
 * Don't change the order and don't remove the packed attribute or the data
 * won't be copied correctly anymore.
 * The variable csi_len is the length of the COMPRESSED CSI data when reading
 * it for the first time from the CSI device. Afterwards the value is changed
 * to the uncompressed size to match the length of the matrix member of struct csi.
 * */
struct csi_hdr {
    uint64_t tstamp;
    uint16_t csi_len;
    uint16_t channel;
    uint8_t phyerr;
    uint8_t noise;
    uint8_t rate;
    uint8_t chanbw;
    uint8_t num_tones;
    uint8_t nr;
    uint8_t nc;
    uint8_t rssi;
    uint8_t rssi_0;
    uint8_t rssi_1;
    uint8_t rssi_2;
    uint16_t pld_len;
} __attribute__((packed));

/* Beware that C99 also introduced its own (floating point) complex data type. */
struct COMPLEX {
    int32_t real;
    int32_t imag;
};

struct csi {
    struct csi_hdr hdr;
    struct COMPLEX matrix[NUM_RX_ANTENNAS][NUM_TX_ANTENNAS][NUM_SUBCARRIERS];
    uint8_t payload[CSI_PAYLOAD_SIZE];
};

void print_csi_header(struct csi_hdr const *const);
void print_csi_matrix(struct COMPLEX const *const, uint8_t, uint8_t, uint8_t);
void print_csi_payload(uint8_t const *const, uint16_t);

int open_csi_dev();
int close_csi_dev();
int32_t get_csi(struct csi *const);
void convert_matrix_type(struct csi const *const, double complex (*)[NUM_TX_ANTENNAS][NUM_SUBCARRIERS]);

#endif
