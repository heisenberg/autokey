/*
* This program is for Access Point(AP) to synchronise, CSI aquisition, parameter extraction,
* reconciliation, key verification.
 */

#include "csi.h"
#include "process_payload.h"
#include "connections.h"
#include "csi_measure.h"
#include "../utils/daemon.h"
#include "../utils/time_calc.h"
#include "../utils/print.h"
#include "../utils/transceive.h"
#include "../keygen/secresketch.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>

static int msr_socket;
static int control_socket;
static int plot_socket;
static struct sockaddr_in server_sync_addr;
static struct sockaddr_in server_msr_addr;
static struct sockaddr_in client_msr_addr;
static struct sockaddr_in remote_server_addr;
static enum state status;                       /* Current server state */
struct csi csi_pkt[MEASURES_TO_TAKE];           /* User structure where csi data is saved. from csi_measure.h */

static void synchronize_start(void)
{
	int32_t bytes_received;						/* Number of received bytes in time sync messages (user payload) */
	static char rcv_buffer[CONTROL_BUF_SIZE];  	/* Buffer for receiving timestamps */
	static char snd_buffer[CONTROL_BUF_SIZE];	/* Buffer for sending timestamps */
	struct timespec time;                       /* Used to synchronize time before starting measures */
	struct timespec job_time;                   /* Delay until the measurement starts after time was synchronized */

	memset(rcv_buffer, 0, CONTROL_BUF_SIZE);
	memset(snd_buffer, 0, CONTROL_BUF_SIZE);

	bytes_received = recv(control_socket, (void*) rcv_buffer, CONTROL_BUF_SIZE-1, 0);
	if(bytes_received > 0) {
        if(clock_gettime(CLOCK_REALTIME, &time) != 0) {
            PRINT(LOG_ERR, "Could not get system time\n");
            status = TERMINATE;
        } else {
            if(strncmp(rcv_buffer, MEASURE_STATE, STATE_SIZE) == 0) {
                job_time.tv_sec = JOB_DELAY_SEC;
                job_time.tv_nsec = JOB_DELAY_NSEC;
                time_add(&time, &time, &job_time);

                // XXX: This is faster than serialization but endianess can break it.
                memcpy(snd_buffer, SYNCHRONIZE_STATE, STATE_SIZE);
                memcpy(&snd_buffer[STATE_SIZE], &time.tv_sec, sizeof(time.tv_sec));
                memcpy(&snd_buffer[STATE_SIZE + sizeof(time.tv_sec)], &time.tv_nsec, sizeof(time.tv_nsec));
                send(control_socket, snd_buffer, STATE_SIZE + sizeof(time.tv_sec) + sizeof(time.tv_nsec), 0);

                /* Start ping and csi measure jobs. */
                status = MEASURE;

                /* This is most likely not as acurate as the function name suggests. */
                DEBUG_PRINT("Start measuring in %ld.%09ld\n", job_time.tv_sec, job_time.tv_nsec);
                nanosleep(&job_time, NULL);
            } else {
                PRINT(LOG_ERR, "Received invalid request of %d bytes.", bytes_received);
                status = TERMINATE;
            }
        }
	} else {
		if(bytes_received == -1)
			PRINT(LOG_ERR, "%s.\n", strerror(errno));
		status = TERMINATE;
	}
}

static void generate_key(uint8_t **key)
{
    PRINT(LOG_INFO, "Quantized Bits:\n");
    double csi_phase_matrix[N_PKT][3][3][56];

    *key = malloc(N_PKT * sizeof(uint8_t));
    for(uint32_t i = 0; i < N_PKT; ++i) {
        calculate_phase(&csi_pkt[i], &csi_phase_matrix[i][0]);
    }
    parameter_quantization(*key, 3, 3, N_PKT, csi_phase_matrix);

    for (int i = 0; i < N_PKT; ++i) {
        printf("%" PRIu8, (*key)[i]);
    }
    printf("\n");


    /* Receive lenth info */
    uint32_t len_info = sizeof(struct key_recovery_data);
    uint8_t buf[len_info];

    /* Receive information about sketch length. */
    msg_recv(control_socket, buf, len_info);
    struct key_recovery_data *key_r_data = (struct key_recovery_data *) buf;
    uint32_t total_len = 2 * key_r_data->sketch_len + key_r_data->ecc_len + sizeof(struct key_recovery_data);
    PRINT(LOG_INFO, "Received Length Info\n");

    /* Receive sketch. */
    uint8_t sketch[total_len];
    memcpy(sketch, buf, len_info);
    msg_recv(control_socket, sketch + len_info, total_len);
    PRINT(LOG_INFO, "Received Sketch\n");

   /* Decode */
    decode(*key, N_PKT, sketch);
    PRINT(LOG_INFO, "Reconciled Bits:\n");

    for (int i = 0; i < N_PKT; ++i) {
        printf("%" PRIu8, (*key)[i]);
    }
    printf("\n");

    /*  Convert Bits to HEX */
    #if 0
    unsigned int nr_blocks = calculate_nr_blocks(bch);
    unsigned int nr_block_bits = calculate_nr_block_bits(bch);
    long int binval[8], hexadecimalval = 0, i = 1, rem;

    for (int i = 0; i < N_PKT; ++i){
        binval[i] = (*key)[i];
    }


    for (unsigned int i = 0; i < nr_blocks; ++i) {


    }

    while (binval[i] != 0)
    {
        rem = binval[i] % 10;
        hexadecimalval = hexadecimalval + rem * i;
        i = i * 2;
        binval = binval / 10;
    }

    printf("Equivalent hexadecimal value: %lX\n", hexadecimalval);

    #endif


}

static uint8_t validate_key(uint8_t *key)
{
    msg_send(control_socket, key, N_PKT);
    PRINT(LOG_NOTICE, "Send AP's key\n");
    #if 0
    uint32_t bytes_received = msg_recv(control_socket, buffer, N_PKT);
    if(bytes_received == N_PKT) {
        DEBUG_PRINT("Alice's key received.\n");
    } else {
        DEBUG_PRINT("Could not receive Alice's key.\n");
    }

    for(int i = 0; i < N_PKT; ++i) {
        if(key[i] != buffer[i])
            missmatch++;
    }
    printf("Missmatch: %d bits.\n", missmatch);
    #endif
    // free(key);

}


int main(void)
{
    #if (RUN_AS_DAEMON)
    create_daemon();
    #endif

    uint8_t *key;

    PRINT(LOG_INFO, "Random probing at AP started.\n");

    status = SYNCHRONIZE;

    if(CONNECT_SERVER_TO_PC) {
        plot_socket = connect_to_pc(&remote_server_addr, REMOTE_PLOT_PORT_SERVER);
    }

    memset(&server_msr_addr, 0, sizeof(struct sockaddr_in));
    inet_aton(SERVER_IP, &server_msr_addr.sin_addr);
    server_msr_addr.sin_family = AF_INET;
    server_msr_addr.sin_port = htons(MEASURE_PORT);
    memset(&client_msr_addr, 0, sizeof(struct sockaddr_in));
    inet_aton(CLIENT_IP, &client_msr_addr.sin_addr);
    client_msr_addr.sin_family = AF_INET;
    client_msr_addr.sin_port = htons(MEASURE_PORT);

    control_socket = connect_tcp_server(&server_sync_addr); /* TODO: Use server.h */
    msr_socket = measure_connect(&server_msr_addr);
	uint32_t i_pkt = 0;
    while(status != TERMINATE) {
        switch(status) {
			case SYNCHRONIZE:
                PRINT(LOG_NOTICE,"SYNCHRONIZE\n");
				synchronize_start();
                DEBUG_PRINT("\n\n");
				break;

			case MEASURE:
                PRINT(LOG_NOTICE,"MEASURE\n");
                /* XXX: needs fixing */
                measure(&csi_pkt[i_pkt], 1, &server_msr_addr, &client_msr_addr, msr_socket, plot_socket);
                status = CHECK_MEASUREMENT;
                // PRINT(LOG_NOTICE,"END MEASURE \n");
				break;

            case CHECK_MEASUREMENT:
                DEBUG_PRINT("CHECK MEASUREMENT\n");
                status = check_measurement(&csi_pkt[i_pkt]);
                DEBUG_PRINT("\n\n");
                break;

            case SIGNAL_REPEAT:
                DEBUG_PRINT("SIGNAL REPEAT\n");
                /* Signal to repeat and if necessary discard measurement i_pkt. */
                signal_repeat(control_socket);
                status = SYNCHRONIZE;
                DEBUG_PRINT("\n\n");
                break;

            case SIGNAL_OK:
                // PRINT(LOG_NOTICE,"SIGNAL OK\n");
                if(signal_ok(control_socket, i_pkt)) {
                    i_pkt++;
                    if(i_pkt < MEASURES_TO_TAKE) {
                        status = SYNCHRONIZE;
                    } else {
                        status = GENERATE_KEY;
                    }
                } else {
                    /* Repeat measurement: don't increase i_pkt. */
                    status = SYNCHRONIZE;
                }
                DEBUG_PRINT("\n\n");
                break;

            case GENERATE_KEY:
                PRINT(LOG_NOTICE,"GENERATE KEY\n");
                #if (SEND_COLLECTED_CSI_TO_PC == 1 && CONNECT_SERVER_TO_PC == 1)
                for(int i = 0; i < N_PKT; ++i) {
                    send(plot_socket, &csi_pkt[i].hdr, sizeof(csi_pkt[i].hdr), 0);
                    send(plot_socket, csi_pkt[i].matrix, csi_pkt[i].hdr.csi_len, 0);
                }
                #endif
                generate_key(&key);

                status = TERMINATE;
                // PRINT(LOG_NOTICE,"END of GENERATE KEY\n");
                break;

            case VALIDATE_KEY:
                // PRINT(LOG_NOTICE,"VALIDATE KEY\n");
                /* TODO: Calculate hash from key and salt and compare it with client hash. */
                #if 1
                if(validate_key(key)) {
                    status = TERMINATE;
                    // PRINT(LOG_NOTICE,"KEY GENERATION SUCCESSFUL!\n");
                } else {
                    /* Try again. */
                    i_pkt = 0;
                    // status = SYNCHRONIZE;
                    status = TERMINATE;

                }
                #endif
                // PRINT(LOG_NOTICE,"END of VALIDATE KEY\n");
                break;

			default:
				/* should never end up here, otherwise something went wrong. */
				PRINT(LOG_ERR, "Unkown server state.\n");
				exit(EXIT_FAILURE);
		}
	}

    close(control_socket);
	close(plot_socket);
	exit(EXIT_SUCCESS);
}
