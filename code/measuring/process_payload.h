#ifndef _PROCESS_PAYLOAD_H_
#define _PROCESS_PAYLOAD_H_

#include <stdint.h>
#include <stdbool.h>

#define ETH_ALEN					6

#define IEEE80211_FCTL_FTYPE		0x0c00
#define IEEE80211_FTYPE_DATA		0x0800
#define IEEE80211_STYPE_QOS_DATA	0x8000

#define SCTP_DATA					0x00

struct ieee80211_ht_hdr {
	uint16_t frame_control;		// little endian
	uint16_t duration_id;		// little endian
	uint8_t addr1[ETH_ALEN];
	uint8_t addr2[ETH_ALEN];
	uint8_t addr3[ETH_ALEN];
	uint16_t seq_ctrl;			// little endian
	uint8_t addr4[ETH_ALEN];
	uint16_t qos_ctrl;			// little endian
	uint32_t ht_ctrl;
} __attribute__((packed, aligned(2)));

struct sctphdr {
	uint16_t src_port;
	uint16_t dest_port;
	uint32_t v_tag;
	uint32_t checksum;
} __attribute__((packed));

struct sctp_chunkhdr {
	uint8_t chunk_type;
	uint8_t chunk_flags;
	uint16_t chunk_length;
} __attribute__((packed));

struct sctp_data_chunk {
	uint8_t chunk_type;
	uint8_t chunk_flags;
	uint16_t chunk_length;
	uint32_t chunk_tsn;
	uint16_t chunk_stream_id;
	uint16_t chunk_stream_seq_nr;
	uint32_t chunk_pld_proto_id;
	uint8_t *chunk_payload;
} __attribute__((packed));

uint16_t get_user_payload(uint8_t const *const, int32_t const, uint8_t *const, uint16_t);

#endif
