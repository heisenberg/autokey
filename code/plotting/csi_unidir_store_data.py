#!/usr/bin/env python3

from csi_utils import CSIReceiver
from gui.csi_types import DTYPE_CSI_HDR, DTYPE_CSI_COMPLEX, DTYPE_C_TIMESTAMP
from gui.pyqtgraph.pyqtgraph.Qt import QtCore, QtGui
import gui.pyqtgraph.pyqtgraph as pg
#~ from csi_utils import TsCSIReceiver

import os
import socket
import threading
import queue
import numpy as np
from pathlib import Path

# Create Thread
SERVER_PORT = 51111
server_queue = queue.Queue()
server_path = Path('./csi_offline_bin_data/csi-unidirectional.npy')
server_thread = CSIReceiver(1, "CSI receiver thread", server_queue, SERVER_PORT)
#~ server_thread = TsCSIReceiver(1, "CSI with timestamp receiver thread", server_queue, LOCAL_IP, SERVER_PORT)
  
# Start Thread
server_thread.start()

active = True
while active:
    try:
        #~ # Get (kernel software) timestamp
        #~ server_ts = np.frombuffer(server_queue.get(), dtype=DTYPE_C_TIMESTAMP)
        #~ print(server_ts["nsec"])

        # Get header
        server_hdr = np.frombuffer(server_queue.get(), dtype=DTYPE_CSI_HDR)
        rx_s = server_hdr[0][8]
        tx_s = server_hdr[0][9]
        num_tones_s = server_hdr[0][7]

        if rx_s != 3 or tx_s != 3:
            server_queue.get()
            print("Wrong antenna configuration: (" + str(rx_s) + "," + str(tx_s) + ") drop packet.")
        else:
            s_csi = np.frombuffer(server_queue.get(), dtype=DTYPE_CSI_COMPLEX)
            server_csi = s_csi.reshape(rx_s, tx_s, num_tones_s)
            with server_path.open('ab') as server_fd:
                print("Write " + str(rx_s) + "x" + str(tx_s) + " csi data to .npy bin file.")
                np.save(server_fd, server_hdr)
                np.save(server_fd, server_csi)
        
    except KeyboardInterrupt:
        print("Terminating.")
        server_thread.stop()
        try:
            server_thread.join(3)
            active = False
        except Exception:
            pass
            
    except Exception:
        active = False
