#!/usr/bin/env python3

import numpy as np
import pyqtgraph.pyqtgraph as pg

# The objects of this class are used to display different plots of csi data.
# TODO: Common function to check length of csi matrix and antenna combinations
#       before plotting it (currently there is some code duplication).
class VisualizationPlotWidget(pg.PlotItem):
    def __init__(self, rx, tx, title, gui_data):
        super().__init__()
        self.rx = rx
        self.tx = tx
        self.title = title
        
        self.gui_data = gui_data
        
        # variables for pearson correlation plot
        self.x_values = np.arange(-49, 1)
        self.y_values = np.zeros(50)
        self.bar_graph = pg.BarGraphItem(x = self.x_values, height = self.y_values, width = 0.6)
        self.bar_color = dict(pens = ["r"] * len(self.x_values), brushes = ["r"] * len(self.x_values))
                        
        self.enableAutoRange(enable=True)
        self.showGrid(True, True)
        self.addItem(pg.ScatterPlotItem(size=6, pen=pg.mkPen(None)))    
        self.setLabel(axis="left", text="Im")
        self.setLabel(axis="bottom", text="Re")
        self.setTitle(self.title)
            
    # Updates visualization with new data.
    def redraw(self, ptype_idx):
        if(ptype_idx <= 2):
            self.draw(ptype_idx)        
        elif(ptype_idx == 3):
            self.draw_pearson_correlation()
        
        self.setTitle(self.title) 
        self.autoRange()
    
    def draw(self, ptype_idx):
        self.clear()
        
        measurement_nr_a = self.gui_data.measurement_nr_a
        measurement_nr_b = self.gui_data.measurement_nr_b
        if(len(self.gui_data.alice_csi.headers) > 0):
            header_a = self.gui_data.alice_csi.headers[measurement_nr_a]
        if(len(self.gui_data.bob_csi.headers) > 0):
            header_b = self.gui_data.bob_csi.headers[measurement_nr_b]
        
        if(ptype_idx == 0):
            if(len(self.gui_data.bob_csi.phase) > 0):
                if self.rx < header_b['nr'] and self.tx < header_b['nc']:
                    self.plot(self.gui_data.bob_csi.phase[measurement_nr_b][self.rx][self.tx][:], pen=(70, 70, 255), name="s_csi")
            
            if(len(self.gui_data.alice_csi.phase) > 0):
                if self.gui_data.unidirectional_plot:
                    if self.rx < header_a['nr'] and self.tx < header_a['nc']:
                        self.plot(self.gui_data.alice_csi.phase[measurement_nr_a][self.rx][self.tx][:], pen=(240, 240, 60), name="c_csi")
                else:
                    if self.tx < header_a['nr'] and self.rx < header_a['nc']:
                        self.plot(self.gui_data.alice_csi.phase[measurement_nr_a][self.tx][self.rx][:], pen=(240, 240, 60), name="c_csi")

            self.setLabel(axis="left", text="Phase")
            self.setLabel(axis="bottom", text="Subcarrier index")
             
        elif(ptype_idx == 1):
            if(len(self.gui_data.bob_csi.amp) > 0):
                # check metadata for antenna configuration before drawing
                # to see if data is available for that configuration
                if self.rx < header_b['nr'] and self.tx < header_b['nc']:
                    self.plot(self.gui_data.bob_csi.amp[measurement_nr_b][self.rx][self.tx][:], pen=(70, 70, 255), name="s_csi")
            
            if(len(self.gui_data.alice_csi.amp) > 0):
                if self.gui_data.unidirectional_plot:
                    if self.rx < header_a['nr'] and self.tx < header_a['nc']:
                        self.plot(self.gui_data.alice_csi.amp[measurement_nr_a][self.rx][self.tx][:], pen=(240, 240, 60), name="c_csi")
                else:
                    if self.tx < header_a['nr'] and self.rx < header_a['nc']:
                        self.plot(self.gui_data.alice_csi.amp[measurement_nr_a][self.tx][self.rx][:], pen=(240, 240, 60), name="c_csi")

            self.setLabel(axis="left", text="Amplitude")
            self.setLabel(axis="bottom", text="Subcarrier index")

        elif(ptype_idx == 2):
            self.clear()
            
            spots = pg.ScatterPlotItem(size=6, pen=pg.mkPen(None))
            self.addItem(spots)
            
            self.showGrid(True, True)
            self.setLabel(axis="left", text="Im")
            self.setLabel(axis="bottom", text="Re")
            
            # Shows the centeroid of all subcarrier measurements for each antenna combination
            if self.gui_data.show_centeroid:
                if(len(self.gui_data.bob_csi.csi) > 0):
                    if self.rx < header_b['nr'] and self.tx < header_b['nc']:
                        b_csi = self.gui_data.bob_csi.csi[measurement_nr_b][:][:][:]['real'] + 1.j*self.gui_data.bob_csi.csi[measurement_nr_b][:][:][:]['imag']
                        b_csi_sum = np.sum(b_csi[self.rx][self.tx][:])
                        b_csi_center = b_csi_sum / self.gui_data.nr_of_subcarriers
                        spots_center_b = [{'pos': [b_csi_center.real, b_csi_center.imag], 'brush': pg.mkBrush(255, 50, 50, 200)}]
                        spots.addPoints(spots_center_b)
                
                if(len(self.gui_data.alice_csi.csi) > 0):
                    if self.gui_data.unidirectional_plot:
                        if self.rx < header_a['nr'] and self.tx < header_a['nc']:
                            a_csi = self.gui_data.alice_csi.csi[measurement_nr_a][:][:][:]['real'] + 1.j*self.gui_data.alice_csi.csi[measurement_nr_a][:][:][:]['imag']
                            a_csi_sum = np.sum(a_csi[self.rx][self.tx][:])
                            a_csi_center = a_csi_sum / self.gui_data.nr_of_subcarriers
                            spots_center_a = [{'pos': [a_csi_center.real, a_csi_center.imag], 'brush': pg.mkBrush(255, 50, 50, 200)}]
                            spots.addPoints(spots_center_a)
                    else:
                        if self.tx < header_a['nr'] and self.rx < header_a['nc']:
                            a_csi = self.gui_data.alice_csi.csi[measurement_nr_a][:][:][:]['real'] + 1.j*self.gui_data.alice_csi.csi[measurement_nr_a][:][:][:]['imag']
                            a_csi_sum = np.sum(a_csi[self.tx][self.rx][:])
                            a_csi_center = a_csi_sum / self.gui_data.nr_of_subcarriers
                            spots_center_a = [{'pos': [a_csi_center.real, a_csi_center.imag], 'brush': pg.mkBrush(255, 50, 50, 200)}]
                            spots.addPoints(spots_center_a)
            
            # Scatter plot of the CSI data
            if(len(self.gui_data.bob_csi.csi) > 0):
                if self.rx < header_b['nr'] and self.tx < header_b['nc']:
                    spots_b = [{'pos': self.gui_data.bob_csi.csi[measurement_nr_b][self.rx][self.tx][i], 'brush': pg.mkBrush(70, 70, 255, 140)} for i in range(self.gui_data.nr_of_subcarriers)]
                    spots.addPoints(spots_b)
                
            if(len(self.gui_data.alice_csi.csi) > 0):
                    if self.gui_data.unidirectional_plot:
                        if self.rx < header_a['nr'] and self.tx < header_a['nc']:
                            spots_a = [{'pos': self.gui_data.alice_csi.csi[measurement_nr_a][self.rx][self.tx][i], 'brush': pg.mkBrush(240, 240, 60, 120)} for i in range(self.gui_data.nr_of_subcarriers)]
                            spots.addPoints(spots_a)
                    else:
                        if self.tx < header_a['nr'] and self.rx < header_a['nc']:         
                            spots_a = [{'pos': self.gui_data.alice_csi.csi[measurement_nr_a][self.tx][self.rx][i], 'brush': pg.mkBrush(240, 240, 60, 120)} for i in range(self.gui_data.nr_of_subcarriers)]        
                            spots.addPoints(spots_a)
        
    # Calculates pearson correlation coefficients and visualizes them as bar plot
    def draw_pearson_correlation(self):        
        self.clear()
        self.showGrid(True, True)
        self.addItem(self.bar_graph)
        self.setLabel(axis="left", text="Correlation")
        self.setLabel(axis="bottom", text="Packet number")
        
        measurement_nr_a = self.gui_data.measurement_nr_a
        measurement_nr_b = self.gui_data.measurement_nr_b
        header_a = self.gui_data.alice_csi.headers[measurement_nr_a]
        header_b = self.gui_data.bob_csi.headers[measurement_nr_b]
        
        if(len(self.gui_data.bob_csi.phase) > 0 and len(self.gui_data.alice_csi.phase) > 0):
            if self.rx < header_b['nr'] and self.tx < header_b['nc']:
                if self.gui_data.unidirectional_plot:
                    if self.rx < header_a['nr'] and self.tx < header_a['nc']:
                        self.x_values[:-1] = self.x_values[1:]
                        self.x_values[-1] = self.x_values[-1] + 1
                        self.y_values[:-1] = self.y_values[1:]
                        self.y_values[-1] = np.corrcoef(self.gui_data.bob_csi.phase[measurement_nr_b][self.rx][self.tx][:], self.gui_data.alice_csi.phase[measurement_nr_a][self.rx][self.tx][:])[0][1]
                        
                        # Set color of bar and move colors with bars
                        self.bar_color["pens"][:-1] = self.bar_color["pens"][1:]
                        self.bar_color["brushes"][:-1] = self.bar_color["brushes"][1:]
                        if self.y_values[-1] >= self.gui_data.correlation_threshold:
                            self.bar_color["pens"][-1] = "g"
                            self.bar_color["brushes"][-1] = "g"
                            self.bar_graph.setOpts(**self.bar_color)
                        else:
                            self.bar_color["pens"][-1] = "r"
                            self.bar_color["brushes"][-1] = "r"
                            self.bar_graph.setOpts(**self.bar_color)
                        self.bar_graph.setOpts(height = self.y_values)

                else:
                    if self.tx < header_a['nr'] and self.rx < header_a['nc']:
                        self.x_values[:-1] = self.x_values[1:]
                        self.x_values[-1] = self.x_values[-1] + 1
                        self.y_values[:-1] = self.y_values[1:]
                        
                        csi_b = self.gui_data.bob_csi.phase[measurement_nr_b][self.rx][self.tx][:]
                        csi_a = self.gui_data.alice_csi.phase[measurement_nr_a][self.tx][self.rx][:]                            
                        self.y_values[-1] = np.corrcoef(csi_b, csi_a)[0][1]

                        # Set color of bar and move colors with bars
                        self.bar_color["pens"][:-1] = self.bar_color["pens"][1:]
                        self.bar_color["brushes"][:-1] = self.bar_color["brushes"][1:]

                        if self.y_values[-1] >= self.gui_data.correlation_threshold:
                            self.bar_color["pens"][-1] = "g"
                            self.bar_color["brushes"][-1] = "g"
                            self.bar_graph.setOpts(**self.bar_color)

                        else:
                            self.bar_color["pens"][-1] = "r"
                            self.bar_color["brushes"][-1] = "r"
                            self.bar_graph.setOpts(**self.bar_color)

                        self.bar_graph.setOpts(height = self.y_values)
