  close all; clear all; 
%function [bitsAlice, bitsBob,misBits, idxStartPkt, entropyBits ] = parameterEst()


varA = 'params_a';
paramsAlice = load('~/Documents/fu-git/autokey/code/plotting/params_a.mat', varA);
%paramsAlice = load('params_a_310519.mat', varA);
paramsAlice = paramsAlice.(varA);


varB = 'params_b';
paramsBob = load('~/Documents/fu-git/autokey/code/plotting/params_b.mat', varB);
%paramsBob = load('params_b_310519.mat', varB);
paramsBob = paramsBob.(varB);

%% Set params

%%% Gain mismatch eA
eAGainAliceRx0Tx0 = (paramsAlice(:, 1, 1)); 

eAGainBobRx0Tx0 = paramsBob(:, 1, 1);

nrSubCarrier = 56; % put 56(if BW= 20) or 114 (if Bw = 40) for Atheros based NIC
nrOmmitSubcarr = 7; % number of ignored subcarrier from both sides, so its 7+7=14, i.e effective subcarriers = 42;
lastIdx = [];

format short % short is used to limit precision

idxSizeServer = size(eAGainAliceRx0Tx0,1);
idxSizeClient = size(eAGainBobRx0Tx0,1);

if idxSizeServer > idxSizeClient
    idxSizeDif = idxSizeServer - idxSizeClient;
    lastIdx = idxSizeServer-idxSizeDif;
    
else
    idxSizeDif = idxSizeClient - idxSizeServer;
    lastIdx = idxSizeClient - idxSizeDif;
end


%% Quantize eA: Alice
%

    MeAGainAliceRx0Tx0 = mean(eAGainAliceRx0Tx0);
    SeAGainAliceRx0Tx0 = std(eAGainAliceRx0Tx0);
  %  scaledPhaseAliceRx0Tx0 = rawPhaseAliceRx0Tx0 - mean(rawPhaseAliceRx0Tx0);
   % scaledMaxMin = [min(scaledPhaseAliceRx0Tx0), max(scaledPhaseAliceRx0Tx0)];

    QeAGainAliceRx0Tx0 = [];
    for i = 1:lastIdx
        if eAGainAliceRx0Tx0(i,1) > MeAGainAliceRx0Tx0
            QeAGainAliceRx0Tx0(i,1) = 1;
        else QeAGainAliceRx0Tx0(i,1) = 0;
            
        end
    end
    
    
   % quantPhaseAlicePre = QeAGainAliceRx0Tx0;
%}

%% Quantize eA: Bob
%

    MeAGainBobRx0Tx0 = mean(eAGainBobRx0Tx0);
    SeAGainBobRx0Tx0 = std(eAGainBobRx0Tx0);
  %  scaledPhaseBobRx0Tx0 = rawPhaseBobRx0Tx0 - mean(rawPhaseBobRx0Tx0);
   % scaledMaxMin = [min(scaledPhaseBobRx0Tx0), max(scaledPhaseBobRx0Tx0)];

    QeAGainBobRx0Tx0 = [];
    for i = 1:lastIdx
        if eAGainBobRx0Tx0(i,1) > MeAGainBobRx0Tx0
            QeAGainBobRx0Tx0(i,1) = 1;
        else QeAGainBobRx0Tx0(i,1) = 0;
            
        end
    end
    
    
   % quantPhaseBobPre = QeAGainBobRx0Tx0;
%}

%{
subplot(3,3,2);
plot(subcarrier_index, ((ePhaseAliceRx0Tx0(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index,( (ePhaseBobRx0Tx0(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
% vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
% vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
% vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
% vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
% vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('\epsilon_\theta' );
xlabel('Subcarrier index range');
%ylabel('Phase(Rad)');
%legend('Alice','Bob' );
% str0 = sprintf('Normalized Amplitudes, #packets = %d',num_packets) ;
% title(str0)

subplot(3,3,3);
plot(subcarrier_index, ((zTimeAliceRx0Tx0(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index,( (zTimeBobRx0Tx0(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
% vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
% vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
% vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
% vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
% vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('\zeta' );
xlabel('Subcarrier index range');
%ylabel('Phase(Rad)');
%legend('Alice','Bob' );
% str0 = sprintf('Normalized Amplitudes, #packets = %d',num_packets) ;
% title(str0)

subplot(3,3,4)
hA= histogram(((eAGainAliceRx0Tx0(firstIdx:lastIdx))));
set(hA(1), 'facecolor', 'b', 'facealpha' , .5, 'edgecolor','none')

hold on 
hB= histogram(((eAGainBobRx0Tx0(firstIdx:lastIdx))));
set(hB(1), 'facecolor', 'r', 'facealpha' , .5, 'edgecolor','none')

hold off
title('Histogram \epsilon_A')

subplot(3,3,5)
hA= histogram(((ePhaseAliceRx0Tx0(firstIdx:lastIdx))));
set(hA(1), 'facecolor', 'b')
title('Histogram \epsilon_\theta')

subplot(3,3,6)
hA= histogram(((zTimeAliceRx0Tx0(firstIdx:lastIdx))));
set(hA(1), 'facecolor', 'b')
title('Histogram \zeta')
%{
subplot(3,3,2);
plot(subcarrier_index, unwrap(angle(AliceRx0Tx1(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index, unwrap(angle(BobRx0Tx1(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('AliceRx0Tx1 and BobRx0Tx1' );
xlabel('Subcarrier index');
ylabel('Phase(Rad)');
%legend('Bob\_to\_Alice','Alice\_to\_Bob' );

subplot(3,3,3);
plot(subcarrier_index,unwrap( angle(AliceRx0Tx2(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index, unwrap(angle(BobRx0Tx2(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('AliceRx0Tx2 and BobRx0Tx2' );
xlabel('Subcarrier index');
ylabel('Phase(Rad)');
%legend('Bob\_to\_Alice','Alice\_to\_Bob' );

subplot(3,3,4);
plot(subcarrier_index,unwrap( angle(AliceRx1Tx0(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index, unwrap(angle(BobRx1Tx0(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('AliceRx1Tx0 and BobRx1Tx0' );
xlabel('Subcarrier index');
ylabel('Phase(Rad)');
%legend('Bob\_to\_Alice','Alice\_to\_Bob' );

subplot(3,3,5);
plot(subcarrier_index, unwrap(angle(AliceRx1Tx1(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index,unwrap( angle(BobRx1Tx1(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('AliceRx1Tx1 and BobRx1Tx1' );
xlabel('Subcarrier index');
ylabel('Phase(Rad)');
%legend('Bob\_to\_Alice','Alice\_to\_Bob' );

subplot(3,3,6);
plot(subcarrier_index, unwrap(angle(AliceRx1Tx2(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index, unwrap(angle(BobRx1Tx2(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('AliceRx1Tx2 and BobRx1Tx2' );
xlabel('Subcarrier index');
ylabel('Phase(Rad)');
%legend('Bob\_to\_Alice','Alice\_to\_Bob' );

subplot(3,3,7);
plot(subcarrier_index,unwrap( angle(AliceRx2Tx0(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index,unwrap( angle(BobRx2Tx0(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('AliceRx2Tx0 and BobRx2Tx0' );
xlabel('Subcarrier index');
ylabel('Phase(Rad)');
%legend('Bob\_to\_Alice','Alice\_to\_Bob' );

subplot(3,3,8);
plot(subcarrier_index, unwrap(angle(AliceRx2Tx1(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index, unwrap(angle(BobRx2Tx1(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('AliceRx2Tx1 and BobRx2Tx1' );
xlabel('Subcarrier index');
ylabel('Phase(Rad)');
%legend('Bob\_to\_Alice','Alice\_to\_Bob' );

subplot(3,3,9);
%yticks([-4])
plot(subcarrier_index, unwrap(angle(AliceRx2Tx2(firstIdx:lastIdx))), 'b', 'LineWidth', 1)
hold on
plot(subcarrier_index,unwrap(angle(BobRx2Tx2(firstIdx:lastIdx))), 'r', 'LineWidth', 1)
vline([(nrSubCarrier/2)+firstIdx,  ],'-k', '0');
vline([((nrSubCarrier/2)-7)+firstIdx,  ],'--m', '-7');
vline([((nrSubCarrier/2)-21)+firstIdx,  ],'--m', '-21');
vline([((nrSubCarrier/2)+7)+firstIdx,  ],'--m', '+7');
vline([((nrSubCarrier/2)+21)+firstIdx,  ],'--m', '+21');
title('AliceRx2Tx2 and BobRx2Tx2' );
xlabel('Subcarrier index');
ylabel('Phase(Rad)');
%legend('Bob\_to\_Alice','Alice\_to\_Bob' );

hold off
%}


%}


%QeAGainAliceRx0Tx0';
%QeAGainBobRx0Tx0;


%% Bits disagreement eA
%
%compeABits= bitxor(QeAGainAliceRx0Tx0, QeAGainBobRx0Tx0);
%miseAbits= sum(compeABits(:)==1)


%% pre entropy
%{
quantAlicePre = QeAGainAliceRx0Tx0;
quantBobPre = QeAGainBobRx0Tx0;

entropAlicePre = entropy(quantAlicePre);
entropBobPre = entropy (quantBobPre);
%}
%{

figure()
stem( QeAGainAliceRx0Tx0(2500:end), 'LineWidth', 1.5, 'MarkerFaceColor', 'blue');
hold on
stem( QeAGainBobRx0Tx0(2500:end),'LineWidth', 1, 'MarkerEdgeColor', 'red');
hold off
ylim([-1 1.5]);
xlabel('Packet')
ylabel('Bit value')
title('\epsilon_A')
legend('Alice', 'Bob')
txt = { 'Nr of BitsDisagreement: ' num2str(miseAbits)   'Nr of packets:' num2str(size(QeAGainAliceRx0Tx0, 1))   'Ratio %: ' num2str((miseAbits/size(QeAGainAliceRx0Tx0, 1))*100) };
text(64, -0.5, txt);
%}


%
%% Quantize ePhase: Alice
%{

    MePhaseAliceRx0Tx0 = mean(ePhaseAliceRx0Tx0);
    SePhaseAliceRx0Tx0 = std(ePhaseAliceRx0Tx0);
  %  scaledPhaseAliceRx0Tx0 = rawPhaseAliceRx0Tx0 - mean(rawPhaseAliceRx0Tx0);
   % scaledMaxMin = [min(scaledPhaseAliceRx0Tx0), max(scaledPhaseAliceRx0Tx0)];

    QePhaseAliceRx0Tx0 = [];
    for i = 1:lastIdx
        if ePhaseAliceRx0Tx0(i,1) > MePhaseAliceRx0Tx0
            QePhaseAliceRx0Tx0(i,1) = 1;
        else QePhaseAliceRx0Tx0(i,1) = 0;
            
        end
    end
    
    
   % quantPhaseAlicePre = QeAGainAliceRx0Tx0;
%}

%% Quantize ePhase: Bob
%{

    MePhaseBobRx0Tx0 = mean(ePhaseBobRx0Tx0);
    SePhaseBobRx0Tx0 = std(ePhaseBobRx0Tx0);
  %  scaledPhaseBobRx0Tx0 = rawPhaseBobRx0Tx0 - mean(rawPhaseBobRx0Tx0);
   % scaledMaxMin = [min(scaledPhaseBobRx0Tx0), max(scaledPhaseBobRx0Tx0)];

    QePhaseBobRx0Tx0 = [];
    for i = 1:lastIdx
        if ePhaseBobRx0Tx0(i,1) > MePhaseBobRx0Tx0
            QePhaseBobRx0Tx0(i,1) = 1;
        else QePhaseBobRx0Tx0(i,1) = 0;
            
        end
    end
    
    
   % quantPhaseBobPre = QeAGainBobRx0Tx0;
%}

%% Bits disagreement ePhase
%{
comparePhaseBits= bitxor(QePhaseAliceRx0Tx0, QePhaseBobRx0Tx0);
misBitsePhase= sum(comparePhaseBits(:)==1)
%}
%{

figure()
bar( QePhaseAliceRx0Tx0, 'FaceColor', 'b');
hold on
bar( QePhaseBobRx0Tx0, 'FaceColor', 'r');
hold off
ylim([-1 1.5]);
xlabel('Packet')
ylabel('Bit value')
title('\epsilon_\theta')
legend('Alice', 'Bob')
txt = { 'Nr of BitsDisagreement: ' num2str(misBitsePhase)   'Nr of packets:' num2str(size(QePhaseAliceRx0Tx0, 1))   'Ratio %: ' num2str((misBitsePhase/size(QeAGainAliceRx0Tx0, 1))*100) };
text(64, -0.5, txt);
%}
%

%
%% Quantize Lambda: Alice
%{

    MeLambdAliceRx0Tx0 = mean(lamdAliceRx0Tx0);
    SeLambdAliceRx0Tx0 = std(lamdAliceRx0Tx0);
  %  scaledPhaseAliceRx0Tx0 = rawPhaseAliceRx0Tx0 - mean(rawPhaseAliceRx0Tx0);
   % scaledMaxMin = [min(scaledPhaseAliceRx0Tx0), max(scaledPhaseAliceRx0Tx0)];

    QlambdAliceRx0Tx0 = [];
    for i = 1:lastIdx
        if ePhaseAliceRx0Tx0(i,1) > MeLambdAliceRx0Tx0
            QlambdAliceRx0Tx0(i,1) = 1;
        else QlambdAliceRx0Tx0(i,1) = 0;
            
        end
    end
    
    
   % quantPhaseAlicePre = QeAGainAliceRx0Tx0;
%}

%% Quantize Lambda: Bob
%{

    MeLambdBobRx0Tx0 = mean(lamdBobRx0Tx0);
    SeLambdBobRx0Tx0 = std(lamdBobRx0Tx0);
  %  scaledPhaseBobRx0Tx0 = rawPhaseBobRx0Tx0 - mean(rawPhaseBobRx0Tx0);
   % scaledMaxMin = [min(scaledPhaseBobRx0Tx0), max(scaledPhaseBobRx0Tx0)];

    QlamdBobRx0Tx0 = [];
    for i = 1:lastIdx
        if lamdBobRx0Tx0(i,1) > MeLambdBobRx0Tx0
            QlamdBobRx0Tx0(i,1) = 1;
        else QlamdBobRx0Tx0(i,1) = 0;
            
        end
    end
    
    
   % quantPhaseBobPre = QeAGainBobRx0Tx0;
%}

%% Bits disagreement Lambda
%{
compareLambdBits= bitxor(QlambdAliceRx0Tx0, QlamdBobRx0Tx0);
misBitseLambd= sum(compareLambdBits(:)==1)
%}
%{

figure()
bar( QlambdAliceRx0Tx0, 'FaceColor', 'b');
hold on
bar( QlamdBobRx0Tx0, 'FaceColor', 'r');
hold off
ylim([-1 1.5]);
xlabel('Packet')
ylabel('Bit value')
title('\lambda')
legend('Alice', 'Bob')
txt = { 'Nr of BitsDisagreement: ' num2str(misBitseLambd)   'Nr of packets:' num2str(size(QlambdAliceRx0Tx0, 1))   'Ratio %: ' num2str((misBitseLambd/size(QlambdAliceRx0Tx0, 1))*100) };
text(64, -0.5, txt);
%}
%

%
%% Quantize Beta: Alice
%{

    MeBetaAliceRx0Tx0 = mean(betaAliceRx0Tx0);
    SeBetaAliceRx0Tx0 = std(betaAliceRx0Tx0);
  %  scaledPhaseAliceRx0Tx0 = rawPhaseAliceRx0Tx0 - mean(rawPhaseAliceRx0Tx0);
   % scaledMaxMin = [min(scaledPhaseAliceRx0Tx0), max(+scaledPhaseAliceRx0Tx0)];

    QBetaAliceRx0Tx0 = [];
    for i = 1:lastIdx
        if betaAliceRx0Tx0(i,1) > MeBetaAliceRx0Tx0
            QBetaAliceRx0Tx0(i,1) = 1;
        else QBetaAliceRx0Tx0(i,1) = 0;
            
        end
    end
    
    
   % quantPhaseAlicePre = QeAGainAliceRx0Tx0;
%}

%% Quantize Beta: Bob
%{
    MeBetaBobRx0Tx0 = mean(betaBobRx0Tx0);
    SeBetaBobRx0Tx0 = std(betaBobRx0Tx0);
  %  scaledPhaseBobRx0Tx0 = rawPhaseBobRx0Tx0 - mean(rawPhaseBobRx0Tx0);
   % scaledMaxMin = [min(scaledPhaseBobRx0Tx0), max(scaledPhaseBobRx0Tx0)];

    QBetaBobRx0Tx0 = [];
    for i = 1:lastIdx
        if betaBobRx0Tx0(i,1) > MeBetaBobRx0Tx0
            QBetaBobRx0Tx0(i,1) = 1;
        else QBetaBobRx0Tx0(i,1) = 0;
            
        end
    end
    
    
   % quantPhaseBobPre = QeAGainBobRx0Tx0;
%}

%% Bits disagreement Beta
%{
compareBetaBits= bitxor(QBetaAliceRx0Tx0, QBetaBobRx0Tx0);
misBitsBeta= sum(compareBetaBits(:)==1)
%}
%{

figure()
bar( QBetaAliceRx0Tx0, 'FaceColor', 'b');
hold on
bar( QBetaBobRx0Tx0, 'FaceColor', 'r');
hold off
ylim([-1 1.5]);
xlabel('Packet')
ylabel('Bit value')
title('\beta')
legend('Alice', 'Bob')
txt = { 'Nr of BitsDisagreement: ' num2str(misBitsBeta)   'Nr of packets:' num2str(size(QBetaAliceRx0Tx0, 1))   'Ratio %: ' num2str((misBitsBeta/size(QeAGainAliceRx0Tx0, 1))*100) };
text(64, -0.5, txt);
%}
%


%% Reconciliation
%

%%%% Randomly select "pre-key" bits from quantized bits

keyLength = 127; % due to restrictions on fuzzy exctractor algorithm key, k is always 1 bit less

nrBlock = mod(lastIdx,keyLength);

nrKeyBlock = [1:nrBlock];

msize = numel((nrKeyBlock));

idxStartPkt = nrKeyBlock(randperm(msize, 1));

%
clean = QeAGainAliceRx0Tx0(idxStartPkt:(idxStartPkt+(keyLength-1)))';
noisy = QeAGainBobRx0Tx0((idxStartPkt:(idxStartPkt+(keyLength-1))))';


Alice01 = hist(clean);
Alice0 = Alice01(:,1);
Alice1 = Alice01(:,10);
Bob01 = hist(noisy);
Bob0 = Bob01(:,1);
Bob1 = Bob01(:,10);

compareBits= bitxor(clean, noisy);
misBits= sum(compareBits(:)==1)

%
% additional randomness size
kTruePhase = 50; %default is 50
% generate secure sketch and hash, return P (x,s) and R
[sTruePhase, xTruePhase, RgenTruePhase] = secure_sketch_generate(clean,kTruePhase);

% get tolerable error count
%t = bchnumerr(cleanPhase,kPhase);

% create hash with noisy w
RrepTruePhase = secure_sketch_reproduce(noisy,sTruePhase,xTruePhase,kTruePhase);

% verify if unable to reproduce the hash value
(isequal(RgenTruePhase,RrepTruePhase));
disp('Reconciled  Successfully' );

bitsAlice = double(RgenTruePhase); % to convert from logical to double use double(), for integers int32()
bitsBob = double(RrepTruePhase);

AliceKey = DataHash(bitsAlice, 'HEX')
BobKey = DataHash(bitsBob, 'HEX')
(isequal(AliceKey,BobKey));
disp('AliceKey and BobKey are Symmetric' );


%}
%% Save output for csi_gui.py 
%
% TbitsAlice = cell2table(KeyAlice,  'VariableNames', {'Alice'});
% writetable( TbitsAlice, 'AliceKey.txt', 'WriteVariableNames', false, 'Delimiter', 'space');
% 
% TbitsBob = table(KeyBob,  'VariableNames', {'Bob'});
% writetable( TbitsBob, 'BobKey.txt', 'WriteVariableNames', false, 'Delimiter', 'space');

dlmwrite('~/Documents/fu-git/autokey/code/plotting/AliceKey.txt', AliceKey, 'delimiter', '');
dlmwrite('~/Documents/fu-git/autokey/code/plotting/BobKey.txt', BobKey, 'delimiter', '');

quit
%}
%figure()
%hist(bitsAlice, double(min(bitsAlice):max(bitsAlice)))

%{
entropyBitsAlice = entropy((bitsAlice))
entropyBitsBob = entropy(bitsBob)
%}


%end
