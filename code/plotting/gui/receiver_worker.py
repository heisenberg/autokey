#!/usr/bin/env python3

import socket
import queue
import csi_types as ct
import numpy as np
from PyQt5 import QtCore

class ReceiverWorker(QtCore.QObject):
    start = QtCore.pyqtSignal()
    
    # TODO: Refactor function signature
    def __init__(self, queue_vis, queue_log, print_queue, port, xinetd_ip, xinetd_port, name):
        super().__init__()
        self.queue_vis = queue_vis
        self.queue_log = queue_log
        self.print_queue = print_queue
        self.ip = "" # the empty string represents INADDR_ANY
        self.port = port
        self.xinetd_ip = xinetd_ip
        self.xinetd_port = xinetd_port
        self.name = name
        self.init = False
        self.receiving = False
        self.conn = None
        self.s = None
    
    def receive(self):        
        # Start probing services
        if self.receiving and self.conn == None:
            self.pkt_count = 0
            # ~ try:
                # ~ self.xinetd_service_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                # ~ self.xinetd_service_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                # ~ self.xinetd_service_sock.connect((self.xinetd_ip, self.xinetd_port))
            # ~ except Exception as error:
                # ~ print(error)
                # ~ self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Probing service not available. Check network connection.")

        # Initialize socket
        if not self.init:
            try:                
                # Listen for probing application
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.s.bind((self.ip, self.port))
                self.s.listen(1)
                
                self.init = True
            except Exception as error:
                print(error)
                self.s = None
                self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Address probably not available yet. Check network connection.")
        
        # Try to connect socket
        if self.receiving and self.conn == None and self.s != None:
            try:
                self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + " waiting for connection.")
                self.conn, self.addr = self.s.accept()
            except Exception:
                self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": Error connecting " + self.name + ".")
                raise
            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": " + str(self.addr) + " connected.")

        # TODO: Optimize for run time complexity and space. Currently too much data is copied!
        # Try to receive data
        if self.receiving and self.conn != None:            
            # First receive header of 25 bytes
            msg = bytearray()
            bytes_left = ct.CSI_HEADER_SIZE
            while bytes_left:
                data = self.conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
            hdr = np.frombuffer(msg, dtype=ct.DTYPE_CSI_HDR)
        
            # Receive CSI data
            msg = bytearray()
            bytes_left = hdr[0][1]
            while bytes_left:
                data = self.conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
            
            # Copy data to visualization queue.
            self.queue_vis.put(hdr)
            self.queue_vis.put(msg)
            
            # Copy data to logging queue, this thread could be much slower than the visualization
            # thread. Because of this a copy is made so that the visualization is not blocked because
            # of the thread that is accessing the queue and writing to the drive.
            self.queue_log.put(hdr)
            self.queue_log.put(msg)
        
        # Close connection
        if not self.receiving and self.conn != None:
            self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": " + str(self.addr) + " closing.")
            self.conn.close()
            self.conn = None
            # ~ QtCore.QTimer.singleShot(2000, self.receive)
        
        # Connection not possible atm
        elif self.receiving and self.s == None:
            # TODO: Change button state to off
            self.receiving = False
        
        # Avoid infinity loop that would freeze GUI and use signal instead
        elif self.receiving:
            QtCore.QTimer.singleShot(0, self.receive)
            
    def disconnect(self):
        self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": disconnect request.")
        self.receiving = False

    def reconnect(self):
        self.print_queue.put(str(int(QtCore.QThread.currentThreadId())) + ": connect request.")
        while not self.queue_vis.empty():
            try:
                self.queue_vis.get(False)
            except Empty:
                continue
            self.queue_vis.task_done()
        while not self.queue_log.empty():
            try:
                self.queue_log.get(False)
            except Empty:
                continue
            self.queue_log.task_done()
        self.receiving = True
        self.start.emit()
