#!/usr/bin/env python3

from receiver_worker import ReceiverWorker
from logging_worker import LoggingWorker

from PyQt5 import QtCore
from PyQt5.QtCore import QThread

class ReceiverController(QtCore.QObject):
    start = QtCore.pyqtSignal()   # Start execution by emitting this signal
    toggle_logging = QtCore.pyqtSignal()
    disconnect = QtCore.pyqtSignal()
    reconnect = QtCore.pyqtSignal()
    
    receiver_thread_a = QThread()
    receiver_thread_b = QThread()
    logging_thread = QThread()
    
    def __init__(self, gui_data, print_queue):
        super().__init__()
        # TODO: Form to change this while GUI is running
        self.LOCAL_IP = '192.168.1.244'
        self.SERVER_PORT = 51111
        self.XINETD_SERVER_PORT = 53333
        self.CLIENT_PORT = 52222
        self.XINETD_CLIENT_PORT = 54444
        
        # Receiver threads
        self.receiver_a = ReceiverWorker(gui_data.csi_vis_queue_a, gui_data.csi_log_queue_a, print_queue, self.CLIENT_PORT, "192.168.1.2", self.XINETD_CLIENT_PORT, "Alice")
        self.receiver_b = ReceiverWorker(gui_data.csi_vis_queue_b, gui_data.csi_log_queue_b, print_queue, self.SERVER_PORT, "192.168.1.1", self.XINETD_SERVER_PORT, "Bob")
        self.receiver_a.moveToThread(self.receiver_thread_a)
        self.receiver_b.moveToThread(self.receiver_thread_b)
        
        # Logging thread
        self.logger = LoggingWorker(gui_data.csi_log_queue_a, gui_data.csi_log_queue_b, print_queue)
        self.logger.moveToThread(self.logging_thread)
        
        # Connect start signal for workers
        self.receiver_a.start.connect(self.receiver_a.receive)
        self.receiver_b.start.connect(self.receiver_b.receive)
        self.logger.start.connect(self.logger.log)
        
        # Connect start signal for controller
        self.start.connect(self.start_threads)
        # Connect controller signals --> receiver worker slots
        self.disconnect.connect(self.receiver_a.disconnect)
        self.disconnect.connect(self.receiver_b.disconnect)
        self.reconnect.connect(self.receiver_a.reconnect)
        self.reconnect.connect(self.receiver_b.reconnect)
        # Connect logger
        self.toggle_logging.connect(self.logger.toggle_logging)
        self.disconnect.connect(self.logger.disable_logging)
        self.reconnect.connect(self.logger.start)
    
    def set_logging_dt_filter(self, microseconds):
        self.logger.set_dt_filter(microseconds)
        
    def set_logging_corr_filter(self, threshold):
        self.logger.set_corr_filter(threshold)
    
    def start_threads(self):
        # Start threads
        self.receiver_thread_a.start()
        self.receiver_thread_b.start()
        self.logging_thread.start()
