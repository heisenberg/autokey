#!/usr/bin/env python3

import csi_types as ct
from PyQt5 import QtCore, QtGui
import queue
import numpy as np

### visualization settings interface
class VisualizationSettingsWidget(QtGui.QWidget):
    def __init__(self, parent, current_plots, gui_data, print_queue):        
        super().__init__(parent)
        
        self.print_queue = print_queue
        self.current_plots = current_plots
        #~ self.vheader_names = ("k_ts", "delta_ts") + ct.DTYPE_CSI_HDR.names
        self.vheader_names = ct.DTYPE_CSI_HDR.names
        self.hheader_names = ("Alice", "Bob")

        self.STANDARD_FILE_BOB = './csi_offline_bin_data/bob_csi.npy'
        self.STANDARD_FILE_ALICE = './csi_offline_bin_data/alice_csi.npy'
        self.NR_TO_PLOT_NAME = {
            0 : 'RX1 TX1',
            1 : 'RX1 TX2',
            2 : 'RX1 TX3',
            3 : 'RX2 TX1',
            4 : 'RX2 TX2',
            5 : 'RX2 TX3',
            6 : 'RX3 TX1',
            7 : 'RX3 TX2',
            8 : 'RX3 TX3'
        }
        
        self.gui_data = gui_data

        self.__view()
        self.__control()
    
        # Init plots
        # TODO: Write propper loader (e.g. load data chunkwise in offline mode instead of bloating ram with huge file)
        self.load_file("Alice", self.STANDARD_FILE_ALICE)
        self.load_file("Bob", self.STANDARD_FILE_BOB)
        self.redraw_all()
    
    def __view(self):
        ####################
        #### GUI widgets ###
        ####################
        self.metadata_table_widget = QtGui.QTableWidget()
        self.metadata_table_widget.setRowCount(15)
        self.metadata_table_widget.setColumnCount(2)
        self.metadata_table_widget.setHorizontalHeaderLabels(self.hheader_names)
        self.metadata_table_widget.setVerticalHeaderLabels(self.vheader_names)
        self.metadata_table_widget.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        self.metadata_table_widget.setFocusPolicy(QtCore.Qt.NoFocus)
        self.metadata_table_widget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.metadata_table_widget.setShowGrid(True)
        
        # resize table
        self.metadata_table_widget.setFixedSize(71 + self.metadata_table_widget.horizontalHeader().length() + 
            self.metadata_table_widget.verticalHeader().width(), 
            10 + self.metadata_table_widget.verticalHeader().length() + 
            self.metadata_table_widget.horizontalHeader().height())
        
        self.all_sb = QtGui.QSpinBox(self)
        self.alice_sb = QtGui.QSpinBox(self)
        self.alice_sb.setEnabled(False)
        self.bob_sb = QtGui.QSpinBox(self)
        self.bob_sb.setEnabled(False)
        
        # load buttons
        self.load_button_a = QtGui.QPushButton("Open")
        self.load_button_b = QtGui.QPushButton("Open")
        
        # Plot type selection
        self.all_cbs = QtGui.QComboBox(self)
        self.all_cbs.addItem("Phase plot")
        self.all_cbs.addItem("Aplitude plot")
        self.all_cbs.addItem("Scatter plot")
        self.all_cbs.addItem("Pearson correlation")
        self.all_cbs.currentIndexChanged.connect(self.change_all_plot_types)
        self.cb_list = []
        i = 0
        for tx in range(3):
            for rx in range(3):
                self.cb_list.append(QtGui.QComboBox(self))
                self.cb_list[i].addItem("Phase plot")
                self.cb_list[i].addItem("Aplitude plot")
                self.cb_list[i].addItem("Scatter plot")
                self.cb_list[i].addItem("Pearson Correlation")
                self.cb_list[i].currentIndexChanged.connect(lambda: self.current_plots.getItem(rx, tx).redraw)
                i += 1
        
        self.centeroid_cb = QtGui.QCheckBox(self)
        self.uniplot_cb = QtGui.QCheckBox(self)
        
        ##################
        ### GUI layout ###
        ##################
        self.visualization_form = QtGui.QFormLayout()
        self.visualization_form.addRow(QtGui.QLabel("Load Alice's data"), self.load_button_a)
        self.visualization_form.addRow(QtGui.QLabel("Load Bob's data"), self.load_button_b)
        
        self.visualization_form.addRow(QtGui.QLabel("All meas. nr."), self.all_sb)
        self.visualization_form.addRow(QtGui.QLabel("Alice's meas. nr."), self.alice_sb)
        self.visualization_form.addRow(QtGui.QLabel("Bob's meas. nr."), self.bob_sb)
        
        self.visualization_form.addRow("All RX TX", self.all_cbs)
        for i in range(9):
            self.visualization_form.addRow(QtGui.QLabel(self.NR_TO_PLOT_NAME[i]), self.cb_list[i])
        
        self.visualization_form.addRow(QtGui.QLabel("Show centeroid"), self.centeroid_cb)
        self.visualization_form.addRow(QtGui.QLabel("One-way plot"), self.uniplot_cb)
    
        self.visualization_layout = QtGui.QVBoxLayout(self)
        self.visualization_layout.addLayout(self.visualization_form)
        self.setLayout(self.visualization_layout)

    def __control(self):        
        self.all_sb.valueChanged.connect(lambda: self.set_meas_nr("All"))
        self.alice_sb.valueChanged.connect(lambda: self.set_meas_nr("Alice"))
        self.bob_sb.valueChanged.connect(lambda: self.set_meas_nr("Bob"))
        self.load_button_a.clicked.connect(lambda: self.load_file("Alice", QtGui.QFileDialog.getOpenFileName(caption = 'Open file', directory = './', filter = '*.npy')[0]))
        self.load_button_b.clicked.connect(lambda: self.load_file("Bob", QtGui.QFileDialog.getOpenFileName(caption = 'Open file', directory = './', filter = '*.npy')[0]))
        self.centeroid_cb.stateChanged.connect(self.toggle_centeroid)
        self.uniplot_cb.stateChanged.connect(self.toggle_uniplot)

    def change_all_plot_types(self, ptype):
        for i in range(9):
            self.cb_list[i].setCurrentIndex(ptype)

    def change_plot_type(self, rx, tx, ptype):
        self.current_plots.getItem(rx, tx).redraw(ptype)
        
    def set_meas_nr(self, csi_id=None):             
        if csi_id == "Alice":
            self.gui_data.measurement_nr_a = self.alice_sb.value()
            self.update_metadata("Alice")
        elif csi_id == "Bob":
            self.gui_data.measurement_nr_b = self.bob_sb.value()
            self.update_metadata("Bob")
        elif csi_id == "All":
            if self.alice_sb.isEnabled():
                self.alice_sb.blockSignals(True)
                self.alice_sb.setValue(self.all_sb.value())
                self.alice_sb.blockSignals(False)
                self.gui_data.measurement_nr_a = self.all_sb.value()
            if self.bob_sb.isEnabled():
                self.bob_sb.blockSignals(True)
                self.bob_sb.setValue(self.all_sb.value())
                self.bob_sb.blockSignals(False)
                self.gui_data.measurement_nr_b = self.all_sb.value()
            self.update_metadata("Alice")
            self.update_metadata("Bob")

        self.redraw_all()
        
    def enable_spin_boxes(self, boolean):
        self.alice_sb.setEnabled(boolean)
        self.bob_sb.setEnabled(boolean)
        self.all_sb.setEnabled(boolean)
    
    def online_update_metadata(self):
        self.update_metadata("Alice")
        self.update_metadata("Bob")
        self.redraw_all()

        self.alice_sb.setRange(0, len(self.gui_data.alice_csi.csi) - 1)
        self.bob_sb.setRange(0, len(self.gui_data.bob_csi.csi) - 1)
        self.all_sb.setRange(0, min(len(self.gui_data.alice_csi.csi), len(self.gui_data.bob_csi.csi)) - 1)

        self.gui_data.measurement_nr_a += 1
        self.gui_data.measurement_nr_b += 1

    def update_metadata(self, csi_id):        
        # change metadata
        i = 0
        if csi_id == "Alice" and self.gui_data.alice_csi.headers != []:
            try:
                for data in self.gui_data.alice_csi.headers[self.gui_data.measurement_nr_a][0]:
                    self.metadata_table_widget.setItem(i, 0, QtGui.QTableWidgetItem(str(data)))
                    i = i + 1
            except Exception as error:
                print(len(self.gui_data.alice_csi.headers))
                print("i : " + str(i))
                print(error)
        elif csi_id == "Bob" and self.gui_data.bob_csi.headers != []:
            for data in self.gui_data.bob_csi.headers[self.gui_data.measurement_nr_b][0]:
                self.metadata_table_widget.setItem(i, 1, QtGui.QTableWidgetItem(str(data)))
                i = i + 1
    
    def toggle_centeroid(self):
        self.gui_data.show_centeroid = not self.gui_data.show_centeroid
        self.redraw_all()
        
    def toggle_uniplot(self):
        self.gui_data.unidirectional_plot = not self.gui_data.unidirectional_plot
        self.redraw_all()

    def redraw_all(self):          
        i = 0
        for tx in range(3):
            for rx in range(3):
                self.current_plots.getItem(rx, tx).redraw(self.cb_list[i].currentIndex())
                i += 1

    def load_file(self, csi_id, file_name):            
        if csi_id == "Alice":
            self.gui_data.alice_csi.clear()
            try:
                self.gui_data.alice_csi.set_path(file_name)
                self.gui_data.alice_csi.load()
            except Exception as err:
                self.gui_data.alice_csi.clear()
                self.alice_sb.setEnabled(False)
                self.print_queue.put(str(err))
            else:
                self.alice_sb.setEnabled(True)
                self.alice_sb.setRange(0, len(self.gui_data.alice_csi.csi) - 1)
                self.all_sb.setRange(0, min(len(self.gui_data.alice_csi.csi), len(self.gui_data.bob_csi.csi)) - 1)
        
        elif csi_id == "Bob":
            self.gui_data.bob_csi.clear()
            try:
                self.gui_data.bob_csi.set_path(file_name)
                self.gui_data.bob_csi.load()
            except Exception as err:
                self.gui_data.bob_csi.clear()
                self.bob_sb.setEnabled(False)
                self.print_queue.put(str(err))
            else:
                self.bob_sb.setEnabled(True)
                self.bob_sb.setRange(0, len(self.gui_data.bob_csi.csi) - 1)
                self.all_sb.setRange(0, min(len(self.gui_data.alice_csi.csi), len(self.gui_data.bob_csi.csi)) - 1)
                
        if(not (self.alice_sb.isEnabled() and self.bob_sb.isEnabled())):
            self.all_sb.setEnabled(False)
        else:
            self.all_sb.setEnabled(True)
