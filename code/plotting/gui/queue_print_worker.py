from PyQt5 import QtCore

# Worker for info box thread
class QueuePrintWorker(QtCore.QObject):
    new_info_text = QtCore.pyqtSignal(str)
    stop_signal = QtCore.pyqtSignal()
       
    def __init__(self, queue):
        super().__init__()
        self.queue = queue
        #~ self.running = QtCore.QAtomicInt(1)
        self.running = 1
        self.stop_signal.connect(self.stop)
    
    def run(self):
        while self.running:
            text = self.queue.get()
            self.new_info_text.emit(text)
            
    def stop(self):
        self.running = 0
