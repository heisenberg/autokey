#!/usr/bin/env python3

from csi_utils import CSIReceiver
from gui.csi_types import DTYPE_CSI_HDR, DTYPE_CSI_COMPLEX
import socket
import numpy as np
import matplotlib.pyplot as plt
import textwrap as tw
import threading
import queue
from collections import deque

from io import StringIO
from Crypto.Cipher import AES
from struct import *

DEBUG = 1
LOCAL_IP = '192.168.1.244'
SERVER_PORT = 51111
CLIENT_PORT = 52222
CSI_BUFFER = 4096
Y_PHASE_RANGE_MIN = -1.5*np.pi
Y_PHASE_RANGE_MAX = 1.5*np.pi
Y_AMP_RANGE_MIN = 7.5
Y_AMP_RANGE_MAX = 27.5
KEY_GENERATOR_THREAD_ID = 4
NUM_SUBCARRIERS = 56                # either 56 or 114
SUBCARRIER_TO_PLOT = 30             # subcarrier to plot
TIME_FRAME = 100                    # length of x axis
RESOLUTION = 2                      # key quantization resolution in bits
KEY_LENGTH = 20                     # key length

###############################
# CREATE FIGURES FOR PLOTTING #
###############################
xAxis = np.arange(0, TIME_FRAME, 1)
yAxis = np.array([0] * TIME_FRAME)
error_line = np.array([0] * NUM_SUBCARRIERS)
fig1 = plt.figure(1)

fig2 = plt.figure(2)
fig3 = plt.figure(3)
fig1.suptitle("Antenna 1")
fig2.suptitle("Antenna 2")
fig3.suptitle("Antenna 3")

ax_p1 = fig1.add_subplot(211)
ax_a1 = fig1.add_subplot(212)

ax_p2 = fig2.add_subplot(211)
ax_a2 = fig2.add_subplot(212)

ax_p3 = fig3.add_subplot(211)
ax_a3 = fig3.add_subplot(212)

ax_p1.grid(True)
ax_p1.set_xlabel("Time")
ax_p1.set_ylabel("Phase [rad]")
ax_p1.axis([0, TIME_FRAME, Y_PHASE_RANGE_MIN, Y_PHASE_RANGE_MAX])
line1=ax_p1.plot(xAxis, yAxis, '-', color='#FF0000', label = 'rx1tx1_s')
line1_c=ax_p1.plot(xAxis, yAxis, '-', color='#050505', label = 'rx1tx1_c')
ax_p1.legend(loc=1)
ax_p1.set_xticks(xAxis, True)

ax_p2.grid(True)
ax_p2.set_xlabel("Time")
ax_p2.set_ylabel("Phase [rad]")
ax_p2.axis([0, TIME_FRAME, Y_PHASE_RANGE_MIN, Y_PHASE_RANGE_MAX])
line2=ax_p2.plot(xAxis, yAxis, '-', color='#ff5d00', label = 'rx2tx2_s')
line2_c=ax_p2.plot(xAxis, yAxis, '-', color='#69D2E7', label = 'rx2tx2_c')
ax_p2.legend(loc=1)
ax_p2.set_xticks(xAxis, True)

ax_p3.grid(True)
ax_p3.set_xlabel("Time")
ax_p3.set_ylabel("Phase [rad]")
ax_p3.axis([0, TIME_FRAME, Y_PHASE_RANGE_MIN, Y_PHASE_RANGE_MAX])
line3=ax_p3.plot(xAxis, yAxis, '-', color='#0000FF', label = 'rx3tx3_s')
line3_c=ax_p3.plot(xAxis, yAxis, '-', color='#FF00FF', label = 'rx3tx3_c')
ax_p3.legend(loc=1)
ax_p3.set_xticks(xAxis, True)

ax_a1.grid(True)
ax_a1.set_xlabel("Time")
ax_a1.set_ylabel("Magnitude [dB]")
ax_a1.axis([0, TIME_FRAME, Y_AMP_RANGE_MIN, Y_AMP_RANGE_MAX])
line4=ax_a1.plot(xAxis, yAxis, '-', color='#FF0000', label = 'rx1tx1_s')
line4_c=ax_a1.plot(xAxis, yAxis, '-', color='#050505', label = 'rx1tx1_c')
ax_a1.legend(loc=1)
ax_a1.set_xticks(xAxis, True)

ax_a2.grid(True)
ax_a2.set_xlabel("Time")
ax_a2.set_ylabel("Magnitude [dB]")
ax_a2.axis([0, TIME_FRAME, Y_AMP_RANGE_MIN, Y_AMP_RANGE_MAX])
line5=ax_a2.plot(xAxis, yAxis, '-', color='#00FF00', label = 'rx2tx2_s')
line5_c=ax_a2.plot(xAxis, yAxis, '-', color='#00FFFF', label = 'rx2tx2_c')
ax_a2.legend(loc=1)
ax_a2.set_xticks(xAxis, True)

ax_a3.grid(True)
ax_a3.set_xlabel("Time")
ax_a3.set_ylabel("Magnitude [dB]")
ax_a3.axis([0, TIME_FRAME, Y_AMP_RANGE_MIN, Y_AMP_RANGE_MAX])
line6=ax_a3.plot(xAxis, yAxis, '-', color='#0000FF', label = 'rx3tx3_s')
line6_c=ax_a3.plot(xAxis, yAxis, '-', color='#FF00FF', label = 'rx3tx3_c')
ax_a3.legend(loc=1)
ax_a3.set_xticks(xAxis, True)

clientQueue = queue.Queue()
serverQueue = queue.Queue()

antenna1 = queue.Queue()
antenna2 = queue.Queue()
antenna3 = queue.Queue()
            
class PacketProcessor(threading.Thread):
    def __init__(self, threadID, name, clientQ, serverQ, ant1Q, ant2Q, ant3Q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.cQ = clientQ
        self.sQ = serverQ
        self.ant1 = ant1Q
        self.ant2 = ant2Q
        self.ant3 = ant3Q
        
    def run(self):
        print('Starting ' + str(self.name))
        
        while 1:
            amp1_s, amp2_s, amp3_s, phase1_s, phase2_s, phase3_s = self.processPacket(self.sQ)
            amp1_c, amp2_c, amp3_c, phase1_c, phase2_c, phase3_c = self.processPacket(self.cQ)
            
            self.ant1.put(amp1_s)
            self.ant1.put(amp1_c)
            self.ant1.put(phase1_s)
            self.ant1.put(phase1_c)
            
            self.ant2.put(amp2_s)
            self.ant2.put(amp2_c)
            self.ant2.put(phase2_s)
            self.ant2.put(phase2_c)
            
            self.ant3.put(amp3_s)
            self.ant3.put(amp3_c)
            self.ant3.put(phase3_s)
            self.ant3.put(phase3_c)

    def processPacket(self, queue):
        # Process header
        item = queue.get()
        hdr = np.frombuffer(item, dtype=DTYPE_CSI_HDR)
        
        # Process CSI data
        item = queue.get()
        csi_data = np.frombuffer(item, dtype=DTYPE_CSI_COMPLEX)
        csi = csi_data['real'] + 1.j*csi_data['imag']
        rxtx1 = []
        rxtx2 = []
        rxtx3 = []
        rx = hdr[0][8]
        tx = hdr[0][9]
        num_tones = hdr[0][7]
        csi = csi.reshape(rx, tx, num_tones)
        
        for i in range(0, num_tones):
            rxtx1.append(csi[0][0][i])
            if rx >= 2 and tx >= 2:
                rxtx2.append(csi[1][1][i])
            if rx == 3 and tx == 3:
                rxtx3.append(csi[2][2][i])
        
        amplitude = [10*np.log10(np.absolute(x)) if x else 0 for x in rxtx1]
        phase = np.unwrap(np.angle(rxtx1))
        
        if rx >= 2 and tx >= 2:
            amplitude2 = [10*np.log10(np.absolute(x)) if x else 0 for x in rxtx2]
            phase2 = np.unwrap(np.angle(rxtx2))
        else:
            amplitude2 = error_line + 17.5
            phase2 = error_line
        
        if rx == 3 and tx == 3:
            amplitude3 = [10*np.log10(np.absolute(x)) if x else 0 for x in rxtx3]
            phase3 = np.unwrap(np.angle(rxtx3))
        else:
            amplitude3 = error_line + 17.5
            phase3 = error_line
        
        return amplitude, amplitude2, amplitude3, phase, phase2, phase3

class Plotter(threading.Thread):
    def __init__(self, threadID, name, queue, frameEvent, drawEvent, ps_line, as_line, pc_line, ac_line):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.queue = queue
        self.frameEvent = frameEvent
        self.drawEvent = drawEvent
        self.as_line = as_line
        self.ps_line = ps_line
        self.ac_line = ac_line
        self.pc_line = pc_line
        self.server_phase_deque = deque(yAxis, maxlen = TIME_FRAME)
        self.server_amp_deque = deque(yAxis, maxlen = TIME_FRAME)
        self.client_phase_deque = deque(yAxis, maxlen = TIME_FRAME)
        self.client_amp_deque = deque(yAxis, maxlen = TIME_FRAME)
        self.client_key = deque(np.arange(0, KEY_LENGTH, 1), maxlen = KEY_LENGTH)
        self.server_key = deque(np.arange(0, KEY_LENGTH, 1), maxlen = KEY_LENGTH)
        self.server_key_data = deque(np.arange(0, KEY_LENGTH, 1), maxlen = KEY_LENGTH)
        self.client_key_data = deque(np.arange(0, KEY_LENGTH, 1), maxlen = KEY_LENGTH)
        self.counter = 0
    
    def run(self):
        print('Starting ' + str(self.name))
        while 1:
            self.counter = (self.counter + 1) % (KEY_LENGTH + 1)
            
            amp_s = np.array(self.queue.get())
            amp_c = np.array(self.queue.get())
            phase_s = np.array(self.queue.get())
            phase_c = np.array(self.queue.get())
            
            #~ phase_avg_diff = np.sum(phase_s - phase_c) / NUM_SUBCARRIERS
            #~ phase_c_shifted = phase_c + phase_avg_diff
            #~ amp_avg_diff = np.sum(amp_s - amp_c) / NUM_SUBCARRIERS
            #~ amp_c_shifted = amp_c + amp_avg_diff
            #~ if self.threadID == KEY_GENERATOR_THREAD_ID:
                #~ print(str(self.name) + ' phase_avg_diff ' + str(phase_avg_diff))
                #~ print(str(self.name) + ' phase_c_shifted ' + str(phase_c_shifted))
                #~ print(str(self.name) + ' phase_c ' + str(phase_c))
            
            # update server phase and amplitude values
            self.server_phase_deque.append(phase_s[SUBCARRIER_TO_PLOT])
            self.server_amp_deque.append(amp_s[SUBCARRIER_TO_PLOT])
            self.server_key_data.append(phase_s[SUBCARRIER_TO_PLOT])
                
            # update client phase and amplitude values            
            self.client_phase_deque.append(phase_c[SUBCARRIER_TO_PLOT])                
            self.client_amp_deque.append(amp_c[SUBCARRIER_TO_PLOT])
            self.client_key_data.append(phase_c[SUBCARRIER_TO_PLOT])
            
            if self.threadID == KEY_GENERATOR_THREAD_ID:
                if self.counter == KEY_LENGTH:
                    self.quantize(self.server_key, self.server_key_data, RESOLUTION)
                    self.quantize(self.client_key, self.client_key_data, RESOLUTION)
                    #print(str(self.name) + ' server key: ' + str(self.server_key))
                    #print(str(self.name) + ' client key: ' + str(self.client_key))
                    if(self.server_key == self.client_key):
                        print(str(self.name) + ' found matching key: ' + str(self.server_key))
            
            # Set new data for server amplitude and phase
            self.ps_line[0].set_data(xAxis, self.server_phase_deque)
            self.as_line[0].set_data(xAxis, self.server_amp_deque)
            
            self.pc_line[0].set_data(xAxis, self.client_phase_deque)
            self.ac_line[0].set_data(xAxis, self.client_amp_deque)
            
            self.drawEvent.clear()
            self.frameEvent.set()
            self.drawEvent.wait()
    
    def quantize(self, key, key_signal, resolution):
        step_size = (abs(min(key_signal)) + abs(max(key_signal))) / (2**resolution)
        print(str(self.name) + ' step_size: ' + str(step_size))
        for i in range(len(key_signal)):
            key.append(round((key_signal[i] / step_size), 0))

# Create Events
frameEvent1 = threading.Event()
frameEvent2 = threading.Event()
frameEvent3 = threading.Event()
drawEvent = threading.Event()

# Create Threads
serverThread = CSIReceiver(1, "Server Thread", serverQueue, SERVER_PORT)
clientThread = CSIReceiver(2, "Client Thread", clientQueue, CLIENT_PORT)
processorThread = PacketProcessor(3, "Packet Processor", clientQueue, serverQueue, antenna1, antenna2, antenna3)
plotter1 = Plotter(KEY_GENERATOR_THREAD_ID, "Plotter 1", antenna1, frameEvent1, drawEvent, line1, line4, line1_c, line4_c)
plotter2 = Plotter(5, "Plotter 2", antenna2, frameEvent2, drawEvent, line2, line5, line2_c, line5_c)
plotter3 = Plotter(6, "Plotter 3", antenna3, frameEvent3, drawEvent, line3, line6, line3_c, line6_c)

# Start Threads
serverThread.start()
clientThread.start()
processorThread.start()
plotter1.start()
plotter2.start()
plotter3.start()

plt.show(block=False)

while 1:
    frameEvent1.wait()
    frameEvent2.wait()
    frameEvent3.wait()
    fig1.canvas.draw()
    fig2.canvas.draw()
    fig3.canvas.draw()
    frameEvent1.clear()
    frameEvent2.clear()
    frameEvent3.clear()
    drawEvent.set()
