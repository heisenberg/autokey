#!/usr/bin/env python3

from csi_types import DTYPE_CSI_HDR, DTYPE_C_TIMESTAMP, CSI_HEADER_SIZE
import socket
import numpy as np
import threading
import queue
from io import StringIO

DEBUG = 1
LOCAL_IP = '192.168.1.244'
SERVER_PORT = 51111
CLIENT_PORT = 52222
RX_ANTENNAS = 3
TX_ANTENNAS = 3
LOG_FILE_PATH = './quantization_match.log'

clientQueue = queue.Queue()
serverQueue = queue.Queue()

class Worker(threading.Thread):
    def __init__(self, threadID, name, queue, port):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.queue = queue
        self.port = port
    def run(self):
        print('Starting ' + str(self.name))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((LOCAL_IP, self.port))
        s.listen(1)
        conn, addr = s.accept()
        print('Connection address:', addr)
        
        while 1:
            # receive propagation delay timestamp
            msg = bytearray()
            bytes_left = 8
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
            ts = np.frombuffer(msg, dtype=DTYPE_C_TIMESTAMP)
            
            # receive header of 25 bytes
            msg = bytearray()
            bytes_left = CSI_HEADER_SIZE
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
            hdr = np.frombuffer(msg, dtype=DTYPE_CSI_HDR)
                    
            # receive quantization array
            msg = bytearray()
            num_tones = hdr[0][7]
            bytes_left = RX_ANTENNAS * TX_ANTENNAS * num_tones
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
    
            self.queue.put(ts)
            self.queue.put(hdr)
            self.queue.put(msg)
            
class Matcher(threading.Thread):
    def __init__(self, threadID, name, clientQ, serverQ):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.cQ = clientQ
        self.sQ = serverQ
        self.pkt_cnt = 0
    def run(self):
        print('Starting ' + str(self.name))
        
        fd = open(LOG_FILE_PATH, 'w')
        
        while 1:
            item = self.sQ.get()
            server_ts = np.frombuffer(item, dtype=DTYPE_C_TIMESTAMP)
            item = self.cQ.get()
            client_ts = np.frombuffer(item, dtype=DTYPE_C_TIMESTAMP)
            
            item = self.sQ.get()
            server_hdr = np.frombuffer(item, dtype=DTYPE_CSI_HDR)
            server_num_tones = server_hdr[0][7]
            
            item = self.cQ.get()
            client_hdr = np.frombuffer(item, dtype=DTYPE_CSI_HDR)
            client_num_tones = client_hdr[0][7]
            
            #~ rx1tx1_s, rx2tx2_s, rx3tx3_s = self.selectRxTx(self.sQ, server_num_tones)
            #~ rx1tx1_c, rx2tx2_c, rx3tx3_c = self.selectRxTx(self.cQ, client_num_tones)
            
            item = self.sQ.get()
            server_quantization_matrix = np.frombuffer(item, dtype=np.uint8)
            server_quantization_matrix = server_quantization_matrix.reshape(RX_ANTENNAS, TX_ANTENNAS, server_num_tones)
            
            item = self.cQ.get()
            client_quantization_matrix = np.frombuffer(item, dtype=np.uint8)
            client_quantization_matrix = client_quantization_matrix.reshape(RX_ANTENNAS, TX_ANTENNAS, client_num_tones)
            
            self.pkt_cnt += 1
            fd.write('Quantization for packet ' + str(self.pkt_cnt) + '\n')
            print('Quantization for packet ' + str(self.pkt_cnt))
            
            print('Client-Server propagation delay (sec, nsec): ' + str(server_ts))
            fd.write('Client-Server propagation delay (sec, nsec): ' + str(server_ts) + '\n')
            print('Server-Client propagation delay (sec, nsec): ' + str(client_ts))
            fd.write('Server-Client propagation delay (sec, nsec): ' + str(client_ts) + '\n')
            
            fd.write('Server header: ' + str(server_hdr) + '\n')
            print('Server header: ' + str(server_hdr))
            
            #~ fd.write('Server quantization:\n' + str(rx1tx1_s) + '\n' + str(rx2tx2_s) + '\n' + str(rx3tx3_s) + '\n')
            #~ print('Server quantization:\n' + str(rx1tx1_s) + '\n' + str(rx2tx2_s) + '\n' + str(rx3tx3_s))
            
            fd.write('Server quantization:\n' + str(server_quantization_matrix) + '\n')
            print('Server quantization:\n' + str(server_quantization_matrix))
            
            fd.write('Client header: ' + str(client_hdr) + '\n')
            print('Client header: ' + str(client_hdr))
            
            fd.write('Client quantization:\n' + str(client_quantization_matrix) + '\n')
            print('Client quantization:\n' + str(client_quantization_matrix))
            
            #~ fd.write('Client quantization:\n' + str(rx1tx1_c[0:server_num_tones-1]) + '\n' + str(rx2tx2_c[0:server_num_tones-1]) + '\n' + str(rx3tx3_c[0:server_num_tones-1]) + '\n')
            #~ print('Client quantization:\n' + str(rx1tx1_c[0:client_num_tones-1]) + '\n' + str(rx2tx2_c[0:client_num_tones-1]) + '\n' + str(rx3tx3_c[0:client_num_tones-1]))
            
            nr_rx = server_hdr[0][8]
            nr_tx = server_hdr[0][9]
            
            if(server_num_tones == client_num_tones and nr_rx == nr_tx):
                for tx_idx in range(0, nr_tx):
                    for rx_idx in range(0, nr_rx):
                        conflicts = np.bitwise_xor(server_quantization_matrix[rx_idx][tx_idx][:], client_quantization_matrix[rx_idx][tx_idx][:])
                        match_ratio = 1 - (sum(conflicts) / server_num_tones)
                        fd.write('Match ratio RX ' + str(rx_idx) + ' TX ' + str(tx_idx) + ': '  + str(match_ratio) + '\n')
                        print('Match ratio RX ' + str(rx_idx) + ' TX ' + str(tx_idx) + ': ' + str(match_ratio))
                print('\n')
                fd.write('\n')
                
                #~ a1_conflicts = np.bitwise_xor(rx1tx1_s, rx1tx1_c)
                #~ a1_match_ratio = 1 - (sum(a1_conflicts) / server_num_tones)
                #~ a2_conflicts = np.bitwise_xor(rx2tx2_s, rx2tx2_c)
                #~ a2_match_ratio = 1 - (sum(a2_conflicts) / server_num_tones)
                #~ a3_conflicts = np.bitwise_xor(rx3tx3_s, rx3tx3_c)
                #~ a3_match_ratio = 1 - (sum(a3_conflicts) / server_num_tones)
                #~ fd.write('Match ratio RX1-TX1: ' + str(a1_match_ratio) + '\n')
                #~ print('Match ratio RX1-TX1: ' + str(a1_match_ratio))
                #~ fd.write('Match ratio RX2-TX2: ' + str(a2_match_ratio) + '\n')
                #~ print('Match ratio RX2-TX2: ' + str(a2_match_ratio) )
                #~ fd.write('Match ratio RX3-TX3: ' + str(a3_match_ratio) + '\n\n')
                #~ print('Match ratio RX3-TX3: ' + str(a3_match_ratio) + '\n')
            else:
                print('Server and client CSI have a diffrent number of subcarriers or rx-tx antennas!')
                fd.write('Server and client CSI have a diffrent number of subcarriers or rx-tx antennas!\n\n')
            
    def selectRxTx(self, queue, num_tones):
        rx1tx1 = []
        rx2tx2 = []
        rx3tx3 = []
        
        item = queue.get()
        quantization_matrix = np.frombuffer(item, dtype=np.uint8)
        quantization_matrix = quantization_matrix.reshape(RX_ANTENNAS, TX_ANTENNAS, num_tones)
        
        for i in range(0, num_tones):
            rx1tx1.append(quantization_matrix[0][0][i])
            rx2tx2.append(quantization_matrix[1][1][i])
            rx3tx3.append(quantization_matrix[2][2][i])       
        
        return rx1tx1, rx2tx2, rx3tx3

# Create Threads
serverThread = Worker(1, "Server Thread", serverQueue, SERVER_PORT)
clientThread = Worker(2, "Client Thread", clientQueue, CLIENT_PORT)
matcherThread = Matcher(3, "Matcher Thread", clientQueue, serverQueue)

# Start Threads
serverThread.start()
clientThread.start()
matcherThread.start()
