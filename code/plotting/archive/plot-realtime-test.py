#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import random as rand

xAchse=np.arange(0,100,1)
yAchse=np.array([0]*100)

fig = plt.figure(1)
ax = fig.add_subplot(111)
ax.grid(True)
ax.set_title("Realtime Waveform Plot")
ax.set_xlabel("Time")
ax.set_ylabel("Amplitude")
ax.axis([0,100,-1.5,1.5])
line1=ax.plot(xAchse,yAchse,'-')

manager = plt.get_current_fig_manager()

values=[]
values = [0 for x in range(100)]

Ta=0.01
fa=1.0/Ta
fcos=3.5

Konstant=np.cos(2*np.pi*fcos*Ta)
T0=1.0
T1=Konstant

def SinwaveformGenerator(arg):
    global values,T1,Konstant,T0
    #ohmegaCos=arccos(T1)/Ta
    #print "fcos=", ohmegaCos/(2*pi), "Hz"

    Tnext=((Konstant*T1)*2)-T0
    if len(values)%100>70:
        values.append(rand.random()*2-1)
    else:
        values.append(Tnext)
    T0=T1
    T1=Tnext

def RealtimePloter(arg):
    global values
    CurrentXAxis=np.arange(len(values)-100,len(values),1)
    line1[0].set_data(CurrentXAxis,np.array(values[-100:]))
    ax.axis([CurrentXAxis.min(),CurrentXAxis.max(),-1.5,1.5])
    manager.canvas.draw()
    #manager.show()

timer = fig.canvas.new_timer(interval=20)
timer.add_callback(RealtimePloter, ())
timer2 = fig.canvas.new_timer(interval=20)
timer2.add_callback(SinwaveformGenerator, ())
timer.start()
timer2.start()

plt.show()
