#!/usr/bin/env python3

import socket
import numpy as np
import threading
import queue
from io import StringIO

DEBUG = 1
LOCAL_IP = '192.168.1.244'
SERVER_PORT = 51111
CLIENT_PORT = 52222
HEADER_SIZE = 25
CSI_BUFFER = 4096
NUM_SUBCARRIERS = 56

# CSI packet header type for numpy
DTYPE_CSI_HDR = np.dtype([
    ("tstamp", np.uint64),
    ("csi_len", np.uint16),
    ("channel", np.uint16),
    ("phyerr", np.uint8),
    ("noise", np.uint8),
    ("rate", np.uint8),
    ("chanbw", np.uint8),
    ("num_tones", np.uint8),
    ("nr", np.uint8),
    ("nc", np.uint8),
    ("rssi", np.uint8),
    ("rssi_0", np.uint8),
    ("rssi_1", np.uint8),
    ("rssi_2", np.uint8),
    ("pld_len", np.uint16)
])

DTYPE_CSI_HDR = DTYPE_CSI_HDR.newbyteorder('>')

DTYPE_CSI_COMPLEX = np.dtype([
    ("real", np.int32),
    ("imag", np.int32)
])

DTYPE_CSI_COMPLEX = DTYPE_CSI_COMPLEX.newbyteorder('>')

error_signal = np.array([0]*NUM_SUBCARRIERS)

clientQueue = queue.Queue()
serverQueue = queue.Queue()
antenna1 = queue.Queue()
antenna2 = queue.Queue()
antenna3 = queue.Queue()

class Worker(threading.Thread):
    def __init__(self, threadID, name, queue, port):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.queue = queue
        self.port = port
    def run(self):
        print('Starting ' + str(self.name))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((LOCAL_IP, self.port))
        s.listen(1)
        conn, addr = s.accept()
        print('Connection address:', addr)
        
        while 1:
            # First receive header of 25 bytes
            msg = bytearray()
            bytes_left = HEADER_SIZE
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
        
            hdr = np.frombuffer(msg, dtype=DTYPE_CSI_HDR)
        
            # Receive CSI data
            msg = bytearray()
            bytes_left = hdr[0][1]
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)

            self.queue.put(hdr)
            self.queue.put(msg)
            
class PacketProcessor(threading.Thread):
    def __init__(self, threadID, name, clientQ, serverQ, ant1Q, ant2Q, ant3Q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.cQ = clientQ
        self.sQ = serverQ
        self.ant1 = ant1Q
        self.ant2 = ant2Q
        self.ant3 = ant3Q
    def run(self):
        print('Starting ' + str(self.name))
        
        while 1:
            phase1_s, phase2_s, phase3_s = self.processPacket(self.sQ)
            phase1_c, phase2_c, phase3_c = self.processPacket(self.cQ)
            
            self.ant1.put(phase1_s)
            self.ant1.put(phase1_c)
            self.ant2.put(phase2_s)
            self.ant2.put(phase2_c)
            self.ant3.put(phase3_s)
            self.ant3.put(phase3_c)

    def processPacket(self, queue):
        # Process header
        item = queue.get()
        hdr = np.frombuffer(item, dtype=DTYPE_CSI_HDR)
        
        # Process CSI data
        item = queue.get()
        csi_data = np.frombuffer(item, dtype=DTYPE_CSI_COMPLEX)
        csi = csi_data['real'] + 1.j*csi_data['imag']
        rxtx1 = []
        rxtx2 = []
        rxtx3 = []
        rx = hdr[0][8]
        tx = hdr[0][9]
        num_tones = hdr[0][7]
        
        # csi = csi.reshape(rx,tx,num_tones)
        item_nr = 0
        csi_ = np.zeros(shape=(rx, tx, num_tones), dtype=complex)
        for r in range(rx):
            for t in range(tx):
                for n in range(num_tones):
                    csi_[r][t][n] = csi[item_nr]
                    item_nr += 1
        
        for i in range(0, num_tones):
            rxtx1.append(csi_[0][0][i])
            if rx >= 2 and tx >= 2:
                rxtx2.append(csi_[1][1][i])
            if rx == 3 and tx == 3:
                rxtx3.append(csi_[2][2][i])
        
        phase1 = np.unwrap(np.angle(rxtx1))
        
        if rx >= 2 and tx >= 2:
            phase2 = np.unwrap(np.angle(rxtx2))
        else:
            phase2 = error_signal
        
        if rx == 3 and tx == 3:
            phase3 = np.unwrap(np.angle(rxtx3))
        else:
            phase3 = error_signal
        
        return phase1, phase2, phase3

class Quantizer(threading.Thread):
    def __init__(self, threadID, name, antenna1, antenna2, antenna3):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.queue = queue
        self.antenna1 = antenna1
        self.antenna2 = antenna2
        self.antenna3 = antenna3
        self.match_sum = 0
        self.measures_taken = 0
    
    def run(self):
        print('Starting ' + str(self.name))
        while 1:
            #self.measures_taken += 3
            self.measures_taken += 1
            
            phase_a1_s = self.antenna1.get()
            phase_a1_c = self.antenna1.get()
            phase_a2_s = self.antenna2.get()
            phase_a2_c = self.antenna2.get()
            phase_a3_s = self.antenna3.get()
            phase_a3_c = self.antenna3.get()
            
            qphase_a1_s, qphase_a1_c = self.quantize(phase_a1_s, phase_a1_c)
            qphase_a2_s, qphase_a2_c = self.quantize(phase_a2_s, phase_a2_c)
            qphase_a3_s, qphase_a3_c = self.quantize(phase_a3_s, phase_a3_c)
            
            a1_conflicts = np.bitwise_xor(qphase_a1_s, qphase_a1_c)
            a1_match_ratio = 1 - (sum(a1_conflicts) / NUM_SUBCARRIERS)
            a2_conflicts = np.bitwise_xor(qphase_a2_s, qphase_a2_c)
            a2_match_ratio = 1 - (sum(a2_conflicts) / NUM_SUBCARRIERS)
            a3_conflicts = np.bitwise_xor(qphase_a3_s, qphase_a3_c)
            a3_match_ratio = 1 - (sum(a3_conflicts) / NUM_SUBCARRIERS)                  
            
            #a1a2a3 = a1_match_ratio + a2_match_ratio + a3_match_ratio
            #self.match_sum += a1a2a3
            
            #if a1a2a3 == 3:
            #    self.match_sum += 1
            #    print('Match #' + str(self.match_sum) + ' after ' + str(self.measures_taken) + ' measures.')
            #    print('Antenna 1 : ' + str(phase_a1_s) + '\n' + str(phase_a1_c))
            #    print('Antenna 2 : ' + str(phase_a2_s) + '\n' + str(phase_a2_c))
            #    print('Antenna 3 : ' + str(phase_a3_s) + '\n' + str(phase_a3_c) + '\n\n')
            
            #print('A 1: ' + str(a1_match_ratio))
            #print('A 2: ' + str(a2_match_ratio))
            #print('A 3: ' + str(a3_match_ratio))
            #print('Avg A1,A2,A3: ' + str((a1a2a3) / 3))
            #print('Total: ' + str(self.match_sum / self.measures_taken) + '\n')
            
            #print('Server Phase 1: ' + str(qphase_a1_s) + '\n')
            #print('Client Phase 1: ' + str(qphase_a1_c) + '\n')
            #print('Conflicts P 1:' + str(a1_conflicts) + '\n')
            print('Match Ratio P1: ' + str(a1_match_ratio) + '\n')
            print('Match Ratio P1: ' + str(a2_match_ratio) + '\n')
            print('Match Ratio P1: ' + str(a3_match_ratio) + '\n\n')
            
            print('Antenna 1 : ' + str(qphase_a1_s) + '\n')
            print('Antenna 2 : ' + str(qphase_a2_s) + '\n')
            print('Antenna 3 : ' + str(qphase_a3_s) + '\n\n')
            
            #if a1_match_ratio == 0:
            #    print('No match for: ' + str(phase_a1_s) + '\n' + str(phase_a1_c) + '\n')
            #    print('Difference s - c: ' + str(phase_a1_s - phase_a1_c) + '\n\n')
                
            #print('Antenna 2 : ' + str(qphase_a2_s) + '\n' + str(qphase_a2_c))
            #print('Antenna 3 : ' + str(qphase_a3_s) + '\n' + str(qphase_a3_c) + '\n\n')
            
            
    def quantize(self, signal_1, signal_2):
        qsignal_1 = np.array([0]*NUM_SUBCARRIERS)
        qsignal_2 = np.array([0]*NUM_SUBCARRIERS)
        
        for i in range(len(signal_1)):
            if signal_1[i] > 0:
                qsignal_1[i] = 1
            else: 
                qsignal_1[i] = 0
                
        for i in range(len(signal_2)):
            if signal_2[i] > 0:
                qsignal_2[i] = 1
            else: 
                qsignal_2[i] = 0
                
        return qsignal_1, qsignal_2
                    

# Create Threads
serverThread = Worker(1, "Server Thread", serverQueue, SERVER_PORT)
clientThread = Worker(2, "Client Thread", clientQueue, CLIENT_PORT)
processorThread = PacketProcessor(3, "Packet Processor", clientQueue, serverQueue, antenna1, antenna2, antenna3)
quantizer = Quantizer(4, "Quantizer", antenna1, antenna2, antenna3)

# Start Threads
serverThread.start()
clientThread.start()
processorThread.start()
quantizer.start()
