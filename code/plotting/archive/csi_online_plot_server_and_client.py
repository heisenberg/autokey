#!/usr/bin/env python3

import socket
import numpy as np
import matplotlib.pyplot as plt
import textwrap as tw
import threading
import queue

#from struct import *

DEBUG = 1
LOCAL_IP = '192.168.1.244'
SERVER_PORT = 51111
CLIENT_PORT = 52222
HEADER_SIZE = 25
CSI_BUFFER = 4096
PLOT_INTERVAL = 100
Y_RANGE_MIN = -3*np.pi
Y_RANGE_MAX = 3*np.pi

# CSI packet header type for numpy
DTYPE_CSI_HDR = np.dtype([
    ("tstamp", np.uint64),
    ("csi_len", np.uint16),
    ("channel", np.uint16),
    ("phyerr", np.uint8),
    ("noise", np.uint8),
    ("rate", np.uint8),
    ("chanbw", np.uint8),
    ("num_tones", np.uint8),
    ("nr", np.uint8),
    ("nc", np.uint8),
    ("rssi", np.uint8),
    ("rssi_0", np.uint8),
    ("rssi_1", np.uint8),
    ("rssi_2", np.uint8),
    ("pld_len", np.uint16)
])

DTYPE_CSI_HDR = DTYPE_CSI_HDR.newbyteorder('>')

DTYPE_CSI_COMPLEX = np.dtype([
    ("real", np.int32),
    ("imag", np.int32)
])

DTYPE_CSI_COMPLEX = DTYPE_CSI_COMPLEX.newbyteorder('>')

##############################
# CREATE FIGURE FOR PLOTTING #
##############################
xAchse=np.arange(0,56,1)
yAchse=np.array([0]*56)
fig = plt.figure(1)
ax1 = fig.add_subplot(321)
ax2 = fig.add_subplot(323)
ax3 = fig.add_subplot(325)
ax4 = fig.add_subplot(322)
ax5 = fig.add_subplot(324)
ax6 = fig.add_subplot(326)

ax1.grid(True)
ax1.set_title("CSI real time plot")
ax1.set_xlabel("Index")
ax1.set_ylabel("Phase [rad]")
ax1.axis([0,56,Y_RANGE_MIN,Y_RANGE_MAX])
line1=ax1.plot(xAchse,yAchse,'-',color='#FF0000', label = 'rx1tx1_s')
line1_c=ax1.plot(xAchse,yAchse+1.5,'-',color='#FFFF00', label = 'rx1tx1_c')
ax1.legend(loc=1)

ax2.grid(True)
ax2.set_xlabel("Index")
ax2.set_ylabel("Phase [rad]")
ax2.axis([0,56,Y_RANGE_MIN,Y_RANGE_MAX])
line2=ax2.plot(xAchse,yAchse+0.5,'-',color='#00FF00', label = 'rx2tx2_s')
line2_c=ax2.plot(xAchse,yAchse+2,'-',color='#00FFFF', label = 'rx2tx2_c')
ax2.legend(loc=1)

ax3.grid(True)
ax3.set_xlabel("Index")
ax3.set_ylabel("Phase [rad]")
ax3.axis([0,56,Y_RANGE_MIN,Y_RANGE_MAX])
line3=ax3.plot(xAchse,yAchse+1,'-',color='#0000FF', label = 'rx3tx3_s')
line3_c=ax3.plot(xAchse,yAchse+2.5,'-',color='#FF00FF', label = 'rx3tx3_c')
ax3.legend(loc=1)

ax4.grid(True)
ax4.set_xlabel("Index")
ax4.set_ylabel("Magnitude [dB]")
ax4.axis([0,56,-35,35])
line4=ax4.plot(xAchse,yAchse,'-',color='#FF0000', label = 'rx1tx1_s')
line4_c=ax4.plot(xAchse,yAchse+1.5,'-',color='#FFFF00', label = 'rx1tx1_c')
ax4.legend(loc=1)

ax5.grid(True)
ax5.set_xlabel("Index")
ax5.set_ylabel("Magnitude [dB]")
ax5.axis([0,56,-35,35])
line5=ax5.plot(xAchse,yAchse+0.5,'-',color='#00FF00', label = 'rx2tx2_s')
line5_c=ax5.plot(xAchse,yAchse+2,'-',color='#00FFFF', label = 'rx2tx2_c')
ax5.legend(loc=1)

ax6.grid(True)
ax6.set_xlabel("Index")
ax6.set_ylabel("Magnitude [dB]")
ax6.axis([0,56,-35,35])
line6=ax6.plot(xAchse,yAchse+1,'-',color='#0000FF', label = 'rx3tx3_s')
line6_c=ax6.plot(xAchse,yAchse+2.5,'-',color='#FF00FF', label = 'rx3tx3_c')
ax6.legend(loc=1)

pkt_ctr = 0
plot_annotations = "CSI packets received: " + str(pkt_ctr) + " Header data: "
figtxt = plt.figtext(0.5, 0.0, plot_annotations, wrap=True, horizontalalignment='center', fontsize=8)

manager = plt.get_current_fig_manager()

clientQueue = queue.Queue()
serverQueue = queue.Queue()

class Worker(threading.Thread):
    def __init__(self, threadID, name, queue, port):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.queue = queue
        self.port = port
    def run(self):
        print('Starting' + str(self.name))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((LOCAL_IP, self.port))
        s.listen(1)
        conn, addr = s.accept()
        if DEBUG:
            print('Connection address:', addr)
        
        while 1:
            # First receive header of 25 bytes
            msg = bytearray()
            bytes_left = HEADER_SIZE
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
        
            hdr = np.frombuffer(msg, dtype=DTYPE_CSI_HDR)
        
            # Receive CSI data
            msg = bytearray()
            bytes_left = hdr[0][1]
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)

            self.queue.put(hdr)
            self.queue.put(msg)

def CsiDataCallback(arg):
    global pkt_ctr, clientQueue, serverQueue
    
    #####################
    #PROCESS SERVER CSI #
    #####################
    #TODO: WRITE FUNCTION FOR THAT...
    # Process header
    item = serverQueue.get()
    hdr_server = np.frombuffer(item, dtype=DTYPE_CSI_HDR)
    #~ if DEBUG:
        #~ print(hdr_server)
        
    pkt_ctr += 1
    plot_annotations = "CSI packets received: " + str(pkt_ctr) + "Server header data: " + str(hdr_server)
    figtxt.set_text(plot_annotations)
    # Process CSI data
    item = serverQueue.get()
    csi_server_data = np.frombuffer(item, dtype=DTYPE_CSI_COMPLEX)
    csi_server = csi_server_data['real'] + 1.j*csi_server_data['imag']
    rxtx1 = []
    rxtx2 = []
    rxtx3 = []
    rx = hdr_server[0][8]
    tx = hdr_server[0][9]
    num_tones = hdr_server[0][7]
    csi_server = csi_server.reshape(rx,tx,num_tones)
    #~ if DEBUG:
        #~ print(csi)
    for i in range(0, num_tones):
        rxtx1.append(csi_server[0][0][i])
        rxtx2.append(csi_server[1][1][i])
        if rx == 3 and tx == 3:
            rxtx3.append(csi_server[2][2][i])
        
    amplitude = 10*np.log10(np.absolute(rxtx1))
    amplitude2 = 10*np.log10(np.absolute(rxtx2))
    phase = np.unwrap(np.angle(rxtx1))
    phase2 = np.unwrap(np.angle(rxtx2))
    if rx == 3 and tx == 3:
        phase3 = np.unwrap(np.angle(rxtx3))
        amplitude3 = 10*np.log10(np.absolute(rxtx3))
    
    #ax.axis([0,56,amp.min(), amp.max()])
    line1[0].set_data(xAchse, phase)
    line4[0].set_data(xAchse, amplitude)
    line2[0].set_data(xAchse, phase2)
    line5[0].set_data(xAchse, amplitude2)
    if rx == 3 and tx == 3:
        line3[0].set_data(xAchse, phase3)
        line6[0].set_data(xAchse, amplitude3)
    else:
        line3[0].set_data(xAchse, yAchse+2)
        line6[0].set_data(xAchse, yAchse)
        
    #####################
    #PROCESS CLIENT CSI #
    #####################
    # Process header
    item = clientQueue.get()
    hdr_client = np.frombuffer(item, dtype=DTYPE_CSI_HDR)
    if DEBUG:
        print(hdr_client)
        
    # Process CSI data
    item = clientQueue.get()
    csi_client_data = np.frombuffer(item, dtype=DTYPE_CSI_COMPLEX)
    csi_client = csi_client_data['real'] + 1.j*csi_client_data['imag']
    rxtx1_c = []
    rxtx2_c = []
    rxtx3_c = []
    rx_c = hdr_client[0][8]
    tx_c = hdr_client[0][9]
    num_tones_c = hdr_client[0][7]
    csi_client = csi_client.reshape(rx_c,tx_c,num_tones_c)
    #~ if DEBUG:
        #~ print(csi)
    for i in range(0, num_tones):
        rxtx1_c.append(csi_client[0][0][i])
        rxtx2_c.append(csi_client[1][1][i])
        if rx_c == 3 and tx_c == 3:
            rxtx3_c.append(csi_client[2][2][i])
    
    amplitude_c = 10*np.log10(np.absolute(rxtx1_c))
    amplitude2_c = 10*np.log10(np.absolute(rxtx2_c))
    phase_c = np.unwrap(np.angle(rxtx1_c))
    phase2_c = np.unwrap(np.angle(rxtx2_c))
    if rx_c == 3 and tx_c == 3:
        phase3_c = np.unwrap(np.angle(rxtx3_c))
        amplitude3_c = 10*np.log10(np.absolute(rxtx3_c))
    
    line1_c[0].set_data(xAchse, phase_c)
    line4_c[0].set_data(xAchse, amplitude_c)
    line2_c[0].set_data(xAchse, phase2_c)
    line5_c[0].set_data(xAchse, amplitude2_c)
    if rx_c == 3 and tx_c == 3:
        line3_c[0].set_data(xAchse, phase3_c)
        line6_c[0].set_data(xAchse, amplitude3_c)
    else:
        line3_c[0].set_data(xAchse, yAchse+2)
        line6_c[0].set_data(xAchse, yAchse)
    
    manager.canvas.draw()

# Create Threads
serverThread = Worker(1, "Server Thread", serverQueue, SERVER_PORT)
clinetThread = Worker(2, "Client Thread", clientQueue, CLIENT_PORT)
 
# Start Threads
serverThread.start()
clinetThread.start()

timer = fig.canvas.new_timer(PLOT_INTERVAL)
timer.add_callback(CsiDataCallback, ())
timer.start()

plt.show()
