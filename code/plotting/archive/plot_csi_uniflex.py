import logging
import datetime
import datetime
import numpy as np

from uniflex.core import modules
from uniflex.core import events

import matplotlib.pyplot as plt

#number of CSI pairs (Erich, Miranda) to plot, is controlled by csi_num. csi_index SHOULD not be changed from 0!
csi_num = 2
csi_index = 0

# Rows are rx indices, columns are tx indices.
# A value of False in the matrix will say that that specific rx and tx antenna pair is not shown.
# The number of rows and columns should match the number of received and transmission antennas,
#respectively.
erich_indices = [[True, False, False],
                [False, False, False],
                [False, False, False]]
miranda_indices = [[True, False, False],
                [False, False, False],
                [False, False, False]]
erich_indices = np.array(erich_indices)
miranda_indices = np.array(miranda_indices)

def find_rx_tx(indices):
    rx = 0
    for r in range(0, indices.shape[0]):
        temp_rx = 0
        for t in range(0, indices.shape[1]):
            if indices[r,t]:
                temp_rx += 1
        if temp_rx > rx:
            rx = temp_rx
    tx = 0
    for t in range(0, indices.shape[1]):
        temp_tx = 0
        for r in range(0, indices.shape[0]):
            if indices[r,t]:
                temp_tx += 1
        if temp_tx > tx:
            tx = temp_tx
    return temp_tx, temp_rx

e_rx, e_tx = find_rx_tx(erich_indices)
m_rx, m_tx = find_rx_tx(miranda_indices)

'''
	Global controller performs channel sounding in 802.11 network using Atheros WiFi chipsets supporting
	CSI.
'''

class ChannelSounderWiFiController(modules.ControlApplication):
    def __init__(self, num_nodes):
        super(ChannelSounderWiFiController, self).__init__()
        self.log = logging.getLogger('ChannelSounderWiFiController')
        self.log.info("ChannelSounderWiFiController")
        self.nodes = {}  # APs UUID -> node
        self.num_nodes = num_nodes
        #setup plot
        self.first_erich = True
        self.first_miranda = True
        self.ydata_erich = None
        self.ydata_miranda = None
        self.phase_erich = None
        self.phase_miranda = None
        self.colors = ['#FF0000', '#0000FF','#800000', '#FFFF00', '#808000', '#00FF00', '#008000',
            '#00FFFF', '#008080', '#000080', '#FF00FF', '#800080', '#000000']
        self.show_legend = True
        print('Finished initialisation')

    @modules.on_start()
    def my_start_function(self):
        self.log.info("start control app")

        self.ifaceName = 'wlan0'
        self.start = None
        #self.hopping_interval = 3

        # CSI stuff
        self.results = []
        self.samples = 1


    @modules.on_exit()
    def my_stop_function(self):
        print("stop control app")

    @modules.on_event(events.NewNodeEvent)
    def add_node(self, event):
        node = event.node

        self.log.info("Added new node: {}, Local: {}"
                      .format(node.uuid, node.local))
        self.nodes[node.uuid] = node

        devs = node.get_devices()
        for dev in devs:
            self.log.info("Dev: %s" % str(dev.name))
            ifaces = dev.get_interfaces()
            self.log.info('Ifaces %s' % ifaces)


        if len(self.nodes) == self.num_nodes:
            self.schedule_fetch_csi()


    @modules.on_event(events.NodeExitEvent)
    @modules.on_event(events.NodeLostEvent)
    def remove_node(self, event):
        self.log.info("Node lost".format())
        node = event.node
        reason = event.reason
        if node in self.nodes:
            del self.nodes[node.uuid]
            self.log.info("Node: {}, Local: {} removed reason: {}"
                          .format(node.uuid, node.local, reason))


    def schedule_fetch_csi(self):
        try:
            self.log.info('First schedule_fetch_csi')

            for node in self.nodes.values():
                device = node.get_device(0)
                device.callback(self.channel_csi_cb).get_csi(self.samples, False)

        except Exception as e:
            self.log.error("{} !!!Exception!!!: {}".format(
                datetime.datetime.now(), e))

    def get_power(self, m):
        s = m.shape
        #print('CSI shape', s)
        rx_pwr = np.zeros((s[0], s[1], s[2]))
        rx_phase = np.zeros((s[0], s[1], s[2]))
        for r in range(0, s[0]):
            for t in range(0, s[1]):
                rx_sig = m[r, t, :]
                rx_pwr[r,t] = 10*np.log10(np.absolute(rx_sig))
                rx_phase[r,t] = np.angle(rx_sig)
        return rx_pwr, rx_phase

    def channel_csi_cb(self, data):
        """
        Callback function called when CSI results are available
        """
        global erich_indices
        global miranda_indices
        global e_rx, e_tx, m_rx, m_tx
        global csi_num, csi_index

        node = data.node
        devName = None
        if data.device:
            devName = data.device.name
        csi = data.msg

        print("Default Callback: "
              "Node: {}, Dev: {}, Data: {}"
              .format(node.hostname, devName, csi.shape))
        host = node.hostname.lower()

        csi_0 = csi[0].view(np.recarray)

        #print(csi_0.header)
        #print(csi_0.csi_matrix)

        #self.results.append(csi_0)

        #plot data (new data will replace the old one)
        y, p = self.get_power(csi_0.csi_matrix)

        if 'erich' == host:
            if self.first_erich:
                self.ydata_erich = y
                self.phase_erich = p
                self.first_erich = False
            else:
                self.ydata_erich = np.concatenate((self.ydata_erich, y), axis=2)
                self.phase_erich = np.concatenate((self.phase_erich, p), axis=2)
        elif 'miranda' == host:
            if self.first_miranda:
                self.ydata_miranda = y
                self.phase_miranda = p
                self.first_miranda = False
            else:
                self.ydata_miranda = np.concatenate((self.ydata_miranda, y), axis=2)
                self.phase_miranda = np.concatenate((self.phase_miranda, p), axis=2)
            x_miranda = np.arange(self.ydata_miranda.shape[2])
            x_erich = np.arange(self.ydata_erich.shape[2])
            plt.figure(0)
            i = 0
            plt.subplot(2, 1, 1)
            for r in range(0, self.ydata_miranda.shape[0]):
                for t in range(0, self.ydata_erich.shape[1]):
                    if erich_indices[r,t]:
                        plt.plot(x_erich, self.ydata_erich[r,t,:], label='erich tx'+str(t)+', rx'+str(r),
                            color=self.colors[i])
                        i = (i+1)%len(self.colors)
                for t in range(0, self.ydata_miranda.shape[1]):
                    if miranda_indices[r,t]:
                        plt.plot(x_miranda, self.ydata_miranda[r,t,:], label='miranda tx'+str(t)+', rx'+str(r),
                            color=self.colors[i])
                        i = (i+1)%len(self.colors)
            if self.show_legend:
                plt.xlabel('Index')
                plt.ylabel('Magnitude [dB]')
                plt.legend()
            i = 0
            plt.subplot(2, 1, 2)
            for r in range(0, self.ydata_miranda.shape[0]):
                for t in range(0, self.phase_erich.shape[1]):
                    if erich_indices[r,t]:
                        plt.plot(x_erich, self.phase_erich[r,t,:], label='erich tx'+str(t)+', rx'+str(r),
                            color=self.colors[i])
                        i = (i+1)%len(self.colors)
                for t in range(0, self.phase_miranda.shape[1]):
                    if miranda_indices[r,t]:
                        plt.plot(x_miranda, self.phase_miranda[r,t,:], label='miranda tx'+str(t)+', rx'+str(r),
                            color=self.colors[i])
                        i = (i+1)%len(self.colors)
            if self.show_legend:
                plt.xlabel('Index')
                plt.ylabel('Phase [rad]')
                plt.legend()
                self.show_legend = False
            plt.savefig('e_rx'+str(e_rx)+'_tx'+str(e_tx)+'_m_rx'+str(m_rx)+'_tx'+str(m_tx)+'.eps')

        # schedule callback for next CSI value
        if csi_index < csi_num:
            data.device.callback(self.channel_csi_cb).get_csi(self.samples, False)
            csi_index += 1
