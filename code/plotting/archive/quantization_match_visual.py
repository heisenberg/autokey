#!/usr/bin/env python3

import socket
import numpy as np
import threading
import queue
from io import StringIO
import matplotlib.pyplot as plt

DEBUG = 1
LOCAL_IP = '192.168.1.244'
SERVER_PORT = 51111
CLIENT_PORT = 52222
RX_ANTENNAS = 3
TX_ANTENNAS = 3
HEADER_SIZE = 25
BAR_WIDTH = 0.25

DTYPE_C_TIMESTAMP = np.dtype([
    ("sec", np.int32),
    ("nsec", np.int32)
])
DTYPE_C_TIMESTAMP = DTYPE_C_TIMESTAMP.newbyteorder('>')

DTYPE_CSI_HDR = np.dtype([
    ("tstamp", np.uint64),
    ("csi_len", np.uint16),
    ("channel", np.uint16),
    ("phyerr", np.uint8),
    ("noise", np.uint8),
    ("rate", np.uint8),
    ("chanbw", np.uint8),
    ("num_tones", np.uint8),
    ("nr", np.uint8),
    ("nc", np.uint8),
    ("rssi", np.uint8),
    ("rssi_0", np.uint8),
    ("rssi_1", np.uint8),
    ("rssi_2", np.uint8),
    ("pld_len", np.uint16)
])
DTYPE_CSI_HDR = DTYPE_CSI_HDR.newbyteorder('>')

LOG_FILE_PATH = './quantization_match.log'

###############################
# CREATE FIGURES FOR PLOTTING #
###############################
phase_match_rates = np.arange(0.1, 1.0, 0.1)
fig = plt.figure()
phase_bars = plt.bar(np.arange(len(phase_match_rates)) - 0.15, phase_match_rates, width=BAR_WIDTH, label = '$\phi$')
amplitude_bars = plt.bar(np.arange(len(phase_match_rates)) + 0.15, phase_match_rates, width=BAR_WIDTH, label = 'A')
plt.xticks(np.arange(9), ('1-1', '2-1', '3-1', '1-2', '2-2', '3-2', '1-3', '2-3', '3-3'))
plt.yticks(np.arange(0, 1.05, 0.05))
plt.xlabel('Antenna pair [RX TX]')
plt.ylabel('Bit agreement rate [%]')
plt.legend(loc=9, bbox_to_anchor=(0.86, 1.11), ncol=2)
plt.grid()

clientQueue = queue.Queue()
serverQueue = queue.Queue()

newDataEvent = threading.Event()
drawEvent = threading.Event()

class Worker(threading.Thread):
    def __init__(self, threadID, name, queue, port):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.queue = queue
        self.port = port
    def run(self):
        print('Starting ' + str(self.name))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((LOCAL_IP, self.port))
        s.listen(1)
        conn, addr = s.accept()
        print('Connection address:', addr)
        
        while 1:
            #TODO: function for reception loop
            # receive propagation delay timestamp
            msg = bytearray()
            bytes_left = 8
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
            ts = np.frombuffer(msg, dtype=DTYPE_C_TIMESTAMP)
            
            # receive header of 25 bytes
            msg = bytearray()
            bytes_left = HEADER_SIZE
            while bytes_left:
                data = conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
            hdr = np.frombuffer(msg, dtype=DTYPE_CSI_HDR)
                    
            # receive quantization array for phase
            phase = bytearray()
            num_tones = hdr[0][7]
            bytes_left = RX_ANTENNAS * TX_ANTENNAS * num_tones
            while bytes_left:
                data = conn.recv(bytes_left)
                phase.extend(data)
                bytes_left -= len(data)
                
            # receive quantization array for amplitude
            amplitude = bytearray()
            bytes_left = RX_ANTENNAS * TX_ANTENNAS * num_tones
            while bytes_left:
                data = conn.recv(bytes_left)
                amplitude.extend(data)
                bytes_left -= len(data)
    
            self.queue.put(ts)
            self.queue.put(hdr)
            self.queue.put(phase)
            self.queue.put(amplitude)
            
class Matcher(threading.Thread):
    def __init__(self, threadID, name, clientQ, serverQ):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.cQ = clientQ
        self.sQ = serverQ
        self.pkt_cnt = 0
    def run(self):
        print('Starting ' + str(self.name))
        
        fd = open(LOG_FILE_PATH, 'w')
        
        while 1:
            # get timestamp
            item = self.sQ.get()
            server_ts = np.frombuffer(item, dtype=DTYPE_C_TIMESTAMP)
            item = self.cQ.get()
            client_ts = np.frombuffer(item, dtype=DTYPE_C_TIMESTAMP)
            
            #get csi header
            item = self.sQ.get()
            server_hdr = np.frombuffer(item, dtype=DTYPE_CSI_HDR)
            server_num_tones = server_hdr[0][7]
            item = self.cQ.get()
            client_hdr = np.frombuffer(item, dtype=DTYPE_CSI_HDR)
            client_num_tones = client_hdr[0][7]
            
            # get phase quantization
            item = self.sQ.get()
            server_phase_quantization_matrix = np.frombuffer(item, dtype=np.uint8)
            server_phase_quantization_matrix = server_phase_quantization_matrix.reshape(RX_ANTENNAS, TX_ANTENNAS, server_num_tones)
            item = self.cQ.get()
            client_phase_quantization_matrix = np.frombuffer(item, dtype=np.uint8)
            client_phase_quantization_matrix = client_phase_quantization_matrix.reshape(RX_ANTENNAS, TX_ANTENNAS, client_num_tones)
            
            #get amplitude quantization
            item = self.sQ.get()
            server_amplitude_quantization_matrix = np.frombuffer(item, dtype=np.uint8)
            server_amplitude_quantization_matrix = server_amplitude_quantization_matrix.reshape(RX_ANTENNAS, TX_ANTENNAS, server_num_tones)            
            item = self.cQ.get()
            client_amplitude_quantization_matrix = np.frombuffer(item, dtype=np.uint8)
            client_amplitude_quantization_matrix = client_amplitude_quantization_matrix.reshape(RX_ANTENNAS, TX_ANTENNAS, client_num_tones)
            
            self.pkt_cnt += 1
            fd.write('Quantization for packet ' + str(self.pkt_cnt) + '\n')
            print('Quantization for packet ' + str(self.pkt_cnt))
            
            print('Client-Server propagation delay (sec, nsec): ' + str(server_ts))
            fd.write('Client-Server propagation delay (sec, nsec): ' + str(server_ts) + '\n')
            print('Server-Client propagation delay (sec, nsec): ' + str(client_ts))
            fd.write('Server-Client propagation delay (sec, nsec): ' + str(client_ts) + '\n')
            
            fd.write('Server header: ' + str(server_hdr) + '\n')
            print('Server header: ' + str(server_hdr))
            
            fd.write('Server quantization:\n' + str(server_phase_quantization_matrix) + '\n')
            print('Server quantization:\n' + str(server_phase_quantization_matrix))
            
            fd.write('Client header: ' + str(client_hdr) + '\n')
            print('Client header: ' + str(client_hdr))
            
            fd.write('Client quantization:\n' + str(client_phase_quantization_matrix) + '\n')
            print('Client quantization:\n' + str(client_phase_quantization_matrix))
                        
            nr_rx = server_hdr[0][8]
            nr_tx = server_hdr[0][9]
            phase_match_rates = []
            amplitude_match_rates = []
            
            if(server_num_tones == client_num_tones and nr_rx == nr_tx):
                for tx_idx in range(0, nr_tx):
                    for rx_idx in range(0, nr_rx):
                        phase_conflicts = np.bitwise_xor(server_phase_quantization_matrix[rx_idx][tx_idx][:], client_phase_quantization_matrix[rx_idx][tx_idx][:])
                        amplitude_conflicts = np.bitwise_xor(server_amplitude_quantization_matrix[rx_idx][tx_idx][:], client_amplitude_quantization_matrix[rx_idx][tx_idx][:])
                        
                        phase_match_ratio = 1 - (sum(phase_conflicts) / server_num_tones)
                        amplitude_match_ratio = 1 - (sum(amplitude_conflicts) / server_num_tones)
                        
                        phase_match_rates.append(phase_match_ratio)
                        amplitude_match_rates.append(amplitude_match_ratio)
                        
                        fd.write('Match ratio phase RX ' + str(rx_idx) + ' TX ' + str(tx_idx) + '    : '  + str(phase_match_ratio) + '\n')
                        print('Match ratio phase RX ' + str(rx_idx) + ' TX ' + str(tx_idx) + '    : ' + str(phase_match_ratio))
                        
                        fd.write('Match ratio amplitude RX ' + str(rx_idx) + ' TX ' + str(tx_idx) + ': '  + str(amplitude_match_ratio) + '\n')
                        print('Match ratio amplitude RX ' + str(rx_idx) + ' TX ' + str(tx_idx) + ': ' + str(amplitude_match_ratio))
                print('\n')
                fd.write('\n')
                
                for bar, ratio in zip(phase_bars, phase_match_rates):
                    bar.set_height(ratio)
                for bar, ratio in zip(amplitude_bars, amplitude_match_rates):
                    bar.set_height(ratio)
                
                drawEvent.clear()
                newDataEvent.set()
                drawEvent.wait()
            #~ if(server_num_tones == client_num_tones and nr_rx != nr_tx):
                #~ for tx_idx in range(0, min(nr_tx, nr_rx)):
                    #~ for rx_idx in range(0, min(nr_tx, nr_rx)):
                        #~ phase_conflicts = np.bitwise_xor(server_phase_quantization_matrix[rx_idx][tx_idx][:], client_phase_quantization_matrix[rx_idx][tx_idx][:])
                        #~ amplitude_conflicts = np.bitwise_xor(server_amplitude_quantization_matrix[rx_idx][tx_idx][:], client_amplitude_quantization_matrix[rx_idx][tx_idx][:])
                        
                        #~ phase_match_ratio = 1 - (sum(phase_conflicts) / server_num_tones)
                        #~ amplitude_match_ratio = 1 - (sum(amplitude_conflicts) / server_num_tones)
                        
                        #~ phase_match_rates.append(phase_match_ratio)
                        #~ amplitude_match_rates.append(amplitude_match_ratio)
                        
                #~ for bar, ratio in zip(phase_bars, phase_match_rates):
                    #~ bar.set_height(ratio)
                #~ for bar, ratio in zip(amplitude_bars, amplitude_match_rates):
                    #~ bar.set_height(ratio)
                
                #~ drawEvent.clear()
                #~ newDataEvent.set()
                #~ drawEvent.wait()
            else:
                print('Server and client CSI have a different number of subcarriers!')
                fd.write('Server and client CSI have a different number of subcarriers!\n\n')

plt.show(block=False)

# Create Threads
serverThread = Worker(1, "Server Thread", serverQueue, SERVER_PORT)
clientThread = Worker(2, "Client Thread", clientQueue, CLIENT_PORT)
matcherThread = Matcher(3, "Matcher Thread", clientQueue, serverQueue)

# Start Threads
serverThread.start()
clientThread.start()
matcherThread.start()

while 1:
    newDataEvent.wait()
    fig.canvas.draw()
    newDataEvent.clear()
    drawEvent.set()
