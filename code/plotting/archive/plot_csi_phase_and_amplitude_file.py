#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from io import StringIO

#import hashlib
from Crypto.Cipher import AES

NUM_COLS = 56
PACKETS_TO_PLOT = 6
ERICH_TX = 3
ERICH_RX = 3
MIRINDA_TX = 3
MIRINDA_RX = 2

erich_log_file = './csi_server.log'
mirinda_log_file = './csi_client.log'

colors = ['#FF0000', '#0000FF','#800000', '#FFFF00', '#808000', '#00FF00', '#008000',
    '#00FFFF', '#008080', '#000080', '#FF00FF', '#800080', '#000000']

# Rows are rx indices, columns are tx indices.
# A value of False in the matrix will say that that specific rx and tx antenna pair is not shown.
# The number of rows and columns should match the number of received and transmission antennas,
#respectively.
erich_indices = [[False, False, False],
                [False, True, False],
                [False, False, False]]
mirinda_indices = [[False, False, False],
                [False, True, False],
                [False, False, False]]

def create_indices(indices):
    antenna_pairs = []
    
    if indices[0][0]:
        antenna_pairs.append((0, 0))
    if indices[0][1]:
        antenna_pairs.append((0, 1))
    if indices[0][2]:
        antenna_pairs.append((0, 2))
    if indices[1][0]:
        antenna_pairs.append((1, 0))
    if indices[1][1]:
        antenna_pairs.append((1, 1))
    if indices[1][2]:
        antenna_pairs.append((1, 2))
    if indices[2][0]:
        antenna_pairs.append((2, 0))
    if indices[2][1]:
        antenna_pairs.append((2, 1))
    if indices[2][2]:
        antenna_pairs.append((2, 2))
    
    return antenna_pairs

def get_cell(str):
    c = StringIO(str)
    mat = np.loadtxt(c, dtype=np.complex)
    arr = np.zeros((NUM_COLS,), dtype=np.complex)
    j = 0
    i = 0
    while j < NUM_COLS:
        if np.isreal(mat[i]):
            arr[j] = mat[i]+mat[i+1]
            i += 2
        else:
            arr[j] = mat[i]
            i += 1
        j += 1
    return arr
    
def read_csi_matrix(path, tx, rx):
	str = ''
	start = False
	fd = open(path, 'r')
	csi_values = []
		
	for line in fd:
		if '(' in line:
			pass
		elif '[' in line:
			tok = line.split('[')
			str += tok[-1].rstrip()
			start = True
		elif ']' in line:
			tok = line.split(']')
			str += tok[0].rstrip()
			start = False
			csi_values.append(get_cell(str))
			str = ''
		elif start:
			str += line.rstrip()
	
	packet_count = len(csi_values) / (tx*rx)
	csi = np.array(csi_values, dtype=complex)
	csi.shape = (int(packet_count), tx, rx, NUM_COLS)
	return csi
    
def getbytes(bits):
    done = False
    while not done:
        byte = 0
        for _ in range(0, 8):
            try:
                bit = next(bits)
            except StopIteration:
                bit = 0
                done = True
            byte = (byte << 1) | int(bit)
        yield byte

# calculate phase and amplitude for erich
e_i = create_indices(erich_indices)
csi_values = read_csi_matrix(erich_log_file, ERICH_TX, ERICH_RX)
phase_erich = np.zeros((len(e_i), PACKETS_TO_PLOT*NUM_COLS))
amplitude_erich = np.zeros((len(e_i), PACKETS_TO_PLOT*NUM_COLS))
for ant_pair in range(len(e_i)):
    phase = []
    amplitude = []
    for i in range (0, PACKETS_TO_PLOT):
        phase.extend(np.angle(csi_values[i, e_i[ant_pair][0], e_i[ant_pair][1],:]))
        amplitude.extend(10*np.log10(np.absolute(csi_values[i, e_i[ant_pair][0], e_i[ant_pair][1], :])))
    phase_erich[ant_pair][:] = phase
    amplitude_erich[ant_pair][:] = amplitude

# calculate phase and amplitude for mirinda
m_i = create_indices(mirinda_indices)
csi_values = read_csi_matrix(mirinda_log_file, MIRINDA_TX, MIRINDA_RX)
phase_mirinda = np.zeros((len(m_i), PACKETS_TO_PLOT*NUM_COLS))
amplitude_mirinda = np.zeros((len(m_i), PACKETS_TO_PLOT*NUM_COLS))
for ant_pair in range(len(m_i)):
    phase = []
    amplitude = []
    for i in range (0, PACKETS_TO_PLOT):
        phase.extend(np.angle(csi_values[i, m_i[ant_pair][0], m_i[ant_pair][1],:]))
        amplitude.extend(10*np.log10(np.absolute(csi_values[i, m_i[ant_pair][0], m_i[ant_pair][1], :])))
    phase_mirinda[ant_pair][:] = phase
    amplitude_mirinda[ant_pair][:] = amplitude

# search for common points in phase data, assume that same antenna configuration was choosen for erich and mirinda
common_candidates = []
common_points = []
common_point = []
if (len(phase_mirinda)) == (len(phase_erich)):
    for i in range(len(phase_mirinda)):
        common_candidates.append(list(np.argwhere(np.diff(np.sign(phase_erich[i][:] - phase_mirinda[i][:])) != 0).reshape(-1)))
    print(common_candidates)
    # find common point for erich and mirinda where difference between measurements is closest to zero
    for i in range(len(common_candidates)):
        cp_index = np.argmin(abs(phase_erich[i][common_candidates[i]] - phase_mirinda[i][common_candidates[i]]))
        print("closest phase values are at index:", [common_candidates[i][cp_index]])
        print("erich   phase:", phase_erich[i][common_candidates[i][cp_index]])
        print("mirinda phase:", phase_mirinda[i][common_candidates[i][cp_index]])
        x = [common_candidates[i][cp_index], common_candidates[i][cp_index]+1]
        y_e = phase_erich[i][x]
        y_m = phase_mirinda[i][x]
        poly_coeff_e = np.polyfit(x, y_e, 1)
        poly_coeff_m = np.polyfit(x, y_m, 1)
        poly_e = np.poly1d(poly_coeff_e)
        poly_m = np.poly1d(poly_coeff_m)
        poly_diff = poly_e - poly_m
        common_point = [poly_diff.r, poly_e(poly_diff.r)]
        print("intersection at: x=", common_point[0], "y=", common_point[1])
        #compute AES key
        key = np.zeros((AES.block_size, 1), dtype=np.uint8)
        k = 0
        for b in getbytes(iter(common_point[1])):
            if k < AES.block_size:
                key[k] = b
                k += 1
            else:
                break
        cipher = AES.new(key, AES.MODE_CFB, key)
        key = cipher.encrypt(key)
        #print numpy array in bin
        #s = binary(key.hex())
        s = key.hex()
        print('AES key:', s)
        
# plot the data
plt.figure(0)
plt.subplot(2, 1, 1)
c_i = 0
for ant_pair in range(0, len(e_i)):
    plt.plot(amplitude_erich[ant_pair], label='erich:' + str(e_i[ant_pair]), color=colors[c_i])
    c_i = (c_i+1)%len(colors)
for ant_pair in range(0, len(m_i)):
    plt.plot(amplitude_mirinda[ant_pair], label='mirinda:' + str(m_i[ant_pair]), color=colors[c_i])
    c_i = (c_i+1)%len(colors)
plt.xlabel('Index')
plt.ylabel('Magnitude [dB]')
plt.legend()
plt.grid()

plt.subplot(2, 1, 2)
c_i = 0
for ant_pair in range(0, len(e_i)):
    plt.plot(phase_erich[ant_pair], label='erich' + str(e_i[ant_pair]), color=colors[c_i])
    c_i = (c_i+1)%len(colors)
for ant_pair in range(0, len(m_i)):
    plt.plot(phase_mirinda[ant_pair], label='mirinda' + str(m_i[ant_pair]), color=colors[c_i])
    c_i = (c_i+1)%len(colors)
if (len(phase_mirinda)) == (len(phase_erich)):
    for ant_pair in range(0, len(m_i)):
        plt.plot(common_candidates[ant_pair], phase_mirinda[ant_pair][common_candidates[ant_pair]], 'x', color='#000000')
        plt.plot(common_point[0], common_point[1], 'o', color='#ABCDCB')

plt.xlabel('Index')
plt.ylabel('Phase [rad]')
plt.legend()
plt.grid()

plt.show()
