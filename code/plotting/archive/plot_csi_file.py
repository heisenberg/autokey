#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from io import StringIO

NUM_COLS = 56

file_name = 'csi_log.txt'
fd = open(file_name, 'r')

def get_row(str):
    c = StringIO(str)
    mat = np.loadtxt(c, dtype=np.complex)
    #adjacent values are from the same complex number
    arr = np.zeros((NUM_COLS,), dtype=np.complex)
    j = 0
    i = 0
    while j < NUM_COLS:
        if np.isreal(mat[i]):
            arr[j] = mat[i]+mat[i+1]
            i += 2
        else:
            arr[j] = mat[i]
            i += 1
        j += 1
    return arr
def get_timestamp(str):
    tok = line.split(',')
    tok = tok[0].split('(')
    return float(tok[-1])

time_support = []
rx0 = []
rx1 = []
rx2 = []

str = ''
start = False
tx_antenna = 0
rx_antenna = 0
rx_pwr = None
for line in fd:
    if '(' in line:
        timestamp = get_timestamp(line)
        time_support.append(timestamp)
    elif '[' in line:
        tok = line.split('[')
        str += tok[-1].rstrip()
        start = True
    elif ']' in line:
        tok = line.split(']')
        str += tok[0].rstrip()
        start = False
        row = get_row(str)
        str = ''
        if 0 == tx_antenna:
            rx_pwr = np.absolute(row)
            tx_antenna += 1
        else:
            rx_pwr += np.absolute(row)/2
            tx_antenna = 0
            if 0 == rx_antenna:
                rx0.append(np.mean(rx_pwr))
                rx_antenna += 1
            elif 1 == rx_antenna:
                rx1.append(np.mean(rx_pwr))
                rx_antenna += 1
            else:
                rx2.append(np.mean(rx_pwr))
                rx_antenna = 0
    elif start:
        str += line.rstrip()

plt.figure()
plt.plot(rx0)
plt.plot(rx1, 'r')
plt.plot(rx2, 'g')
plt.legend(('RX0', 'RX1', 'RX2'))
plt.grid()
plt.show()
