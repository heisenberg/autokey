#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 17:19:19 2016


"""
import os
import time
from csi import receiver as csi_receiver
import numpy as np
import matplotlib.pyplot as plt

# define receiver params
csi_dev = '/dev/CSI_dev'
runt = 60 #total time in seconds for the measurements
ival = 1.0 # interval in seconds
debug = True

# check CSI device
if not os.path.exists(csi_dev):
    raise ValueError('Could not find CSI device: %s.' % csi_dev)

# set timer
start_time = time.time()
end_time = start_time + runt

x_axis = []
y_axis = []
print("Start receiving CSI for %d sec in %d sec intervals..." %(runt, ival))
while time.time() < end_time:
    csi = csi_receiver.scan(debug=debug)
    #print("csi," +str(csi))
    time.sleep(ival)

    csi_0 = csi[0].view(np.recarray)
    #print(str(csi_0.header))
    #print(str(csi_0.csi_matrix))
    tmp = csi_0.header[0]
    x_axis.append(tmp[0])
    y_axis.append(tmp[1])

#csi[0]['csi_matrix']
#csi[3]['csi_matrix']
#csi[0:2]

plt.figure()
plt.plot(x_axis, y_axis)
plt.grid()
plt.title('Plot title')
plt.xlabel('time')
plt.ylabel('Measurement')
