#!/usr/bin/env python3

import socket
import numpy as np
import matplotlib.pyplot as plt
import textwrap as tw
#from struct import *

DEBUG = 1
TCP_IP = '192.168.1.244'
TCP_PORT = 51111
HEADER_SIZE = 25
CSI_BUFFER = 4096

# CSI packet header type for numpy
DTYPE_CSI_HDR = np.dtype([
    ("tstamp", np.uint64),
    ("csi_len", np.uint16),
    ("channel", np.uint16),
    ("phyerr", np.uint8),
    ("noise", np.uint8),
    ("rate", np.uint8),
    ("chanbw", np.uint8),
    ("num_tones", np.uint8),
    ("nr", np.uint8),
    ("nc", np.uint8),
    ("rssi", np.uint8),
    ("rssi_0", np.uint8),
    ("rssi_1", np.uint8),
    ("rssi_2", np.uint8),
    ("pld_len", np.uint16)
])

DTYPE_CSI_HDR = DTYPE_CSI_HDR.newbyteorder('>')

DTYPE_CSI_COMPLEX = np.dtype([
    ("real", np.int32),
    ("imag", np.int32)
])

DTYPE_CSI_COMPLEX = DTYPE_CSI_COMPLEX.newbyteorder('>')

#~ xAchse=np.arange(0,56,1)
#~ yAchse=np.array([0]*56)
#~ fig = plt.figure(1)
#~ ax = fig.add_subplot(111)
#~ ax.grid(True)
#~ ax.set_title("CSI real time plot")
#~ ax.set_xlabel("Index")
#~ ax.set_ylabel("Magnitude [dB]")
#~ ax.axis([0,56,-10,35])
#~ line1=ax.plot(xAchse, yAchse, '-', color='#FF0000', label = 'amplitude') 

xAchse=np.arange(0,56,1)
yAchse=np.array([0]*56)
fig = plt.figure(1)
ax = fig.add_subplot(111)
ax.grid(True)
ax.set_title("CSI real time plot")
ax.set_xlabel("Index")
ax.set_ylabel("Phase [rad]")
ax.axis([0,56,-3.2,3.2])
line1=ax.plot(xAchse,yAchse,'-',color='#FF0000', label = 'rx1tx1')
line2=ax.plot(xAchse,yAchse+1,'-',color='#00FF00', label = 'rx2tx2')
line3=ax.plot(xAchse,yAchse+2,'-',color='#0000FF', label = 'rx3tx3')
ax.legend(loc=1)

#~ phase_axis = ax.twinx()
#~ phase_axis.set_ylabel('Phase [rad]')
#~ phase_axis.set_ylim(-3.2, 3.2)
#~ line2=phase_axis.plot(xAchse, yAchse+1, '-', color='#0000FF', label = 'phase')

#~ lines = line1 + line2 + line3
#~ labels = [l.get_label() for l in lines]
#~ ax.legend(lines, labels, loc=0)

pkt_ctr = 0
plot_annotations = "CSI packets received: " + str(pkt_ctr) + " Header data: "
figtxt = plt.figtext(0.5, 0.0, plot_annotations, wrap=True, horizontalalignment='center', fontsize=8)

manager = plt.get_current_fig_manager()

idx1 = 0
idx2 = 1
msg1 = bytearray()
msg2 = bytearray()
hdr1 = bytearray()
hdr2 = bytearray()
csi_slider = np.full((3, 3, 112), (0 + 1.j*0), dtype=complex)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setblocking(True)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
#if DEBUG:
    #print('Connection address:', addr)

def CsiDataCallback(arg):
    global conn, pkt_ctr, msg1, msg2, hdr1, hdr2, idx1, idx2, csi_slider
    
    # First receive header of 25 bytes
    msg1 = bytearray()
    bytes_left = HEADER_SIZE
    while bytes_left:
        data = conn.recv(bytes_left)
        msg1.extend(data)
        bytes_left -= len(data)
    # Process header
    hdr1 = np.frombuffer(msg1, dtype=DTYPE_CSI_HDR)                  # Alternative way of parsing that: hdr1 = unpack('>QHH11BH', msg1)
    if DEBUG:
        print(hdr1)
    
    # Receive CSI data
    msg1 = bytearray()
    bytes_left = hdr1[0][1]
    while bytes_left:
        data = conn.recv(bytes_left)
        msg1.extend(data)
        bytes_left -= len(data)
        
    pkt_ctr += 1
    plot_annotations = "CSI packets received: " + str(pkt_ctr) + " Header data: " + str(hdr1)
    figtxt.set_text(plot_annotations)
    
    # Process CSI data
    csi_data = np.frombuffer(msg1, dtype=DTYPE_CSI_COMPLEX)          # Alternative: formatstr = '>' + str(int(hdr1[0][1]/2)) + 'h'; csi_data = unpack(formatstr, msg1)
    
    csi = csi_data['real'] + 1.j*csi_data['imag']
        
    rxtx1 = []
    rxtx2 = []
    rxtx3 = []
    rx = hdr1[0][8]
    tx = hdr1[0][9]
    num_tones = hdr1[0][7]
    
    csi = csi.reshape(rx,tx,num_tones)
    if DEBUG:
        print(csi)
    for i in range(0, num_tones):
        rxtx1.append(csi[0][0][i])
        rxtx2.append(csi[1][1][i])
        if rx == 3 and tx == 3:
            rxtx3.append(csi[2][2][i])
        
    #amplitude = 10*np.log10(np.absolute(rxtx1))
    phase = np.angle(rxtx1)
    phase2 = np.angle(rxtx2)
    if rx == 3 and tx == 3:
        phase3 = np.angle(rxtx3)
    
    #ax.axis([0,56,amp.min(), amp.max()])
    line1[0].set_data(xAchse, phase)
    line2[0].set_data(xAchse, phase2)
    if rx == 3 and tx == 3:
        line3[0].set_data(xAchse, phase3)
    else:
        line3[0].set_data(xAchse, yAchse+2)
        
    #~ line2[0].set_data(xAchse, phase)
    manager.canvas.draw()

timer = fig.canvas.new_timer(interval=200)
timer.add_callback(CsiDataCallback, ())
timer.start()

plt.show()
