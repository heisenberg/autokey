#!/usr/bin/env python3

from gui.csi_types import DTYPE_CSI_HDR, CSI_HEADER_SIZE, DTYPE_C_TIMESTAMP
import socket
import numpy as np
import threading
import queue

MAX_RCV_TIM_DIFF_US = 1000

class CSIReceiver(threading.Thread):
    def __init__(self, threadID, name, queue, port):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.queue = queue
        self.ip = "" # the empty string represents INADDR_ANY
        self.port = port
        self.conn = None
        self.active = threading.Event()
    
    def customize(self):
        pass
        
    def run(self):
        print('Starting ' + str(self.name))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((self.ip, self.port))
        s.listen(1)
        self.conn, addr = s.accept()
        self.conn.settimeout(1)
        print('Connection address:', addr)
        
        while not self.active.wait(0):
            self.customize()
            
            # First receive header of 25 bytes
            msg = bytearray()
            bytes_left = CSI_HEADER_SIZE
            while bytes_left:
                try:
                    data = self.conn.recv(bytes_left)
                except socket.timeout:
                    if self.active.wait(0):
                        self.close_connection()
                        return
                    else:
                        continue          
                msg.extend(data)
                bytes_left -= len(data)
        
            hdr = np.frombuffer(msg, dtype=DTYPE_CSI_HDR)
        
            # Receive CSI data
            msg = bytearray()
            bytes_left = hdr[0][1]
            while bytes_left:
                try:
                    data = self.conn.recv(bytes_left)
                except socket.timeout:
                    if self.active.wait(0):
                        self.close_connection()
                        return
                    else:
                        continue
                msg.extend(data)
                bytes_left -= len(data)

            self.queue.put(hdr)
            self.queue.put(msg)
        
        self.close_connection()
    
    def stop(self):
        print("Stop receiver thread.")
        self.active.set()
        
    def close_connection(self):
        print("Close receiver thread connection.")
        self.conn.close()
            
class TsCSIReceiver(CSIReceiver):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
    def customize(self):
        if self.conn != None:
            # receive timestamp
            msg = bytearray()
            bytes_left = 8
            while bytes_left:
                data = self.conn.recv(bytes_left)
                msg.extend(data)
                bytes_left -= len(data)
                
            ts = np.frombuffer(msg, dtype=DTYPE_C_TIMESTAMP)     
            self.queue.put(ts)
            
def orderTsCSI(server_queue, client_queue):
    server_ts = np.frombuffer(server_queue.get(), dtype=DTYPE_C_TIMESTAMP)
    client_ts = np.frombuffer(client_queue.get(), dtype=DTYPE_C_TIMESTAMP)
    order = True
    while order:            
        server_t_us = (server_ts['sec'] * 1000000) + (server_ts['nsec'] / 1000)
        client_t_us = (client_ts['sec'] * 1000000) + (client_ts['nsec'] / 1000)
        timediff_us = server_t_us - client_t_us
        if timediff_us < -MAX_RCV_TIM_DIFF_US:
            server_queue.get()  #drop server hdr
            server_queue.get()  #drop server csi
            server_ts = server_queue.get() #get new server ts
        elif timediff_us > MAX_RCV_TIM_DIFF_US:
            client_queue.get()  #drop client hdr
            client_queue.get()  #drop client csi
            client_ts = client_queue.get() #get new client ts
        else:
            order = False
