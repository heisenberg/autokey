#include "queue.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

static uint8_t enqueue_check(void);
static uint8_t dequeue_check(void);

/* Some globals for testing because I'm lazy. */
static struct queue test_queue;
static Q_TYPE item;

/**
 * Test the queue.
 */ 
int main(void) {
    uint8_t result;
    
    /* Initialize */
    item = 0;
    init_queue(&test_queue);
    printf("Queue array at: %p\n", (void*) test_queue.q);
    printf("Queue head  at: %d\n", test_queue.head);
    printf("Queue last  at: %d\n\n", test_queue.last);
    
    /* Check enqueue */
    printf("--- enqueue check: enqueue Q_MAX_ELEMENTS = %d elements. ---\n", Q_MAX_ELEMENTS);
    result = enqueue_check();
    if(result) {
        printf("--- enqueue check: OK. ---\n\n");
    }
    else {
        printf("--- enqueue check: FAIL. ---\n\n");
        exit(EXIT_FAILURE);
    }    
    
    /* Check is_full */
    printf("--- is_full check: enqueue element number Q_MAX_ELEMENTS + 1 = %d. ---\n", Q_MAX_ELEMENTS + 1);
    if(!enqueue(&test_queue, &item)) {
        printf("--- is_full Check: OK. ---\n\n");
    }
    else {
        printf("--- is_full Check: FAIL. ---\n\n");
        exit(EXIT_FAILURE);
    }
    
    /* Check dequeue */
    printf("--- dequeue check: dequeue Q_MAX_ELEMENTS = %d elements. ---\n", Q_MAX_ELEMENTS);
    dequeue_check();
    if(result) {
        printf("--- dequeue check: OK. ---\n\n");
    }
    else {
        printf("--- dequeue check: FAIL. ---\n\n");
        exit(EXIT_FAILURE);
    }
    
    /* Check is_empty */
    result = 1;
    printf("--- is_empty check: ---\n");
    for(uint8_t i = 0; i < 3; i++) {
        result = dequeue_check();
    }
    if(!result) {
        printf("--- is_empty check: OK. ---\n\n");
    }
    else {
        printf("--- is_empty check: FAIL. ---\n\n");
        exit(EXIT_FAILURE);
    }
        
    /* Check Wraparound */
    printf("--- Wraparound check: ---\n");
    for(uint8_t i = 0; i < 3; i++) {
        result = enqueue_check();
        if(!result) {
            exit(EXIT_FAILURE);
        }
        result = dequeue_check();
        if(!result) {
            exit(EXIT_FAILURE);
        }
    }
    printf("--- Wraparound check: OK. ---\n");
    
}

static uint8_t enqueue_check(void) {
    uint8_t result = 1;
    
    for(uint8_t i = 0; i < Q_MAX_ELEMENTS; i++) {
        item++;
        printf("enqueue(%d)\n", item);
        fflush(stdout);
        if(!enqueue(&test_queue, &item)) {
            result = 0;
            break;
        }
    }
    
    return result;
}

static uint8_t dequeue_check(void) {
    uint8_t result = 1;
    
    for(uint8_t i = 0; i < Q_MAX_ELEMENTS; i++) {
        if(!dequeue(&test_queue, &item)) {
            result = 0;
            break;
        }
        printf("dequeue() == %d\n", item);
    }
    
    return result;
}
