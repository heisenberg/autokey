/**
 * @file
 * @brief A simple fixed sized queue.
 */

#ifndef __QUEUE_H_
#define __QUEUE_H_

#include <stdint.h>

#define DEBUG               1

#define Q_TYPE              int
#define Q_SIZE              8
#define Q_MAX_ELEMENTS      (Q_SIZE - 1)
#if (DEBUG == 1)
    #define Q_PRINT(format_str, ...)    do { printf(format_str, ##__VA_ARGS__); fflush(stdout); } while(0)
#else
    #define Q_PRINT(format_str, ...)
#endif

/**
 * A fixed sized queue.
 * It consist of a fixed sized array to a known type <Q_TYPE>
 * as well as a head and a last pointer.
 */
struct queue {
    Q_TYPE q[Q_SIZE];
    uint32_t head;
    uint32_t last;
};

/**
 * Must be called before struct queue can be used.
 * 
 * \param q A Pointer to the queue.
 */
void init_queue(struct queue *q);

/**
 * Check if queue is empty.
 * 
 * \param q A Pointer to the queue.
 * \return 1 if queue is empty, else 0.
 */
uint8_t is_empty(struct queue *q);

/**
 * Check if queue is full.
 * 
 * \param q A Pointer to the queue.
 * \return 1 if queue is full, else 0.
 */
uint8_t is_full(struct queue *q);

/**
 * Copies an element into the queue if the queue is not full.
 * 
 * \param q A Pointer to the queue.
 * \param element Pointer to the element to copy.
 * \return 1 on success, 0 if queue is full.
 */
uint8_t enqueue(struct queue *q, Q_TYPE *element);

/**
 * Returns and removes the first element from the queue if it is not empty.
 * 
 * \param q A Pointer to the queue.
 * \param element Pointer to an element into which the first element of the queue will be copied.
 * \return 1 on success, 0 if the queue is empty.
 */
uint8_t dequeue(struct queue *q, Q_TYPE *element);

#endif
