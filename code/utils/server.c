#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "server.h"
#include "print.h"
#include "transceive.h"

static struct event_handler *handler_registry[MAX_N_HANDLERS];

static int get_server_handle(void *instance) {
    struct server *server_p = (struct server *) instance;
    return server_p->listen_socket;
}

static int get_client_handle(void *instance) {
    struct client *client_p = (struct client *) instance;
    return client_p->socket;
}

static void destroy_client(struct client *client_p)
{    
    unregister_handler(&client_p->handler);
    close(client_p->socket);
    for(size_t i = 0; i < MAX_N_CLIENTS; i++) {
        if(client_p->server_p->clients[i] == client_p) {
            client_p->server_p->clients[i] = NULL;
        }
    }
    free(client_p);
}

/**
 * Handle incoming data on socket.
 * 
 * \param instance Pointer to the "object" that called this function.
 */
static void read_handler(void *instance)
{
    uint8_t buf[256];
    uint32_t bytes_received;
    struct client *client_p = (struct client *) instance;
    
    bytes_received = msg_recv(client_p->socket, buf, 13);
    if(bytes_received > 0) {
        PRINT(LOG_NOTICE, "Received: %s\n", buf);
    } else {
        /* Connection has been closed by peer. */
        destroy_client(client_p);
    }
}

/**
 * Accept incoming connections and dynamically create a new client for each connection.
 * 
 * \param instance Pointer to the "object" that called this function.
 */
static void connection_request_handler(void *instance)
{
    int no_delay = 1;
    int priority = 6;    
    struct server *server_p = (struct server *) instance;
    
    PRINT(LOG_NOTICE, "Incoming connection request.\n");
    for(size_t i = 0; i < MAX_N_CLIENTS; ++i) {
        if(server_p->clients[i] == NULL) {
            /* Create and initialize new client. */
            struct client *new_client = malloc(sizeof(struct client));            
            new_client->socket = accept(server_p->listen_socket, 0, 0);
            if(new_client->socket == -1) {
                PRINT(LOG_WARNING, "Accepting connection failed: %s.\n", strerror(errno));
            }
            new_client->handler.instance = new_client;
            new_client->handler.handle_event = &read_handler;
            new_client->handler.get_handle = &get_client_handle;
            new_client->server_p = server_p;
            
            /* SO_PRIORITY : Low priotity 0 to high priority 6 (7 with CAP_NET_ADMIN capability).
             *               Only works if network interface qdisc (queuing discipline) supports priorities. */
            if(setsockopt(new_client->socket, SOL_SOCKET, SO_PRIORITY, (void const *) &priority, sizeof(priority)) == -1) {
                PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
            }
            /* TCP_NODELAY : Don't use Nagle's algorithm when sending. */
            if(setsockopt(new_client->socket, IPPROTO_TCP, TCP_NODELAY, (void const *) &no_delay, sizeof(no_delay)) == -1) {
                PRINT(LOG_WARNING, "setsockopt: %s.\n", strerror(errno));
            }

            server_p->clients[i] = new_client;
            register_handler(&new_client->handler);
            return;
        }
    }
    PRINT(LOG_WARNING, "Maximum number of connections has been reached.\n");    
}

/**
 * Create a listen socket for tcp connections.
 * 
 * \param listen_port Port to listen on for connections.
 * \return -1 on error.
 */
int create_listen_socket(uint16_t listen_port)
{
    struct sockaddr_in address = {0};
    int listen_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
        
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_family = AF_INET;
    address.sin_port = htons(listen_port);    
    
    if(listen_socket == -1) {
        PRINT(LOG_ERROR, "Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(bind(listen_socket, (struct sockaddr*) &address, sizeof(struct sockaddr_in)) == -1) {
        PRINT(LOG_ERROR, "Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(listen(listen_socket, MAX_N_CLIENTS)) {
        PRINT(LOG_ERROR, "Socket listening failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    return listen_socket;
}

/**
 * Fill an array of struct pollfd with the pollfds from the registry.
 * 
 * \param fds The struct pollfd to fill.
 * \return The number of pollfds that have been added to fds.
 */
size_t initialize_pollfd(struct pollfd *fds)
{
    struct event_handler *handler;
    size_t n_registered = 0;
    
    for(size_t i = 0; i < MAX_N_HANDLERS; ++i) {
        handler = handler_registry[i];
        if(handler != NULL) {
            fds[n_registered] = handler->poll_fd;
            ++n_registered;
        }
    }
    
    return n_registered;
}

/**
 * Performs multiplexing of available file descriptors for I/O via poll and dispatches them to correponding handlers.
 * Decouples event multiplexing from application logic.
 */
void handle_events()
{
    static struct pollfd fds[MAX_N_HANDLERS] = {0};
    struct event_handler *handler;
    size_t nfds = initialize_pollfd(fds);
    
    if(poll(fds, nfds, -1) > 0) {
        for(size_t i = 0; i < nfds; ++i) {            
            if(fds[i].revents & POLLIN) {
                /* XXX: Bad runtime behaviour because of nested loop over array.
                 *      Use better performing ADT like red-black tree. */
                for(size_t h = 0; h < MAX_N_HANDLERS; ++h) {
                    handler = handler_registry[h];
                    if((handler != NULL) &&
                        handler->poll_fd.fd == fds[i].fd) {
                            handler->handle_event(handler->instance);
                    }
                }
            } else if(fds[i].revents & (POLLERR | POLLHUP | POLLNVAL)) {
                /* TODO: Handle error condition. */
                PRINT(LOG_ERROR, "Poll error.\n");
            }
        }
    }
}

/**
 * Register a handler in the event handler registry.
 * 
 * \param handler A pointer to the struct event_handler to register.
 * \return 0 on success, -1 if no more handlers can be registered.
 */
int register_handler(struct event_handler *handler)
{
    for(size_t i = 0; i < MAX_N_HANDLERS; ++i) {
        if(handler_registry[i] == NULL) {
            handler->poll_fd.fd = handler->get_handle(handler->instance);
            handler->poll_fd.events = POLLIN;
            handler_registry[i] = handler;
            return 0;
        }
    }
    PRINT(LOG_ERROR, "Can't register more handlers: registry is full.\n");
    return -1;
}

/**
 * Remove a handler from the event handler registry.
 * 
 * \param handler A pointer to the struct event_handler to remove from the handler registry.
 * \return 0 on success, -1 if handler could not be found in registry.
 */
int unregister_handler(struct event_handler *handler)
{
    for(size_t i = 0; i < MAX_N_HANDLERS; ++i) {
        if(handler_registry[i] != NULL &&
            handler_registry[i]->instance == handler->instance) {
            handler_registry[i] = NULL;
            return 0;
        }
    }
    return -1;
}

/**
 * Dynamically create a new server.
 * 
 * \param port The port the server will listen on.
 * \param handler_f A function pointer to the function that handles incoming connections.
 * \return A pointer the server struct that has to be destroyed via destroy_server. 
 */
struct server* create_server(uint16_t port, handle_event_f handler_f)
{
    struct server *new_server = malloc(sizeof(struct server));
    new_server->listen_socket = create_listen_socket(port);
    PRINT(LOG_INFO, "Listening on port %d.\n", port);
    if(handler_f == NULL) {
        new_server->handler.handle_event = &connection_request_handler;        
    } else {
        new_server->handler.handle_event = handler_f;
    }
    new_server->handler.get_handle = &get_server_handle;
    new_server->handler.instance = new_server;
    register_handler(&new_server->handler);
    return new_server;
}

/**
 * Delete the struct server and all it's connected clients.
 * 
 * \param server_p Pointer to the struct server to delete.
 */
void destroy_server(struct server *server_p)
{    
    for(size_t i = 0; i < MAX_N_CLIENTS; ++i) {
        if(server_p->clients[i] != NULL) {
            unregister_handler(&server_p->clients[i]->handler);
            close(server_p->clients[i]->socket);
            free(server_p->clients[i]);
        }
    }
    unregister_handler(&server_p->handler);
    close(server_p->listen_socket);    
    free(server_p);
}

/* Test */
#if 0
int main(int argc, char **argv)
{
    struct server *server_p = create_server(50001, NULL);
    
    while(1) {
        handle_events();
    }
}
#endif
