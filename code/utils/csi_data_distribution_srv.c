#include "csi_data_distribution_srv.h"
#include "csi.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>

#define CSI_DATA_PRODUCTION_PORT        50010
#define CSI_DATA_CONSUMPTION_PORT       50011
#define CSI_DATA_IP_ADDR                "192.168.2.1"

void msr_service(void);
int prepare_socket(struct sockaddr_in *addr);
int new_connection(int listen_socket, fd_set *fdset);

int main(int argc, char **argv) {
    msr_service();
    exit(EXIT_SUCCESS);
}

void msr_service(void) {
    int production_listen_sockfd;
    int consumption_listen_sockfd;
    int production_sockfd;
    int consumption_sockfd;
    struct sockaddr_in production_addr;
    struct sockaddr_in consumption_addr;
    fd_set active_read_fd_set;
    fd_set active_write_fd_set;
    fd_set read_fd_set;
    fd_set write_fd_set;
    int enable = 1;
    
    // Production
    memset(&production_addr, 0, sizeof(struct sockaddr_in));
	inet_aton(CSI_DATA_IP_ADDR, &production_addr.sin_addr);     // TODO: dynamically assign wlan ip
    production_addr.sin_family = AF_INET;
    production_addr.sin_port = htons(CSI_DATA_PRODUCTION_PORT);
    production_listen_sockfd = prepare_socket(&production_addr);
    
    // Consumption
    memcpy(&consumption_addr, &production_addr, sizeof(struct sockaddr_in));
    consumption_listen_sockfd = prepare_socket(&consumption_addr);
    
    FD_ZERO(&active_read_fd_set);
    FD_SET(production_listen_sockfd, &active_read_fd_set);
    FD_SET(consumption_listen_sockfd, &active_read_fd_set);
    
    printf("Start csi data distribution service.\n");
    while(1) {
        read_fd_set = active_read_fd_set;
        if(select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) printf("Select error: %s.\n", strerror(errno));
        for(int fd_i = 0; fd_i < FD_SETSIZE; fd_i++) { // TODO: nfds = max fd + 1
            if(FD_ISSET(fd_i, &read_fd_set)) {
                if(fd_i == production_listen_sockfd) {
                    new_connection(production_listen_sockfd, &active_read_fd_set);
                }
                else if(fd_i == consumption_listen_sockfd) {
                    new_connection(consumption_listen_sockfd, &active_write_fd_set);
                }
                else {
                    // TODO: Get data
                    write_fd_set = active_write_fd_set;
                    if(select(FD_SETSIZE, NULL, &write_fd_set, NULL, NULL) < 0) printf("Select error: %s.\n", strerror(errno));
                    for(int fd_i = 0; fd_i < FD_SETSIZE; fd_i++) { // TODO: nfds = max fd + 1
                        if(FD_ISSET(fd_i, &read_fd_set)) {
                            // TODO: send data
                        }
                    }
                }
            }
        }
    }
}

int prepare_socket(struct sockaddr_in *addr) {
    int sockfd;
    int enable = 1;
    
    addr->sin_port = htons(CSI_DATA_CONSUMPTION_PORT);
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(sockfd == -1) {
        printf("Socket creation failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(enable)) == -1) {
        printf("setsockopt: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(bind(sockfd, (struct sockaddr*) addr, sizeof(struct sockaddr_in)) == -1) {
        printf("Socket binding failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(listen(sockfd, 1)) {
        printf("Socket listening failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    return sockfd;
}

int new_connection(int listen_socket, fd_set *fdset) {
    int sockfd;
    
    sockfd = accept(listen_socket, 0, 0);
    if(sockfd == -1) {
        printf("Accepting connection failed: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    else {
        FD_SET(sockfd, fdset);
        printf("New connection established.\n");
    }
    
    return sockfd;
}
