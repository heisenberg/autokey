#include "time_calc.h"

#define DEBUG 1

#if (DEBUG == 1)
#include <assert.h>
#endif

/**
 * result.sign is negative if t1 < t2, otherwise positive. 
 */
void time_sub(struct timediff *result, struct timespec const *const t1, struct timespec const *const t2) {	
	#if (DEBUG == 1)
    assert(t1->tv_nsec <= MAX_NANOSLEEP_NSEC);
	assert(t2->tv_nsec <= MAX_NANOSLEEP_NSEC);
    #endif
	
	if(t1->tv_sec > t2->tv_sec) {
		if(t1->tv_nsec >= t2->tv_nsec) {
			result->time.tv_sec = t1->tv_sec - t2->tv_sec;
			result->time.tv_nsec = t1->tv_nsec - t2->tv_nsec;
		}
		else {
			result->time.tv_sec = t1->tv_sec - t2->tv_sec - 1;
			result->time.tv_nsec = MAX_NANOSLEEP_NSEC - t2->tv_nsec + t1->tv_nsec + 1;
		}
		result->sign = 1;
	}
	else if(t1->tv_sec < t2->tv_sec) {
		if(t1->tv_nsec <= t2->tv_nsec) {
			result->time.tv_sec = t2->tv_sec - t1->tv_sec;
			result->time.tv_nsec = t2->tv_nsec - t1->tv_nsec;
		}
		else {
			result->time.tv_sec = t2->tv_sec - t1->tv_sec - 1;
			result->time.tv_nsec = MAX_NANOSLEEP_NSEC + t2->tv_nsec - t1->tv_nsec + 1;
		}
		result->sign = -1;
	}
	else {
		result->time.tv_sec = 0;
		if(t1->tv_nsec >= t2->tv_nsec) {
			result->time.tv_nsec = t1->tv_nsec - t2->tv_nsec;
			result->sign = 1;
		}
		else {
			result->time.tv_nsec = t2->tv_nsec - t1->tv_nsec;
			result->sign = -1;
		}
	}
}

void time_add(struct timespec *result, struct timespec const *const t1, struct timespec const *const t2) {
	#if (DEBUG == 1)
    assert(t1->tv_nsec <= MAX_NANOSLEEP_NSEC);
	assert(t2->tv_nsec <= MAX_NANOSLEEP_NSEC);
    #endif
	
	if(t1->tv_nsec + t2->tv_nsec <= MAX_NANOSLEEP_NSEC) {
		result->tv_nsec = t1->tv_nsec + t2->tv_nsec;
		result->tv_sec = t1->tv_sec + t2->tv_sec;
	}
	else {
		result->tv_nsec = t1->tv_nsec + t2->tv_nsec - (MAX_NANOSLEEP_NSEC + 1);
		result->tv_sec = t1->tv_sec + t2->tv_sec + 1;
	}
}

/**
 * Calculates t1 - t2.
 */
void timediff_substract(struct timediff *result, struct timediff const *const t1, struct timediff const *const t2) {
	#if (DEBUG == 1)
    assert(t1->time.tv_nsec <= MAX_NANOSLEEP_NSEC);
	assert(t2->time.tv_nsec <= MAX_NANOSLEEP_NSEC);
	#endif
    
	/* t1 - t2 */
	if(t1->sign == 1 && t2->sign == 1) {
		time_sub(result, &(t1->time), &(t2->time));
	}
	/* t1 + t2 */
	else if(t1->sign == 1 && t2->sign == -1) {
		time_add(&(result->time), &(t1->time), &(t2->time));
		result->sign = 1;
	}
	/* -(t1 + t2) */
	else if(t1->sign == -1 && t2->sign == 1) {
		time_add(&(result->time), &(t1->time), &(t2->time));
		result->sign = -1;
	}
	/* t2 - t1 */
	else if(t1->sign == -1 && t2->sign == -1) {
		time_sub(result, &(t2->time), &(t1->time));
	}
}
