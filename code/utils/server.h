#ifndef _SERVER_H_
#define _SERVER_H_

#define MAX_N_HANDLERS      10
#define MAX_N_CLIENTS       5

#include <stdint.h>
#include <poll.h>

typedef void (*handle_event_f)(void *instance);
typedef int (*get_handle_f)(void *instance);

struct event_handler {
    void *instance;
    struct pollfd poll_fd;
    handle_event_f handle_event;
    get_handle_f get_handle;
};

struct client {
    int socket;
    struct event_handler handler;
    struct server *server_p;
};

struct server {
    int listen_socket;
    struct event_handler handler;
    struct client *clients[MAX_N_CLIENTS];
};

int register_handler(struct event_handler *handler);
int unregister_handler(struct event_handler *handler);
void handle_events();

struct server* create_server(uint16_t port, handle_event_f handler_f);
void destroy_server(struct server *server_p);

int create_listen_socket(uint16_t listen_port);

#endif
