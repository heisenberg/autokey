#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

struct msg {
    uint8_t msg_type;
    struct timespec timestamp;
} __attribute__((packed));

// TODO: Implement better de/serialization (e.g. googles lib)
inline int8_t deserialize_msg(struct msg *dest, uint8_t *src, uint32_t src_size, int8_t (*validate)(uint8_t *, uint32_t)) {
    int8_t result = 0;
    
    if(src_size == sizeof(struct msg) && src != NULL && dest != NULL) {
        if(validate != NULL) {
            if(validate(src, src_size)) {
                dest->msg_type = src[0];
                dest->timestamp.tv_sec = ((struct msg *) src)->timestamp.tv_sec;
                dest->timestamp.tv_nsec = ((struct msg *) src)->timestamp.tv_nsec;
                result = 1;
            }
        }
        else {
            dest->msg_type = src[0];
            dest->timestamp.tv_sec = ((struct msg *) src)->timestamp.tv_sec;
            dest->timestamp.tv_nsec = ((struct msg *) src)->timestamp.tv_nsec;                
            result = 1;
        }
    }
    else {
        printf("Deserialization failed.\n");
        dest = NULL;
    }
    
    return result;
}

// TODO: Implement better de/serialization (e.g. googles lib)
inline uint32_t serialize_msg(uint8_t *dest, struct msg *src, uint32_t dst_size) {
    uint32_t msg_size = 0;
    
    if(dest != NULL && src != NULL && dst_size > 0 && sizeof(struct msg) <= dst_size) {
        memcpy(dest, &src->msg_type, sizeof(src->msg_type));
        memcpy(&dest[sizeof(src->msg_type)], &src->timestamp.tv_sec, sizeof(src->timestamp.tv_sec));
        memcpy(&dest[sizeof(src->msg_type) + sizeof(src->timestamp.tv_sec)], &src->timestamp.tv_nsec, sizeof(src->timestamp.tv_nsec));
        msg_size = sizeof(src->msg_type) + sizeof(src->timestamp.tv_sec) + sizeof(src->timestamp.tv_nsec);
    }
    
    return msg_size;
}

#endif
