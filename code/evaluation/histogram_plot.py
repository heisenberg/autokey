#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np

#~ FILE_TO_PLOT = "./nanosleep.dat"
FILE_TO_PLOT = "./clock_gettime.dat"

DTYPE_C_TIMESTAMP = np.dtype([
    ("sec", np.int32),
    ("nsec", np.int32)
])

timestamps = np.fromfile(FILE_TO_PLOT, DTYPE_C_TIMESTAMP)
ts_data = np.empty(len(timestamps), dtype=np.double)
for i in range(len(ts_data)):
    ts_data[i] = timestamps[i]["sec"] + timestamps[i]["nsec"] / 1000000000

fig, ax = plt.subplots()

maximum = np.max(ts_data)
minimum = np.min(ts_data)
mu = ts_data.mean()
median = np.median(ts_data)
sigma = ts_data.std()
textstr = '\n'.join((
    r'$\mu=%.9fs$' % (mu, ),
    r'$\mathrm{median}=%.9fs$' % (median, ),
    #~ r'$\sigma=%.9fs$' % (sigma, ),
    r'$max=%.9fs$' % (maximum, ),
    r'$min=%.9fs$' % (minimum, )))

ax.hist(ts_data, bins='auto')
ax.axvline(ts_data.mean(), color='k', linestyle='dashed', linewidth=1)
ax.axvline(np.percentile(ts_data, 1), color='k', linestyle='dashed', linewidth=1)
ax.axvline(np.median(ts_data), color='k', linestyle='dashed', linewidth=1)
ax.axvline(np.percentile(ts_data, 99), color='k', linestyle='dashed', linewidth=1)
ax.set_xlabel('Time (seconds)', fontsize=24)
ax.set_ylabel('Frequency', fontsize=24)
ax.tick_params(axis='both', which='major', labelsize=24)
#~ ax.set_title("Histogram of execution time of nanosleep and clock_gettime.", fontsize=16)

props = dict(boxstyle='round', facecolor='lightblue')
ax.text(0.71, 0.98, textstr, transform=ax.transAxes, fontsize=24, verticalalignment='top', bbox=props)
        
plt.show()
