import logging
import datetime
import datetime
import numpy as np

from uniflex.core import modules
from uniflex.core import events

import matplotlib.pyplot as plt

#number of CSI pairs (Erich, Miranda) to plot
csi_num = 100
csi_index = 0

# Rows are rx indices, columns are tx indices.
# A value of True in the matrix will select that specific rx and tx antenna pair for cross-correlation.
# You need to enable exaclty 2 pairs of (rx, tx) in both matrices.
erich_indices = [[True, False, False],
                [False, False, False],
                [False, False, False]]
miranda_indices = [[True, False, False],
                [False, False, False],
                [False, False, False]]
erich_indices = np.array(erich_indices)
miranda_indices = np.array(miranda_indices)

def find_pairs(indices):
    pairs = []
    for r in range(0, indices.shape[0]):
        for t in range(0, indices.shape[1]):
            if indices[r,t]:
                pairs.append([r, t])
    return pairs

erich_pairs = find_pairs(erich_indices)
miranda_pairs = find_pairs(miranda_indices)

'''
	Global controller performs channel sounding in 802.11 network using Atheros WiFi chipsets supporting
	CSI.
'''

class ChannelSounderWiFiController(modules.ControlApplication):
    def __init__(self, num_nodes):
        super(ChannelSounderWiFiController, self).__init__()
        self.log = logging.getLogger('ChannelSounderWiFiController')
        self.log.info("ChannelSounderWiFiController")
        self.nodes = {}  # APs UUID -> node
        self.num_nodes = num_nodes
        #setup plot
        self.first_erich = True
        self.first_miranda = True
        self.ydata_erich = None
        self.ydata_miranda = None
        self.colors = ['#FF0000', '#800000', '#FFFF00', '#808000', '#00FF00', '#008000',
            '#00FFFF', '#008080', '#0000FF', '#000080', '#FF00FF', '#800080', '#000000']
        self.show_legend = True
        print('Finished initialisation')

    @modules.on_start()
    def my_start_function(self):
        self.log.info("start control app")

        self.ifaceName = 'wlan0'
        self.start = None
        #self.hopping_interval = 3

        # CSI stuff
        self.results = []
        self.samples = 1


    @modules.on_exit()
    def my_stop_function(self):
        print("stop control app")

    @modules.on_event(events.NewNodeEvent)
    def add_node(self, event):
        node = event.node

        self.log.info("Added new node: {}, Local: {}"
                      .format(node.uuid, node.local))
        self.nodes[node.uuid] = node

        devs = node.get_devices()
        for dev in devs:
            self.log.info("Dev: %s" % str(dev.name))
            ifaces = dev.get_interfaces()
            self.log.info('Ifaces %s' % ifaces)


        if len(self.nodes) == self.num_nodes:
            self.schedule_fetch_csi()


    @modules.on_event(events.NodeExitEvent)
    @modules.on_event(events.NodeLostEvent)
    def remove_node(self, event):
        self.log.info("Node lost".format())
        node = event.node
        reason = event.reason
        if node in self.nodes:
            del self.nodes[node.uuid]
            self.log.info("Node: {}, Local: {} removed reason: {}"
                          .format(node.uuid, node.local, reason))


    def schedule_fetch_csi(self):
        try:
            self.log.info('First schedule_fetch_csi')

            for node in self.nodes.values():
                device = node.get_device(0)
                device.callback(self.channel_csi_cb).get_csi(self.samples, False)

        except Exception as e:
            self.log.error("{} !!!Exception!!!: {}".format(
                datetime.datetime.now(), e))

    def get_power(self, m):
        s = m.shape
        #print('CSI shape', s)
        rx_pwr = np.zeros((s[0], s[1], s[2]))
        rx_phase = np.zeros((s[0], s[1], s[2]))
        for r in range(0, s[0]):
            for t in range(0, s[1]):
                rx_sig = m[r, t, :]
                rx_pwr[r,t] = 10*np.log10(np.absolute(rx_sig))
                rx_phase[r,t] = np.angle(rx_sig)
        return rx_pwr, rx_phase

    def channel_csi_cb(self, data):
        """
        Callback function called when CSI results are available
        """
        global erich_pairs, miranda_pairs
        global csi_num, csi_index

        node = data.node
        devName = None
        if data.device:
            devName = data.device.name
        csi = data.msg

        print("Default Callback: "
              "Node: {}, Dev: {}, Data: {}"
              .format(node.hostname, devName, csi.shape))
        host = node.hostname.lower()

        csi_0 = csi[0].view(np.recarray)

        #print(csi_0.header)
        #print(csi_0.csi_matrix)

        #self.results.append(csi_0)

        #plot data (new data will replace the old one)
        #y, p = self.get_power(csi_0.csi_matrix)
        y = csi_0.csi_matrix

        if 'erich' == host:
            if self.first_erich:
                self.ydata_erich = y
                self.first_erich = False
            else:
                self.ydata_erich = np.concatenate((self.ydata_erich, y), axis=2)
        elif 'miranda' == host:
            if self.first_miranda:
                self.ydata_miranda = y
                self.first_miranda = False
            else:
                self.ydata_miranda = np.concatenate((self.ydata_miranda, y), axis=2)
            left_sig = np.array([])
            left_label = ''
            right_sig = np.array([])
            right_label = ''
            if 0 < len(erich_pairs):
                p = erich_pairs[0]
                left_sig = self.ydata_erich[p[0],p[1],:]
                left_label = 'e_rx'+str(p[0])+'_tx'+str(p[1])
            if 1 < len(erich_pairs):
                p = erich_pairs[1]
                if 0 == len(left_sig):
                    left_sig = self.ydata_erich[p[0],p[1],:]
                    left_label = 'e_rx'+str(p[0])+'_tx'+str(p[1])
                else:
                    right_sig = self.ydata_erich[p[0],p[1],:]
                    right_label = 'e_rx'+str(p[0])+'_tx'+str(p[1])
            if 0 < len(miranda_pairs):
                p = miranda_pairs[0]
                if 0 == len(left_sig):
                    left_sig = self.ydata_miranda[p[0],p[1],:]
                    left_label = 'm_rx'+str(p[0])+'_tx'+str(p[1])
                else:
                    right_sig = self.ydata_miranda[p[0],p[1],:]
                    right_label = 'm_rx'+str(p[0])+'_tx'+str(p[1])
            if 1 < len(miranda_pairs):
                p = miranda_pairs[1]
                if 0 == len(left_sig):
                    left_sig = self.ydata_miranda[p[0],p[1],:]
                    left_label = 'm_rx'+str(p[0])+'_tx'+str(p[1])
                else:
                    right_sig = self.ydata_miranda[p[0],p[1],:]
                    right_label = 'm_rx'+str(p[0])+'_tx'+str(p[1])
            if 0 == len(left_sig) or 0 == len(right_sig):
                print('Cannot find 2 non-empty antenna pairs (rx, tx)')
                return
            cross_corr = np.correlate(left_sig, right_sig, mode='full')
            print('xcorr len ', len(cross_corr))
            plt.figure(0)
            plt.subplot(2, 1, 1)
            plt.plot(10*np.log10(np.absolute(left_sig)), '-', label=left_label,
                color=self.colors[0])
            plt.plot(10*np.log10(np.absolute(right_sig)), '-', label=right_label,
                color=self.colors[1])
            plt.plot(10*np.log10(np.absolute(cross_corr)), 'k--', label='Cross-correlation')
            if self.show_legend:
                plt.xlabel('Index ('+str(csi_num)+' CSI packets)')
                plt.ylabel('Magnitude [dB]')
                plt.legend()
            plt.subplot(2, 1, 2)
            plt.plot(np.angle(left_sig), '-', label=left_label, color=self.colors[0])
            plt.plot(np.angle(right_sig), '-', label=right_label, color=self.colors[1])
            plt.plot(np.angle(cross_corr), 'k--', label='Cross-correlation')
            if self.show_legend:
                plt.xlabel('Index ('+str(csi_num)+' CSI packets)')
                plt.ylabel('Phase [rad]')
                plt.legend()
                self.show_legend = False
            fn_suffix = ''
            if 0 < len(erich_pairs):
                p = erich_pairs[0]
                fn_suffix += 'e_rx'+str(p[0])+'_tx'+str(p[1])
            if 1 < len(erich_pairs):
                p = erich_pairs[1]
                if '' != fn_suffix:
                    fn_suffix += '_'
                fn_suffix += 'e_rx'+str(p[0])+'_tx'+str(p[1])
            if 0 < len(miranda_pairs):
                p = miranda_pairs[0]
                if '' != fn_suffix:
                    fn_suffix += '_'
                fn_suffix += 'm_rx'+str(p[0])+'_tx'+str(p[1])
            if 1 < len(miranda_pairs):
                p = miranda_pairs[1]
                if '' != fn_suffix:
                    fn_suffix += '_'
                fn_suffix += 'm_rx'+str(p[0])+'_tx'+str(p[1])
            plt.savefig('chsounder_xcorr'+fn_suffix+'.eps')
            plt.clf()

        # schedule callback for next CSI value
        if csi_index < csi_num:
            data.device.callback(self.channel_csi_cb).get_csi(self.samples, False)
            csi_index += 1
