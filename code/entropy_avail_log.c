#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUFFER_SIZE 32

int main(void) {
	const char* ENTROPY_PATH = "/proc/sys/kernel/random/entropy_avail";
	const char* LOG_NAME = "entropy_avail.log";
	FILE *entropy_file;
	FILE *log_file;
	time_t ts;
	int entropy_avail;
	
	log_file = fopen(LOG_NAME, "w");
	
	printf("Entropy available log: <entropy_available>, <time>\n");
	
	while(1) {
		entropy_file = fopen(ENTROPY_PATH, "r");
		
		ts = time(NULL);
		fscanf(entropy_file, "%d", &entropy_avail);
		printf("%d, %s", entropy_avail, ctime(&ts));
		fprintf(log_file, "%d, %s", entropy_avail, ctime(&ts));
		fflush(log_file);
		
		fclose(entropy_file);
		sleep(1);
	}

	fclose(log_file);
	return 1;
}
