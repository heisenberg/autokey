# It works as follows:

# It waits until the two uniflex (wishful) agents register (erich/mirinda)

# It aks for the next CSI using call:

#          device.callback(self.channel_csi_cb).get_csi(self.samples, False)

# The results are received in channel_csi_cb callback function:

# csi_0.header - header information: e.g. Rf frequency, ...

# csi_0.csi_matrix - the CSI matrix itself

# The originator of the CSI can be retrieved using: node.hostname (line 101)
