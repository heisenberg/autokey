\documentclass{article}[12pt, a4paper, twoside]
%-------------------------------------------
\usepackage[utf8]{inputenc}
%-------------------------------------------
\usepackage[numbers]{natbib}                % Reimplementierung von \cite
\usepackage{fancyhdr}                       % Kopf und Fußzeile
\usepackage{titlesec}                       % Sectionname ohne Nummer in Kopfzeile
\usepackage[onehalfspacing]{setspace}       % Zeilenabstand
\usepackage{amsmath}                        % Mathematische Umgebungen und Symbole
\usepackage{graphicx}                       % Einfügen von Bilder
\usepackage{listings}                       % Code listings
\usepackage{xcolor}
\usepackage{tabularx}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=3cm,twoside]{geometry} % Randabstände
\setlength{\parindent}{0pt}

%https://en.wikibooks.org/wiki/LaTeX/Source_Code_Listings
\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  numbers=left,
  xleftmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{blue},
  commentstyle=\itshape\color{purple},
  identifierstyle=\color{black},
  stringstyle=\color{orange},
}
\lstset{escapechar=@,style=customc}

\begin{document}

	\title{Documentation for the TCP time synchronization application}
	\author{Aljoscha Pinz}
	\date{\today}
	\maketitle
	
	\section{Purpose of the application}
	The $tcp\_timesync$ application executes two successive tasks:
	At first it synchronizes the system time of two wirelessly connected PCs in the TWIST testbed, namely erich and mirinda.
	Afterwards at the same time a ping job and a csi measurement job is started on both erich and mirinda.
	
	\section{Usage of the application}
	Currently mirinda has the role of server and erich the role of client.
	It is assumed that prior to the execution of the application a working access point was created on erich and that mirinda created a link with that access point.
	This can be done by calling two scripts:
	\begin{enumerate}
		\item $@erich:   sudo .erich\_ap\_setup.sh$}
		\item $@mirinda: sudo .mirinda\_link\_setup.sh$}
	\end{enumerate}
	It is further assumed that there is no program running that is interfering with the process of reading or setting the system time.
	It should be noted that therefore the precision time protocol deamon (ptpd) on erich has to be killed prior to execution.
	To execute the application just start the server and the client.
	The programs can take an ipv4 address as an optional argument for testing purposes.
	\begin{enumerate}
		\item $@mirinda: sudo .tcp\_timesync\_server$}
		\item $@erich: sudo .tcp\_timesync\_client$}
	\end{enumerate}
	
	\section{Functional description of the application}
	The program has a client-server model, whereby the client synchronizes its system time with the sytem time of the server.
	The time synchronization is done via a slightly altered version of a two-way message exchange as described in \cite{Dargie:2010:FWS:1941122}.
	The original approach for a two-way message exchange utilizes four timestamps, which is reasonable when further computations are made between reception and sending of the second and third timestamp or when the network interface controller (NIC) of the system is able to perform hardware timestamping.
	However, this application only uses three timestamps to calculate the time offset of the clocks of erich and mirinda because of two reasons:
	Hardware timestamping support is dependent on the system hardware, mirinda probably allows hardware timestamping whereas erich doesn't.
	The message arriving at the server, containing the timestamps t1 is received, the timestamp is taken and it is sent back directly to the client, as shown in the code listing below.
	
	\begin{lstlisting}
if(recvfrom(client_socket, (void*) rcv_buffer, BUFFER_SIZE-1, 0, (struct sockaddr*) &client, &addr_len) == -1) {
  printf("error: socket is nonblocking.\n");
}
else {
  if(clock_gettime(CLOCK_REALTIME, &time) == 0) {
    str_len = snprintf(time_str, BUFFER_SIZE, "%ld.%09ld", time.tv_sec, time.tv_nsec);
    if(str_len < 0) {
      printf("error: serializing timespec.\n");
    }
  }
  else {	
    printf(CLOCK_GETTIME_ERROR);
  }
  if(check_for_state_change(rcv_buffer, EXEC_JOB_STATE, EXEC_JOB_STATE_SIZE)) {
    state = EXEC_JOB;
  }
  else {
    printf("received time: %s\n", rcv_buffer);
    printf("local time:    %s\n\n", time_str);
    str_len = snprintf(snd_buffer, BUFFER_SIZE, "%s;%s", rcv_buffer, time_str);
    send(client_socket, snd_buffer, str_len, 0);
  }
}
	\end{lstlisting}
	The time spent for the call of $check\_for\_state\_change$ in line 14 is negligible, the other calls between $clock\_gettime$ in line 5 and $send$ in line 21 are necessary calls for error checking.
	
	\begin{figure}[ht]
	\centering
		\includegraphics[width=1.0\textwidth]{timesync_delay_diagram.pdf}
	\caption{Illustration of computation delays and points in time when the timestamps are taken.}
	\label{fig:delay}
	\end{figure}
	
	To compute the offset $\delta$ of the clocks of erich and mirinda the differences of the timestamps are calculated.
	Those differences still contain a propagation delay $D$ and computation time delays $D_{ci}$ and $D_{si}$ as shown in figure~\ref{fig:delay}.
	\[t_{2} - t_{1} = D + D_{c1} + D_{s1} + \delta\]
	\[t_{3} - t_{2} = D + D_{c2} + D_{s2} - \delta\]
	For clearification it should be noted that $\delta_{erich} = -\delta_{mirinda}$.
	Further it is assumed that $D_{erich} = D_{mirinda}$ and that D is very small.
	
	To reduce the error of the calculated offset the above offsets are substracted:
	\[\frac{(t_{2} - t_{1}) - (t_{3} - t_{2})}{2} = \delta + \frac{D_{c1} + D_{s1} - D_{c2} - D_{s2}}{2}\]
	The remaining error is described with the latter term:
	\[\frac{D_{c1} + D_{s1} - D_{c2} - D_{s2}}{2}\]
	
	\section{Notes}
	In the $tcp\_timesync.h$ header some customizations can be made, the onces of most interest for the user are:
	
	\begin{lstlisting}
#define TIME_SYNC_PORT            50000
#define OFFSET_MEASUREMENTS       1
#define MEASUREMENT_DELAY         1
#define CSI_MEASURE_DIRECTORY     "./collect_csi_local.py"
#define CSI_MEASURE_PROCESS_NAME  "csi_measure"
#define PING_COUNT                "-c 350"
#define PING_INTERFACE_ERICH      "-I wlan905"
#define PING_INTERFACE_MIRINDA    "-I wlan9"
#define PING_INTERVAL             "-i 0.2"
#define PING_QUIET                "-q"
#define IP_ERICH                  "192.168.5.1"
#define IP_MIRINDA                "192.168.5.2"
	\end{lstlisting}
	
	To handle a higher propagation delay D in case of a TCP retransmission, the number of offset measures taken can be increased by setting $OFFSET\_MEASUREMENTS$.
	The application will then use the minimum of all the measured offsets as correction value to set the system time.
	
	In the remainder of this document table~\ref{table:headers} shows which functions of the included headers are used and figure~\ref{fig:sequence} shows a sequence diagram for the execution of a $tcp\_timesync\_client$ and $tcp\_timesync\_server$ instance.
	\lstset{numbers=none, frame=none}
	\begin{table}
	\centering
	\begin{tabular}{ll}
Header & Function \\
\hline
stdlib.h &
\begin{lstlisting}
void exit(int status);
\end{lstlisting} \\

stdio.h &
\begin{lstlisting}
int printf(const char *format, ...);
\end{lstlisting} \\
 &
\begin{lstlisting}
int snprintf(char *str, size_t size, const char *format, ...);
\end{lstlisting} \\
 & 
\begin{lstlisting}
int fflush(FILE *stream);
\end{lstlisting} \\

string.h &
\begin{lstlisting}
void *memset(void *s, int c, size_t n);
\end{lstlisting} \\

unistd.h &
\begin{lstlisting}
int close(int fd);
\end{lstlisting} \\
 &
\begin{lstlisting}
pid_t fork(void);
\end{lstlisting} \\
 &
\begin{lstlisting}
unsigned int sleep(unsigned int seconds);	
\end{lstlisting} \\
 &
\begin{lstlisting}
void _exit(int status);	
\end{lstlisting} \\
 &
\begin{lstlisting}
int execl(const char *path, const char *arg, ... /* (char  *) NULL */);	
\end{lstlisting} \\
 &
\begin{lstlisting} 
int execlp(const char *file, const char *arg, ... /* (char  *) NULL */);
\end{lstlisting} \\
 &
\begin{lstlisting}
int dup2(int oldfd, int newfd);	
\end{lstlisting} \\

sys/socket.h &
\begin{lstlisting}
ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t *addrlen);
\end{lstlisting} \\
 &
\begin{lstlisting}
ssize_t send(int sockfd, const void *buf, size_t len, int flags);
\end{lstlisting} \\
 &
\begin{lstlisting}
int socket(int domain, int type, int protocol);
\end{lstlisting} \\
 &
\begin{lstlisting}
int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
\end{lstlisting} \\
 &
\begin{lstlisting}
int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
\end{lstlisting} \\
 &
\begin{lstlisting}
int listen(int sockfd, int backlog);
\end{lstlisting} \\
 &
\begin{lstlisting}
int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
\end{lstlisting} \\

netinet/in.h &
\begin{lstlisting}
struct sockaddr_in {
  sa_family_t    sin_family; /* address family: AF_INET */
  in_port_t      sin_port;   /* port in network byte order */
  struct in_addr sin_addr;   /* internet address */
};
\end{lstlisting} \\

arpa/inet.h &
\begin{lstlisting}
int inet_aton(const char *cp, struct in_addr *inp);
\end{lstlisting} \\

time.h &
\begin{lstlisting}
int clock_gettime(clockid_t clk_id, struct timespec *tp);	
\end{lstlisting} \\
 &
\begin{lstlisting}
int clock_settime(clockid_t clk_id, const struct timespec *tp);	
\end{lstlisting} \\
 &
\begin{lstlisting}
int nanosleep(const struct timespec *req, struct timespec *rem);	
\end{lstlisting} \\

errno.h &
\begin{lstlisting}
/* The  <errno.h>  header  file defines the integer variable errno */
\end{lstlisting} \\

sys/wait.h &
\begin{lstlisting}
pid_t waitpid(pid_t pid, int *status, int options);
\end{lstlisting} \\

sys/types.h & 
\begin{lstlisting}
/* defines the types: pid_t, ssize_t, socklen_t */
\end{lstlisting} \\

fcntl.h & 
\begin{lstlisting}
#define O_WRONLY
#define O_CREAT
\end{lstlisting} \\

	\end{tabular}
	\caption{Included headers and functions that are used by $tcp\_timesync$ programs.}
	\label{table:headers}
	\end{table}

	\begin{figure}[ht]
	\centering
		\includegraphics[width=1.0\textwidth]{timesync_sequence_diagram.pdf}
	\caption{Sequence diagramm for the $tcp\_timesync$ application. The value of "offset" can be positive or negative.}
	\label{fig:sequence}
	\end{figure}
	
	\bibliographystyle{unsrt}
	\bibliography{bibliography}
}
	
\end{document}
